# CURSO EKONOMIKON
__ECONOMÍA ALTERNATIVA Y SOCIAL__

Con este curso queremos entender que es la economía alternativa y porqué debe ser nuestro modelo a seguir repudiando el sistema actual imperante llamado Capitalismo.

Especialmente indicado para la gente que piensa que otro mundo es posible y que éste ya está aquí.

Recopilado principalmente del Ekonomikon con experiencias cooperativistas y otras bibliografías que se irán recopilando en un glosario.

## CONCEPTOS CLAVE

### RIQUEZA.-

¿Qué es para tí la riqueza? (marca tu respuesta)
- [ ] La riqueza es TODO el dinero que tengo.
- [ ] La riqueza es el conjunto de las cosas que se poseen, especialmente dinero, bienes o cosas valiosas.
- [ ] La riqueza es el conjunto de bienes, derechos y obligaciones.
- [ ] La riqueza es el amor de mi gente.
- [ ] La riqueza es los recursos que dispongo para cubrir mis necesidades.
- [ ] La riqueza son los recursos que mi colectivo comparte para cubrir nuestras necesidades.

- - -

Las primeras 3 definiciones son la primera que define alquien al preguntarle, la definición del diccionario RAE y la de la enciclopedía económica... pero tan solo las 3 de abajo son interesantes... porque la 4 habla de amor algo etereo y de mi gente como colectivo, la 5 de los recursos necesarios y la 6 incluye al colectivo...

Nos vamos a quedar con las últimas 3 y en especial con la 6 como que la riqueza real son los recursos que dispone/comparte el colectivo para cubrir cada una de nuestras necesidades y las de los bienes o dinero la llamaremos riqueza capitalista pues no piensa en las necesidades sino simplmenete en tener recursos.

Para completar la riqueza real hay que incluir también la fuerza de trabajo del colectivo por lo que para conocer la riqueza real sumaremos los recursos del colectivo y su fuerza de trabajo, así en productos y servicios respectivamente.

* * *

### NECESIDADES.-

Qué necesitas es una gran pregunta que nos podemos hacer... hoy en día si tenemos la fortuna de levantarnos y vivir en lo que llamamos occidente o primer mundo casi ni nos la hacemos y si nos la hacen igual nos quedaríamos pensando un rato antes de contestar o igual decimos un ferrari o una casa en la playa... jejeje pueden ser necesidades... aunque no seré yo el que las cuestione. Antes de seguir pensando ya nos queda claro que para cada uno puede ser distinta la respuesta.

Por ejemplo:

 - Para la wikipedia, una necesidad es aquello imprescindible para vivir en estado de salud plena.
 - Para la RAE, una necesidad es el impulso que hace que las causas obren infaliblemente en cierto sentido o aquello a lo cual es imposible resistirse.

_Para mi, básicamente una necesidad es algo material o inmaterial que una persona o colectivo requiere para realizar una tarea o realizarse uno mismo._

Es tan ambiguo que hay que llegar a un acuerdo previo para establecer que es una necesidad... aunque podemos guiarnos por algunos estudios cada uno es un mundo y por lo tanto cada colectivo será el mundo que ellos quieran.

Y esa decisión de que NECESIDAD del colectivo es importante que TODO el colectivo la tenga consensuada.

* * *

### INTERCAMBIO.-

El tercer concepto que debemos tener claro antes de continuar es el intercambio... básicamente une las necesidades individuales al poder intecambiar unas por otras.

Veamos también las definiciones de Intercambio:
- RAE: Hacer cambio recíproco de una cosa o persona por otra u otras
- Wikipedia: intercambio se refiere a hacer el cambio de una cosa por otra.

Vemos que en este caso hay consenso pero como vamos a realizar esos intercambios para que nos parezcan justos y cubran nuestras necesidades individuales y de colectivo y así se añadan a la riqueza es lo verdaderamente interesante.

Cuando un intercambio entre dos personas es directo, una cosa por otra, podemos también usar la palabra __trueque__ por lo que podríamos decir que son sinónimos.

Y aquí comienza el primer sistema de intercambio que tenemos constancia... El Trueque es un sistema que sigue siendo utilizado y que permite mediante un acuerdo equiparar el valor de una cosa por el de la otra intercambiandolo directamente.

Ejemplo:
 - 1 melón = 1 sandía
 - 1 melón = 2 sandias

En estos ejemplos el valor del melón es disntinto en el primer caso es igual al de la sandía y en el segundo es el doble que el de la sandía... y esta situación creó las referencias y/o unidades de valor que serán la base de los siguientes sistemas de intercambio.

Me importante reforzar este punto y danos cuenta que el valor de las cosas es es simplemente una referencia otorgada en un momento y ocasión por las personas que realizan el intercambio en modo trueque por lo que influyen sus circunstancias, las circunstancias de los productos/servicios que se intercambian y pueden o no estar pensando en las circunstancias del otro pero de alguna manera llegan a consenso.

_ Por lo tanto podríamos decir que el __valor__ es el consenso de un momento creado por unas personas y podemos o no utilizarlo si pensamos que son las mismas circunstancias._