# EKONOMIKÓN
MANUAL BÁSICO DE ECONOMÍA ALTERNATIVA Y MONEDAS SOCIALES

* * *
## Licencia Libre
Esta es la versión en Markdown del libro Ekonomikon sin autoria previa y ahora al pasarse a código he querido incluir la licencia libre Creative Comons Compartir igual para garantizar el libro de forma libre.

Pueden consultar las bases de la licencia aquí:
[Creactive Commons BySA](http://creativecommons.com/...)

# INTRODUCCIÓN HISTÓRICA A NUESTRA ECONOMÍA

Para explicar las monedas sociales es necesario, en primer lugar, tener algunas nociones básicas de economía:

## 1.- CONCEPTOS CLAVE

### Riqueza:
¿ Qué es la "riqueza" de un grupo? Riqueza es la capacidad de un grupo o persona para satisfacer sus "necesidades", aunque en la actualidad puede también considerarse como "la cantidad de dinero que posee un grupo o persona". Las distinguiremos como "riqueza real" y "riqueza monetaria".

La riqueza real incluye tanto los recursos como la fuerza de trabajo que transforma los recursos. También existen recursos y fuerzas naturales que pueden considerarse como riqueza real. La fuerza de trabajo será la capacidad que tiene el ser humano de trabajar.

Los recursos y la fuerza de trabajo funcionan en la economía como "productos" y "servicios" respectivamente.

- - -

### Necesidades:
Las necesidades son aquellas cosas que resultan necesarias para cada grupo o persona, y en esto influye mucho lo que cada persona considera "necesario", ya que no todos creen necesitar las mismas cosas, aunque hay algunas cosas que todos están de acuerdo en que son necesarias (Productos: ropa y alimento, aunque también en la ropa y alimento que necesita cada uno puede haber diferencias; Servicios: La salud).

Las personas pueden producir algunos productos y prestar algunos servicios, pero nadie puede producirlos y prestarlos todos, por lo que se hace necesario el intercambio de unos por otros entre los diferentes miembros de un grupo. Para facilitar esos intercambios surgirá el dinero.

ESTOS CONCEPTOS DEBEN QUEDAR CLAROS. SI HAY ALGUNA DUDA AL RESPECTO, DEBE SOLUCIONARSE ANTES DE SEGUIR. Riqueza, necesidades e intercambio...

Por ejemplo: ¿Y si alguien puede producir todo lo que necesita y no necesita nada de nadie?. Eso es muy improbable, pero si esa persona existe, esa persona no necesita de la economía. Si tiene un excedente, sería una pena que se desperdiciase, así que aunque no consuma ningún bien o servicio ajeno, habrá que ver la manera de aprovechar sus excedentes sin causarle molestias.
¿Alguna duda más?


* * *



## 2.- EL INTERCAMBIO DE PRODUCTOS O SERVICIOS: DEL TRUEQUE PURO A LA UNIDAD DE VALOR.

Actualmente, cualquier intercambio o transacción se produce a cambio de unidades monetarias, o dinero. Así pues, la moneda surge para facilitar el intercambio de bienes, pero no siempre existió la moneda. Hubo un tiempo en que las cosas se intercambiaban directamente a través del "trueque" o de la "permuta" (estas dos palabras fueron creadas para referirse a los intercambios, así que las 3 palabras son sinónimas).

- - -

### 2.1.- EL TRUEQUE PURO
En sus orígenes, el trueque aparece como una necesidad imperiosa de las personas para acceder a aquellos productos o servicios que ellos mismos no podían procurarse. Esto pudo deberse a la aparición de la propiedad como evolución jurídica de la posesión, pero como de momento, no tenemos intención de crear una nueva propiedad, ni de abolir la existente, daremos por sentado que la propiedad existe, aunque sepamos que es algo sobre lo que, tarde o temprano, habrá que
hablar.

De esta manera, la gente tiene la posesión, y también la propiedad, de aquellos bienes que produce, y de aquellos servicios que presta, y puede intercambiarlos por otros que necesita y no posee.

Así surgirá lo que se conoce como "trueque", "intercambio" o "permuta". El trueque consiste en que una persona entrega a otra un producto, o le presta un servicio, a cambio de que esa persona le entregue otro "producto" o "servicio", o le devuelva el mismo que le prestó en un momento futuro.

Ambas partes estarán de acuerdo en que el intercambio es justo para llevarlo a cabo, y es de esa concepción de la "justicia" en el intercambio de la que nace el concepto de "valor", referido al valor de esos productos o servicios intercambiados.

Evidentemente, las diferentes necesidades de cada persona llevan a la paradoja de que un mismo producto o servicio, puede tener valores diferentes para personas diferentes. El valor es algo subjetivo de cada persona, y al hacerse el intercambio, el valor de esas cosas sigue siendo subjetivo, aunque ahora sea idéntico para esas dos personas que hacen el trueque, lo que puede dar una apariencia de objetividad.

Por ejemplo, si A y B intercambian una sandía por un melón, será porque ambos creen que la sandía y el melón valen lo mismo. Si cambian una sandía por 2 melones, será porque ambos creen que una
sandía vale el doble que un melón.

Una vaca podría cambiarse por 10 sandías, o por 15 sandías, o por 100 sandías, dependiendo del valor que ambas personas atribuyan a una vaca y a una sandía. Y todos esos valores podrán ser válidos, siempre que ambas partes estén de acuerdo en el valor de los productos intercambiados.

Esta economía del trueque puede resultar suficiente para pequeños grupos en los que existan pocos recursos, porque será posible hacer una tabla de valores usando como "unidad de valor" la sandía, o el melón, o cualquier otro producto.

Para los servicios podrían usarse como "unidades de valor" las unidades de tiempo (las horas, o los minutos), y así podría ser que una hora de arar la tierra, equivalga a una hora de podar árboles, o de cortar leña, pero también podría suceder que una hora de cortar leña, equivalga a 2 horas regando, o a 4 horas vigilando una parcela, atendiendo al esfuerzo físico que suponga el servicio prestado.

También podrían valorarse atendiendo a la cualificación del servicio prestado, o a cualquier otro criterio que ambas partes consideren "justo".

No debe olvidarse que esos intercambios tienen lugar entre 2 personas, y que son esas dos personas las que realizan el intercambio, por lo que, los demás podrán conocer dicho intercambio, y podrán considerarlo justo o injusto, pero no podrán impedirlo.

Los terceros podrán aconsejar que no se haga dicho intercambio, o podrán negarse a hacer ellos mismos intercambios similares, o incluso podrán negarse a realizar intercambios con aquella persona que haga intercambios injustos en perjuicio de otras personas para no ser perjudicados, pero es muy importante entender que un trueque es un negocio que afecta a dos personas y solo ellas pueden decidir el valor que atribuyen a los productos o servicios intercambiados, y cada cual lo hace atendiendo a sus "necesidades".

- - -

### 2.2.- EVOLUCIÓN A LA UNIDAD DE VALOR

En los grupos más numerosos o en las sociedades más grandes, con gran variedad de productos, resultaba muy complejo hacer tablas que incluyesen las equivalencias de todos los productos y se optó por hacer una tabla más simple que únicamente establecía los productos y su equivalencia en un mismo producto, digamos que por ejemplo, fueran "las sandías".

Era evidente que no todo el mundo necesitaba sandías, pero se forzaba el acuerdo imponiendo "la tabla de valores en sandías", lo que supuso un pequeño error de la economía, ya que no se atendía a la realidad de que existía gente que podía vivir sin necesitar las sandías, por lo que las sandías para ellos carecían de valor, pero el problema se agravaba cuando había personas que NO tenían sandías para intercambiar. Así que los intercambios se hacían con una lógica matemática como la siguiente:

	A necesitaba una gallina de B, y B necesitaba usar el tractor de A durante 1 hora. Era sabido por todos que una gallina solía cambiarse por 2 sandías (porque en la tabla se había elegido la sandía como "unidad de valor"), y que 1 hora de uso del tractor se intercambiaba por 4 sandías, por lo que A y B podían hacer una regla de 3 y calcular que si una gallina son 2 sandías, y 1 hora de tractor eran 4 sandías, una gallina solo se podía intercambiar por media hora de uso del tractor. O bien podían intercambiar 2 gallinas por usar el tractor una hora completa.

		1 gallina = 2 sandías = 1/2 hora de tractor
		2 gallinas = 4 sandías = 1 hora de tractor

Si eliminábamos las sandías, podíamos intercambiar directamente las gallinas por el uso del tractor, y en ese caso, la sandía se había convertido en "la unidad de valor", o lo que también se podía llamar "la unidad de intercambio".

Obsérvese que en estas tablas se ignoraba el hecho de que no todo el mundo necesitaba las sandías de la misma forma, pero a la sandía se le atribuía un "valor" igual para todos, para que todos pudiesen saber qué cosas se podían conseguir con un número determinado de sandías.

La unidad de valor elegida, que en este ejemplo sería la sandía, recibía un valor impuesto e invariable para quienes aplicaban esas tablas, sin atender a sus necesidades reales, y a cambio, podían intercambiar cualquier producto o servicio confiando en la objetividad de "la tabla de las sandías".

En un principio sería frecuente que dichas unidades de valor poseyeran un valor intrínseco, o sea, una utilidad, para que en caso de que nadie aceptase intercambiar dicho producto, dicho producto pudiera al menos satisfacer alguna necesidad. Dicha utilidad podía ser diferente para cada persona, pero la tabla hacía que dicho valor se hiciese objetivo para quienes aceptaban la tabla.

Olvidar la subjetividad del valor, para imponer una objetividad ilusoria del mismo a través de la tabla, fue la base sobre la que surgirían todos los problemas en el futuro, ya que ello creó la piedra angular sobre la que el valor falsamente objetivizado permitiría controlar el "valor" como una realidad objetiva.

Como las sociedades eran diferentes y estaban alejadas unas de otras, cada una podía elegir el producto con que fabricarían sus tablas, y así algunos establecieron como "unidad de valor" el cacao, otros establecieron la sal y otros establecieron otro tipo de bienes, como la pólvora, el té, o las pieles, aunque todos ellos acabarían siendo consumidos y desaparecerían del ciclo económico apareciendo nuevas unidades de intercambio para que la economía siguiera
funcionando.

Había surgido el fantasma de "la unidad de valor".

* * *

# 3.- DE LA UNIDAD DE VALOR A LAS MONEDAS DE ORO.

Las unidades de valor que usaban los grupos solían tener unas cualidades determinadas para que pudieran desempeñar bien su función.

Así, para que una Unidad de valor fuese realmente eficaz era necesario que dicha unidad fuese duradera, para que su uso no la hiciera desaparecer. También debía ser escasa, o al menos, difícil de conseguir, para evitar que todo el mundo pudiera producir únicamente dicha unidad de valor sin producir nada más, ya que ello llevaría a la desaparición de la riqueza real en cuanto todo el mundo se dedicase a producir únicamente sandías (si fuese la sandía la unidad de valor).

Y también era necesario que fuese fácil de transportar, para que ir al mercado pudiera hacerse sin tener que llevar un carrito en el que cargar las unidades de valor, por lo molesto que sería ir al mercado portando 500 sandías para comprar.

Con estos tres requisitos (durabilidad, escasez y ligereza) surgieron unidades de valor como el cacao o la sal, pero también empezaron a utilizarse algunos metales preciosos como el oro o la plata, que sin ser comestibles ni estrictamente necesarios, cumplían mejor la exigencia
de esos 3 requisitos, al ser escasos, poder transportarse fácilmente en una bolsa y ser muy duraderos.

Así empezaron a usarse el oro, la plata, el bronce, las joyas y todo lo que se conoce como "metales preciosos" para intercambiarlos por cualquier producto o servicio, y empezaron a utilizarse para fijar "el precio" de las cosas (de ahí que se les llame metales "preciosos").

Esto sucedió hace más de 4.000 años.

Estos metales, para ser utilizados, debían estar referidos a algún sistema métrico, ya que en un principio, alguien podía cambiar un pedazo de oro por un caballo, y otra persona podía cambiar ese mismo caballo por otro pedazo de oro que fuese de mayor o menor tamaño que el que había entregado para adquirir el caballo.

Para ello se trató de hacer pedazos de oro, o de otros metales preciosos, de tamaños más o menos parecidos, pero no tardaron en aparecer quienes limaban esos pedazos para extraerles una pequeña porción, y así, haciendo eso con varios pedazos, se conseguía reunir suficiente metal para hacer un pedazo nuevo.

Esto no gustaba a quienes se habían tomado la molestia de hacer los pedazos más o menos iguales para atribuirles un mismo valor, ya que los pedazos se hacían cada vez más pequeños y adquirían tamaños diferentes, y a la vez aparecían cada vez más pedazos.
Así que, para evitar este fraude, se decidió hacer todos esos pedazos de una misma cantidad de metal precioso y añadirle inscripciones que permitiesen distinguir los pedazos verdaderos de los pedazos falsos.

Así se estableció un peso determinado para cada pedazo de metal, y a éste se le solía inscribir su peso y composición metálica, o algún dibujo o, como se haría más adelante, incrustar la fecha de la emisión y la cara de la persona que ordenaba su emisión.

Estas medidas tenían como finalidad evitar que alguien con 100 pedazos pudiera, a base de limarlas y quitar un poco de polvo de metal a cada una, fabricarse un pedazo nuevo y tener 101 pedazos, ya que se tenía claro que para conseguir un pedazo de metal precioso era necesario crear algo de riqueza real, bien produciendo algún producto, o bien prestando algún servicio.

Cuando esta fabricación en serie de pedazos de metales preciosos iguales e identificables se hizo de manera oficial, con las garantías para evitar su falsificación, fue cuando surgieron "las monedas".

Estas monedas se hacían con recursos naturales que de por sí eran difíciles de conseguir, y se les añadían unas garantías que evitaran su falsificación o creación fuera del sistema oficial, pero seguía siendo compatible con la aparición de dichos metales en las minas, y dichos metales extraídos de las minas seguían conservando un valor, aunque no llevasen esos mecanismos de seguridad.

De entre todos los metales acabaron por imponerse el uso del oro y de la plata, de manera que con el tiempo, los diferentes grupos acabaron aceptando tablas de intercambio que usaban como unidad de valor el oro y la plata.

Para ello bastaba con usar únicamente el oro como unidad de valor, e incluir la plata como un producto fácilmente convertible a oro. Por ejemplo, diciendo que una moneda de oro equivalía a 10 monedas de plata.

También podían ajustar otros metales a esa fácil conversión, y así una moneda de oro podía valer 10 monedas de plata, o 100 monedas de bronce o 1.000 monedas de cobre. Y así se elaboraría una tabla de intercambio basada en el oro.

	Tabla del oro: 1 moneda de oro = 10 monedas de plata = 100 monedas de bronce = 1000 monedas de cobre.

La gente empezó a utilizar las monedas como herramienta para intercambiar sus productos o servicios, sabiendo que cualquier persona aceptaría intercambiar otros productos por estas monedas, siempre y cuando dichos intercambios se ajustasen a esas tablas.

Al mismo tiempo, cada vez se suprimían más productos de dichas tablas, atendiendo a la realidad social de que la riqueza real de la sociedad (los productos y servicios) tenían valores distintos para cada persona, pero se había creado una manera de valorar las cosas que resultaba fácilmente utilizable por cualquiera que aceptase intercambiar las cosas por alguno de los metales de la tabla.

En esencia, se consiguió que el valor de las cosas se midiese usando esas monedas, lo que en realidad venía a significar que todo el mundo aceptaba como unidad de valor el oro, fácilmente convertible a los demás metales, y a su vez, a los demás recursos. Y ahí empezó una época que podremos identificar como "la época de las monedas de oro".

Hubo gente que solo utilizó esas tablas para tener un referente de valor para sus productos, sin llegar a intercambiarse oro físicamente, por lo que esas personas, lo único que verían en la tabla de intercambio era una manera de establecer un "valor" a sus productos, aunque con frecuencia lo consideraban puramente orientativo, y seguían intercambiando las cosas directamente por aquellos productos o servicios que ellos necesitaban, sin someterse al uso de las 11monedas, y siendo ellos quienes decidían en cada intercambio el auténtico valor de las cosas.

Como se ha dicho antes, para la mayoría, el sistema de las monedas acabó usando el oro como medida de referencia, por lo que la cantidad de oro de un grupo social acababa siendo un elemento fácilmente observable y cuantificable de su "riqueza".

Esto podía parecer una simplificación beneficiosa para entender o conocer la riqueza de los diferentes grupos o personas de una sociedad, pero trajo consigo algunos problemas que deformaron la economía a esta altura de la historia como consecuencia de esta simplificación.

Y a consecuencia del uso de las monedas de oro, apareció el capitalismo, sigilosamente al principio y cada vez haciendo más ruido, para transformarlo todo, hasta que se hizo visible, y finalmente, llegó a imponerse. Pero vayamos por partes.

* * *

# 4.- LA LLEGADA DEL CAPITALISMO

#### 4.1.- CAPITALISMO UTIL Y MONETARIO.

El capitalismo puede entenderse de varias maneras, ya que en sus orígenes, hace 4.000 años aproximadamente, el capitalismo trataba de ser una estrategia económica que trataba de organizar grupos para producir una mayor riqueza, y ni si quiera se denominaba capitalismo a sí mismo.

Pero como la riqueza tenía dos significados, hubo dos capitalismos, el que creía que la riqueza era la capacidad para satisfacer las necesidades, y el que creía que la riqueza era la posesión de monedas de oro.

Uno trataba de incrementar la producción de las riquezas reales reduciendo costes, desarrollando técnicas más eficientes de producción, coordinando a grupos de personas y repartiendo las tareas de manera organizada para producir recursos suficientes, usando menos trabajo y menos recursos, con la intención teórica de satisfacer más eficientemente las necesidades.

El otro capitalismo era el que consideraba que la riqueza era la cantidad de monedas de un grupo (riqueza monetaria), y su finalidad era la de conseguir el mayor número de monedas posibles controlando a su vez el valor de estas monedas para que éste no se devaluase, y permitiese a los poseedores de monedas acceder al mayor número de recursos reales que fuera posible.

Resulta evidente que en la vida cotidiana ambos capitalismos iban a ir de la mano, ya que el "capitalismo útil", el que trataba de producir más riqueza real utilizando la menor cantidad de recursos posible, evidentemente sería utilizado y absorbido por el "capitalismo monetario", que era el que trataba de conseguir muchas monedas invirtiendo el menor número de monedas posible.

- - -

## 4.2.- PROBLEMAS CON LA MONEDA

A continuación iremos viendo la aparición de los problemas creados por el uso de las monedas de oro, y la actitud o respuesta de la economía capitalista ante cada uno de los problemas anteriores, para entender la evolución de la economía desde las monedas de oro hasta la economía actual.

Los problemas que citaremos pueden clasificarse en diferentes categorías, según afectan a la percepción personal y moral de los individuos, a las relaciones sociales del grupo, o al funcionamiento mismo de la economía. Los llamaremos problemas morales, problemas sociales, o problemas económicos, según afecten a una u otra de estas esferas, aunque todos están muy relacionados entre sí, por lo que dichas categorías pueden ser obviadas o modificadas, aunque preferimos utilizarlas para facilitar la comprensión.

- - -

### 4.2.1- Problemas morales o personales de las monedas
Estos son los problemas que encontrarían los individuos concretos en su vida personal, y respecto a los cuales eran ellos quienes debían decidir cómo afrontarlos.

- - -

- __A) Primer problema: Olvido de las necesidades reales, e interés exclusivo por el oro.__
 En primer lugar, empieza a ser transformado el concepto de riqueza, ya que en lugar de atenderse a "la capacidad para satisfacer las necesidades", se empieza a atender a "la cantidad de oro que se posee" para medir la riqueza.

 Se deja de atender tanto a las necesidades reales de las personas como a su capacidad para satisfacerlas, y en su lugar, se empieza a considerar que quien consiga mucho oro, tendrá mucha riqueza, y podrá conseguir todos los productos o servicios que quiera a cambio de ese oro, y con todos esos productos podrá satisfacer todas sus necesidades.

 Mientras todos los miembros de dicho grupo acepten la tabla de intercambio del oro, bastará con producir oro, o tener oro, para satisfacer todas las necesidades, y esto es algo que sabían todos los individuos.

 Evolución del capitalismo: Cambio del trueque por la compra-venta, e inmoralidad de los procesos productivos.

 El capitalismo útil no pierde de vista la utilidad práctica de las monedas, conservando la atención sobre los resultados en forma de productos, y empieza a considerar a los humanos como un recurso productivo más, ya que al usar las monedas para contratar los servicios de los trabajadores (intercambio de monedas por un servicio), dicho trabajo empieza a ser considerado una simple mercancía que puede comprarse, dejando la condición humana en un segundo plano mucho menos importante que la obtención de beneficios, ya sean estos beneficios unos recursos reales o unas monedas de oro intercambiables por dichos recursos, ya que incluso los recursos reales pueden ser vendidos y transformados en monedas de oro.

 Los productos del trabajo empiezan a ser considerados "mercancías"  que deberán ser vendidas (cambiadas por oro) y posteriormente, ese oro será usado para comprar el recurso realmente necesario (lo que llamaremos la riqueza real).

 En los tiempos más antiguos, se intercambiaban sobre todo productos, ya que la mayoría de los trabajos eran realizados por esclavos, pero como los esclavos suponían un coste en alimentación, alojamiento y vestido, supondremos que ese era el precio que recibían por trabajar, sin profundizar sobre la ausencia de todo tipo de derechos como persona.

 Con esto empieza a aparecer gente que se dedica a producir productos, o a organizar la prestación colectiva de servicios, pensando únicamente en su "venta", y esto a su vez hace que los productores consideren como imprescindible "la necesidad de vender", ya que la venta será la única finalidad de estas actividades económicas.

 Ya no se produce para "satisfacer las necesidades", sino para "vender"", y esto hace surgir un nuevo tipo de comerciantes, ya que empieza a ser una cualidad importante del comerciante la de "ser capaz de vender algo, aunque no sea necesario para quien lo compre".

 Un comerciante tradicional prestaba el servicio de transportar las cosas del lugar donde se producían al lugar donde eran necesarias, pero este nuevo modelo de comerciante presta el servicio de "vender cualquier producto a cualquiera en cualquier lugar, mientras pague el precio exigido, le resulte necesario o no al comprador". Su trabajo consistirá en hacer creer a los compradores potenciales que necesitan el producto que el comerciante vende.

 De este modo, las personas empiezan a ser consideradas "trabajadores" en el plano productivo y "clientes" en el plano consumidor, los recursos empiezan a considerarse "mercancías" desde que son fabricados únicamente para su venta, y empieza a apreciarse la aparición de una clase de "ricos" cuya peculiaridad más valiosa para el comercio es que compran cosas que realmente no necesitan, lo que ayuda al flujo de las mercancías.

 El capitalismo monetario, por su parte, se centra más a fondo en las monedas como recurso esencial, y empieza a calcular la rentabilidad de sus recursos (sobre todo de las monedas) atendiendo al número de monedas invertido al principio del trabajo y el número de monedas obtenido al final del trabajo.

 Ya no se preocupa de si el producto obtenido y "vendido" es de calidad, si el proceso de producción es sano, o si la actividad económica desarrollada es moralmente aceptable. Sino que es capaz de despreocuparse totalmente del proceso productivo y atender únicamente a la obtención de monedas.

 En este contexto, "vender" sería "intercambiar un producto o servicio por monedas", y "comprar" sería "intercambiar monedas por productos o servicios", por lo que el valor de las cosas deja de ser un simple concepto para convertirse en una realidad física, y por lo tanto empieza a ser considerado un recurso más en su forma de "monedas".

 El uso de los conceptos "compra" y "venta" es el que termina de separar en la mente de todos el acto del intercambio que tenía lugar cuando alguien entregaba algo para recibir otra cosa, ya que se tenía asumido que un trueque tenía que ser un negocio entre dos personas, y al hacerse compras y ventas, el uso de las monedas permitía implicar a más de dos en el ciclo económico de los recursos intercambiados, por lo que no existía trueque, y el sistema monetario solo consideraba movimiento de riqueza aquellos negocios que se hacían usando monedas, lo que también excluía el trueque directo.

 Esto supuso la muerte conceptual del "intercambio colectivo", también llamado "trueque multilateral" o "permuta de futuro", que era el trueque en el que una persona entregaba algo y en vez de recibir otra cosa en el momento, simplemente conservaba el "derecho a exigir una cosa", sin necesidad de saber qué cosa sería ni a qué persona concreta se le exigiría, ni cuando lo haría, aunque también podía saberlo y seguiría siendo una "permuta de futuro".

 Esa fractura de la realidad social que supone convertir el negocio único del intercambio en una compra y una venta separadas, ayudó a que ambos capitalismos se perdiesen cada vez más en el mundo de las monedas, porque las monedas dejaron de ser un instrumento para los intercambios y pasaron a convertirse en el elemento esencial de cada compra y de cada venta: el precio.

- - -

- __B) Segundo problema: Aparecen personas que dejan de producir riqueza real, y únicamente aspiran a conseguir oro:__
 En segundo lugar, como todo el mundo cree en la posesión del oro como verdadera riqueza, todo el mundo está dispuesto a entregar sus productos y servicios a quien les entregue oro, y eso permite que las personas atribuyan una importancia mayor a conseguir oro que a conseguir los productos o servicios, y algunos se centran exclusivamente en conseguir oro, sin preocuparse de producir ningún otro producto ni prestar ningún otro servicio, lo que llevaría a reducir la producción de riqueza real, si bien, permitía también que la riqueza existente se moviese de manera más rápida y eficiente, por lo que con menos riqueza real, era posible mejorar el nivel de vida, o al menos, el nivel de vida de aquellos que manejaban el oro, y con dichas mejoras...hacer que este problema no fuese apreciado por la sociedad.

 Evolución del capitalismo: Aparecen los empresarios y los banqueros:
Ambos capitalismos toleran esta situación sin darle mayor importancia, ya que parece sensato que toda persona trate de conseguir el mayor número de monedas posible, para satisfacer sus necesidades con la mayor comodidad posible.

 Con el tiempo este tipo de situación daría lugar a la aparición de los empresarios y los banqueros, y tanto la avaricia como el egoísmo terminarían considerándose cualidades naturales y totalmente comprensibles del ser humano, ya que sería lógico que cada vez más personas aspirasen a hacerse empresarios o banqueros, o en última instancia, a conseguir monedas.

 En el capitalismo útil aparecería el empresario capitalista, que trata de generar monedas a base transformar o comerciar productos y servicios vendiéndolos a un precio mayor del que le cuesta producirlos o prestarlos, justificando su beneficio en el servicio de coordinación que presta a sus trabajadores, y en el riesgo que asume por ser quien aporta el capital inicial en monedas.

 También los productores irán convirtiéndose en empresarios al especializarse en producir algún recurso concreto, confiando en venderlo y obtener monedas que le permitan comprar los demás recursos que necesita y no produce.

 Resulta curioso ver ahora, desde este prisma, que se pagaba al empresario por el riesgo que suponía la aportación de monedas, cuando dichas monedas no eran en realidad necesarias para desarrollar ningún trabajo, sino que lo necesario eran los recursos y la fuerza de trabajo de los trabajadores; Pero como la gente solía entregar tanto los recursos como su fuerza de trabajo a cambio de esas monedas confiando en poder cambiarlas por los recursos que necesitaban, cualquiera podía pensar fácilmente que el empresario estaba arriesgando algo al adelantar las monedas, aunque todos supieran que más tarde se recuperarían las monedas al vender los productos del trabajo, o en el peor de los casos, el empresario habría conseguido para sí todos los productos producidos, aunque se viese con el problema de no poder venderlos, lo que ahora suponía un problema también para quienes producían cosas sin necesitarlas y sin estar seguros de si alguien las necesitaba.

 El capitalismo útil permitió que la figura del empresario se elevara por encima de la de los demás trabajadores, y con el tiempo le permitiría apoderarse del trabajo de los demás con el pretexto de que él era quien arriesgaba las monedas iniciales, quien coordinaba el proceso productivo y quien se encargaba de vender los productos, aunque con el tiempo también podría terminar contratando a alguien para que hiciese dichas tareas a cambio de monedas, siempre que el coste fuera inferior al beneficio.

 Resultará evidente que estos empresarios, al tratar de pagar siempre el menor número de monedas posible por los recursos, tratarán de abaratar el precio de los servicios que contrata, y al llegar la revolución industrial y los avances tecnológicos, tratará de reemplazar a sus trabajadores por maquinaria y tecnología que realicen sus funciones, con la finalidad de seguir abaratando los costes de producción.

 La rentabilidad del trabajo colectivo se la apropiará el empresario capitalista en lo que se conoce como "plusvalía", que es el valor añadido a consecuencia de un trabajo realizado sobre un producto.

 Como el empresario contrata el trabajo individualmente, paga el trabajo como si fuera realizado de manera individual, pero se adueña del valor añadido o producido con el mismo, y también del margen de beneficio extra que supone el trabajo colectivo. A cambio solo entregará una cantidad determinada de monedas al trabajador que presta el servicio, manteniendo al trabajador en un nivel de vida equivalente al del trabajo individual.

 A medida que las herramientas, la maquinaria y la tecnología permiten multiplicar la productividad del trabajo, este incremento pasa a manos del empresario de manera exclusiva, haciendo que los avances sociales solo beneficien a estos empresarios, manteniendo a los trabajadores asalariados percibiendo los mismos salarios, o incluso a veces salarios más bajos, ya que hay avances científicos que permiten prescindir de determinados trabajadores, creando una abundancia de trabajadores disponibles, lo que, como recurso productivo que son para este sistema, abarata los salarios (que son el precio de los trabajadores) cada vez más. Así, cualquier mejora científica o tecnológica que beneficiase el proceso productivo permitiendo reducir la jornada de trabajo, en vez de mejorar las condiciones del colectivo que trabajaba, solo beneficiaba al empresario, que podría desarrollar el mismo proceso productivo con menos trabajadores que seguirían trabajando como cuando trabajaban sin formar parte de ningún colectivo de trabajo.

 Esto permitía al empresario incrementar mucho sus beneficios en monedas.

 Un tipo de empresario fue el comerciante, que era aquél que, sin producir nada, prestaba el servicio de llevar los productos desde el lugar donde se producían hasta el lugar donde se necesitaban, aunque ahora los llevaba al lugar donde pudieran ser vendidos. Estos comerciantes también hacían fortuna invirtiendo su dinero en comprar grandes cantidades de cosas a un mismo empresario, lo que venía a ser para el empresario como contratar a un vendedor al que se le pagaba su trabajo con una reducción en el precio de venta.

 Este comerciante podía posteriormente vender los productos a un precio mayor del que había pagado por ellos, obteniendo así un beneficio en monedas, aunque al menos el comerciante desarrollaba un trabajo de distribución de los productos.

 El problema del empresario se repetiría en el comerciante cuando su red de distribución creciera, y necesitase empleados a los que aplicaría las normas del capitalismo útil, acumulando gran cantidad de beneficios en monedas y dando lugar a los actuales centros comerciales.

 El capitalismo monetario juega más fuerte aún, ya que no hace más que reducir las funciones del empresario, suprimiendo su intervención en la producción de riqueza real, y permitiendo que un empresario solo aporte monedas a un proceso productivo, recibiendo finalmente más monedas de las que aportó al principio, sin haber realizado trabajo alguno.

 Estos empresarios del dinero serán los que llamamos banqueros, aunque también se les puede llamar prestamistas o accionistas.

 El banquero solo entregaría dinero a cambio de que en el futuro no se le entregase el mismo dinero, sino más dinero del que había entregado, y así obtenía fácilmente un beneficio en monedas, y podía hacerlo sin necesidad de trabajar ni de producir ninguna riqueza real.

 Esto podría ser mucho más grave si resultasen ser los banqueros quienes se encargaban de emitir las monedas que luego prestaban, pero al menos en los primeros tiempos, eso no sucedía.

 Se podría distinguir llamando "prestamistas" a quienes hacían las funciones del banquero sin tener la legitimidad para acuñar monedas, y banqueros a quienes prestan dinero que ellos mismos emiten.

 Evidentemente, estos banqueros y prestamistas, hacían su negocio también a costa de los empresarios del capitalismo útil, a los que podían ver del mismo modo que aquellos veían a sus trabajadores, es decir, como un recurso más para la obtención de monedas.

 Se le prestaba 100 monedas a un empresario para que iniciara una actividad económica, y a cambio, se le exigía que devolviese 120 monedas, independientemente del éxito o fracaso de su aventura empresarial.

 Así veríamos aparecer en la historia a comerciantes, terratenientes, jornaleros, obreros, trabajadores, banqueros, prestamistas, empresarios, plebeyos y la ingente cantidad de nombres que reciben los seres humanos atendiendo al papel que desempeñan en la economía de su época... todos ellos incorporando el juego de las monedas a sus vidas personales.

 Estos papeles en el juego capitalista son aceptados por cada persona de manera individual, y cualquier persona, de forma individual, puede renegar de ellos.

- - -

### 4.2.2- Problemas sociales
Los problemas sociales serán aquellos que superan la escala del
individuo para tener una repercusión sobre la totalidad del grupo en el
que aparecen.

- - -

- __C) Tercer problema: El oscurecimiento de la contabilidad:__
 En tercer lugar, se ignora el problema de conocer la riqueza real de un grupo, ya que se empiezan a ignorar aquellos intercambios que tienen lugar sin usar oro, y además se añaden otros problemas adicionales, como es el de empezar a igualar contablemente la cantidad de oro de un grupo a la cantidad de recursos que posee dicho grupo.

 Esto puede parecer algo sensato, pero solo lo es cuando se piensa a nivel de grupo aislado, ya que es cierto que si en un grupo cerrado existen 100 monedas de oro, toda la riqueza de ese grupo valdría 100 monedas de oro, y en base a esa proporción se establecerá el valor de las cosas.

 Imaginando el caso de que toda la riqueza del grupo se mueve de unas manos a otras en el mismo momento, cada uno de los bienes que se pueden comerciar debería tener una moneda utilizable para cada intercambio.

 Si ese grupo de 100 monedas de oro, solo tiene 100 sandías, cada sandía valdrá una moneda de oro en ese grupo, pero si ese mismo grupo tiene 1000 sandías y 100 monedas de oro, cada sandía valdrá 0'1 moneda de oro, por lo que según la tabla del oro, habrá que cambiar las monedas de oro por monedas de plata, y cada sandía valdrá 1 moneda de plata (1 oro = 10 plata).

 De esta manera, el productor de sandías verá caer el precio de su riqueza real (las sandías) por la sencilla razón de que alguien introduzca monedas en su grupo, sin alterarse la riqueza real del grupo, y se vería reducido el valor contable de los productos concretos que componen esa riqueza real.

 Del mismo modo sucederá si se altera la cantidad de sandías, ya que si aumenta el número de productos, se reducirá el valor de los mismos, y si se reduce el número de productos, su valor aumentará.

 Esto, a largo plazo, llevará a considerar la escasez de riqueza “real” como un beneficio contable, al menos para quienes entendían la fluctuación de los precios y querían mantener elevado el precio de aquellos productos concretos que ellos vendían.

 A esto debía sumarse el problema de que las monedas de oro y plata tenían fijado un peso determinado, por lo que el valor de las monedas debía coincidir con el valor intrínseco de ese metal con el que estaba fabricada la moneda, y esto resultaba complicado si se tenía en cuenta que la cantidad de metal contenido en la moneda era invariable y, sin embargo, el valor práctico que tenía la moneda atendiendo al número de monedas en circulación y la cantidad de riquezas reales existentes hacían fluctuar el valor de las monedas, por lo que el valor de las monedas contenía ciertas paradojas que tardaron mucho tiempo en señalarse.

 Evolución del capitalismo: Fomento de la escasez y acumulación de riqueza en las manos de los empresarios y banqueros.

 Tanto el empresario del capitalismo útil como el banquero del capitalismo monetario empiezan, con este sistema capitalista, a acumular gran cantidad de monedas que vuelven a invertir para conseguir más monedas, desarrollando un juego competitivo en el que todos quieren tener más monedas que los demás, aunque en vez de usarlas para satisfacer sus necesidades, las usan para seguir multiplicando sus monedas a costa del trabajo ajeno.

 Esto les hace interesarse por el valor de las monedas, así como por el valor de sus productos, y crean una escasez artificial de recursos reales para poder subir los precios de sus ventas, lo que frena el crecimiento de la riqueza real, pero aumenta sus riquezas en monedas, bien aumentando la cantidad de monedas que acumulan (cuando hay más monedas que riqueza real), o bien aumentando el valor práctico de las mismas (cuando hay más riqueza real que monedas).

 La clave era elegir el momento adecuado para comprar y el momento adecuado para vender, atendiendo a esa fluctuación en los precios.

 Esta escasez incluirá tanto acciones que lleven a destruir productos, alimentos o bienes necesarios, con la única finalidad de vender más caros los que no sean destruidos, pero también llevará a producir menos productos de los que resultaba posible producir, o incluso a producirlos con la intención de que se rompan o dejen de ser útiles en un tiempo determinado, para asegurar precios elevados, nuevas ventas en el futuro, y la obtención continua de más monedas. Cuantas más
monedas acumulan, más ricos son en este sistema capitalista.

 Al evolucionar los negocios capitalistas y acumular gran cantidad de monedas, empezarán a adquirir las riquezas reales de los grupos, dejando al resto de la población cada vez con menos recursos reales disponibles, ya que dichos recursos reales solo les serán entregados a quienes tengan monedas para intercambiar.

 Estos momentos de "recuperación monetaria" son causados porque los capitalistas se ven con la práctica totalidad del dinero en su poder, y antes de que el resto de la población entienda la realidad, rápidamente se dedican a comprar riquezas reales cuya propiedad se adjudican definitivamente, y así vuelven a entregar monedas al resto de la población, para que el sistema siga funcionando.

 Otra opción es emitir nuevas monedas para que la gente no se percate de la apropiación de todas las monedas por parte de los capitalistas, pero eso no hace más que retrasar la "recuperación monetaria efectiva" sin atribuir riquezas reales a buenos precios para la clase capitalista, ya que esa emisión de moneda ayuda a mantener los precios en un nivel más estable que si se deja aparecer la crisis por ausencia de monedas en circulación.

 Estos problemas podían no afectar a aquellas personas que seguían utilizando el trueque puro, pero éstas cada vez eran menos, ya que aunque tratasen de seguir usando los intercambios de riqueza real por riqueza real, cada vez les resultaba más difícil encontrar personas que no exigiesen la entrega de monedas a cambio de los productos o servicios.

 Quienes no tienen esas monedas y se dedican a trabajar se verán obligados a producir riqueza real para entregarla a quienes tengan monedas, ya que cada vez, serán menos quienes recuerden que podían intercambiar esa riqueza directamente por otra riqueza, y todos se habrán acostumbrado a cambiarlo todo por monedas. Muchos de ellos terminarán conservando como única riqueza real su propia fuerza de trabajo.

 De esta manera, sin darse cuenta, los miembros del grupo en el que aparecen los empresarios y banqueros irán quedando a merced de la abundancia o ausencia de productos y monedas de oro, así como del valor de estas últimas, y con ello empezarán a prestar sus servicios ya vender los productos cada vez por menos monedas a quienes se dedican a acumular monedas.

 Los empresarios y banqueros aprenden que para comprar servicios y productos baratos es bueno crear una escasez de monedas (o abundancia de recursos) entre los vendedores, y que para vender caro, es bueno crear una escasez de productos (o abundancia de monedas) entre los compradores.

 Del mismo modo, les interesaba que hubiese entre la gente no capitalista abundancia de aquellos productos que necesitaban comprar, por lo que podían ayudar a quienes fabricaban tales cosas a iniciar actividades productivas en dichos sectores para crear un exceso de oferta. Y la abundancia de moneda entre la gente no capitalista... bueno, eso era inconcebible, ya que se sabía quiénes tenían la abundancia de moneda, y éstas rápidamente volvían a manos de los capitalistas a través de las redes de comercio y de los préstamos.

 Tampoco les preocupaba el desempleo de la gente, porque ello abarataría el coste de los salarios. Para poder hacer todo esto con mayor facilidad, necesitarían controlar directamente la emisión de monedas de oro y los medios de producción, incluyendo la maquinaria, la tecnología y la propiedad de los centros productivos, bien directamente, o bien a través de"empresarios" Aunque teniendo monedas... resultaba posible.

- - -

- __D) Cuarto problema: Creación de burocracia y órganos de poder:__
 La economía analizará estos problemas y resolverá establecer leyes sobre la fluctuación de los precios, o imponer leyes sobre la cantidadde monedas que puede haber en circulación en cada grupo, establecerá quién puede emitir monedas, o incluso podría dar normas sobre los precios a los que deben venderse determinados productos o servicios, todo ello siempre bajo la apariencia de que se hace "para proteger a la población de los incontrolables flujos de la economía". Esto llevaba a la necesidad de crear algún organismo con poder para decidir sobre las cuestiones económicas, como cuántas monedas debe tener un grupo atendiendo a la riqueza real de ese grupo, dejando la fiabilidad del sistema monetario en manos de ese órgano de poder.

 Esto, a su vez, atribuiría a la evolución económica una dimensión política, para poder controlar estos factores económicos que terminarían afectando al conjunto de la sociedad.

 Evolución del capitalismo: Surge el capitalismo político:
Ninguno de los dos capitalismos se opondrá a la aparición de estos órganos de poder, ya que este sistema les puede beneficiar, aunque deberán asegurarse de que sean empresarios y banqueros capitalistas quienes controlen estos órganos de poder, para poder ser ellos quienes controlan el valor de las monedas.

 Además, dado que la situación evoluciona favorablemente a sus intereses a consecuencia de que la inmensa mayoría de la población no entiende del todo lo que está sucediendo, tienen la garantía de que cualquier persona que ocupe dichos puestos de poder estará dispuesta a ayudarles en sus proyectos a cambio de monedas.

 Para ello, bastaría con introducir la filosofía capitalista en los sectores políticos de los diferentes grupos sociales, considerando a cada grupo político como a un empresario más, darle algunas monedas y hacer que empiece a jugar.

 Con esto se trasladaba a los órganos de poder aquella soberanía económica que cada persona tenía en un principio, y que les permitía establecer por sí mismos el valor de las cosas, o que más tarde les había permitido fabricar trozos de oro, y se permite a estos nuevos organismos económicos influir de manera decisiva en la economía, ya que podrán establecer el uso obligatorio de determinadas monedas y dictar leyes que pueden ser aplicadas por los mecanismos políticos de coerción, prohibiendo a las personas emitir monedas.

 Incluso podrá imponer por ley la obligación de entregar determinadas cantidades de monedas para el sostenimiento de las instituciones, incluso de esas mismas instituciones que se dedican a controlar la creación de monedas.

 Las herramientas económicas de los empresarios y banqueros podrán ser utilizadas tanto para la recaudación de impuestos y contribuciones a los ciudadanos, como para justificar los gastos y la contabilidad de los gobiernos. Esto obligaría a que con el tiempo, fuesen desapareciendo los reyes y fuesen sustituidos por gobiernos colectivos, ya que siempre resultará más difícil que se rebele con éxito un grupo completo a que lo haga un individuo aislado.

 Un grupo numeroso puede ser predecible con un alto grado de fiabilidad, pero un individuo siempre tendrá la facultad de sorprender y de cambiar su parecer de un día para otro. De esta manera, si un rey no aceptaba las normas del juego capitalista, sería eliminado de su trono.

- - -


### 4.2.3.- Problemas económicos
Los problemas económicos serán también problemas sociales en la medida que afectarán a toda la sociedad, pero se les llama económicos, porque ya serán problemas en los que no dejan intervenir a los individuos, aunque les afecten, sino que será la élite de empresarios, banqueros y políticos la que se encargará de su gestión, manteniendo a los miembros del grupo sin estos conocimientos e imponiendo ellos las normas con las que se irá desarrollando el capitalismo y, en consecuencia, evolucionando la sociedad.

Estos problemas serían gestionados por las autoridades económicas.

- - -

- __E) Quinto problema: Variación de la cantidad de oro:__
 A la vez que todos los problemas anteriores, hay un nuevo problema que no tarda en hacer su aparición, que es el aumento y disminución del oro, ya que existen nuevas producciones de oro y de metales preciosos, provenientes de las minas, o del comercio con otros grupos, o de las conquistas de otros grupos que poseían oro.

 Eso hace que la cantidad de oro pueda aumentar por la aparición o llegada de oro de las minas o de otros grupos, respectivamente, o que pueda disminuir cuando se gasta el oro comprando recursos a otro grupo, o se es atacado por otro grupo y desposeído de todas las riquezas, incluidos los metales preciosos.

 Evolución del capitalismo: Guerras por el control del oro, por el valor del oro, y la inflación:
El capitalismo no tardaba en darse cuenta, si es que no lo sabía desde antes de que sucediese, que el hecho de hacer monedas no suprimía la existencia de oro como recurso natural, aunque mientras estuviese hecho moneda perdiera su utilidad como oro real.

 De hecho, si analizamos fríamente el oro, veremos que es un recurso bastante inútil a efectos prácticos, aunque algunos no entiendan eso porque hoy día todo el mundo le atribuye un valor como mercancía y sirve para cambiarlo por cosas realmente útiles. Exceptuando su utilidad como conductor de la electricidad, o su cualidad de dureza como protección, poco partido se le podría sacar a un lingote de oro más allá del uso como ladrillo.

 Pero como se le asignó un valor como mercancía, y todo el mundo aceptaría entregar cosas útiles a cambio de oro, la gente hace que el oro resulte valioso para mucha gente, aunque la inmensa mayoría de los seres humanos puede sobrevivir sin él.

 El hecho de que el oro siguiese existiendo hacía que el oro continuase siendo extraído de las minas, y como en un principio las monedas tendían a cuantificar la cantidad de oro del que estaban hechas, resultaba fácil confundir una cantidad de oro determinado con el valor asignado a la moneda fabricada con dicha cantidad de oro. Por eso se le asignaba a cada moneda su "valor intrínseco".

 Así, había quienes en lugar de producir riquezas reales para conseguir monedas, prefirieron dedicarse a extraer oro de las minas, de los ríos, o de cualquier lugar donde existiese oro en estado natural, y dichos lugares empezaron a ser objeto de discusiones por aquellos que pretendían controlarlos para su exclusiva explotación.

 Quien encontraba un yacimiento de oro se volvía rico de la noche a la mañana, ya que estaba sacando riqueza oficial de la tierra, y toda esa riqueza por estar en su tierra, era suya. ¿O era de todos?, ¿O era dequien podía sacar el oro?.

 Independientemente de quién consiguiese quedarse con el oro de las minas o del río, dicho oro entraba rápidamente en el tráfico económico, ya que quien lo extraía no podía utilizarlo para nada más provechoso que para intercambiarlo. Y cuanto más oro aparecía, más oro había en circulación. Y lo mismo sucedía con la aparición de otros metales preciosos, hasta que se decidía fabricar monedas con ellos, y posteriormente institucionalizar los órganos legitimados para emitir monedas oficiales.

 Sumando estos factores, veremos que a medida que se introduzca oro  en la sociedad, la forma más útil de sacarle rentabilidad para el capitalismo será convertirlo en moneda, o al menos, usarlo como "unidad de valor" atendiendo a su peso, y así, cada grupo capitalista que va encontrando oro en su suelo, o extrayendo oro de alguna mina o de algún río, incorporará "moneda" a su economía.

 Por supuesto, esto lo hará sin preocuparse de si la riqueza real aumenta o no, ni tampoco por a qué ritmo crece, en caso de que crezca.

 Ya se vio antes que a mayor cantidad de oro, menos valor tendrá dicho oro para ese grupo, sobre todo si tiene mucha riqueza real, por lo que ese grupo podrá entregar oro a otros grupos que carezcan de oro a cambio de riquezas reales, y lo hará sin muchas complicaciones, sobre todo si los grupos que reciben el oro le atribuyen un valor mayor.

 Ese mismo oro podrá tener un valor mayor para un grupo en el que el oro sea más escaso, y sobren muchas mercancías producidas que sí puedan ser riquezas reales.

 Sobre este aspecto de las mercancías, conviene señalar que una mercancía no es un producto útil por sí mismo, sino que solo resultará útil en la medida en que pueda ser intercambiada, por lo que conviene entender que, para satisfacer las necesidades reales en el sistema capitalista es mejor tener riqueza real, pero para acceder a la riqueza real que no podemos producir siempre será mejor el oro, ya que el oro lo aceptará cualquier poseedor de riqueza real.

 Los excedentes de producción destinados a la venta serán mercancía porque no son útiles a su poseedor, y por eso éste decide venderlos.

 De esta manera vemos que la riqueza real puede ser un producto o un servicio útil para su poseedor, o puede ser una mercancía, mientras que el oro siempre será una mercancía cuya única utilidad es ser intercambiado.

 En este contexto, habrá grupos que prefieran conseguir oro en vez de tener que trabajar en algún oficio realmente productivo, y ahí vimos aparecer a empresarios y banqueros, pero ahora habrá que añadir a la historia la presencia de los buscadores de oro, y de los ladrones de oro, porque incluso los ladrones veían los cambios sociales, y entendían que era mejor robar oro que robar sandías.

 Estos buscadores y ladrones de oro, al igual que muchos se iban de sus tierras a buscar fortuna en el extranjero, terminarían uniéndose para salir de sus territorios a buscar y robar oro en nuevas tierras, formando lo que podríamos llamar la clase social de "los conquistadores".

 Los conquistadores eran aquellos que se iban a otras tierras a buscar oro, y si lo encontraban, lo cogían, bien por la fuerza, bien con argucias menos violentas, y así seguían incrementando la cantidad de oro que circulaba en sus economías.

 Como ejemplo de esto bastará con poner las guerras del imperio romano, las guerras medievales, o incluso la conquista de lasAméricas, en las que los vencedores se adueñaban del oro de los conquistados y lo usaban para comprar en sus economías.

 No hace falta decir que los conquistados podrían ser invitados o no a formar parte del grupo conquistador, pero fuesen invitados o no, entrarían a formar parte del grupo después de ser desposeídos del oro.

 En los grupos en los que el oro aumentaba de cualquiera de estas formas se procedía a acuñar nuevas monedas con el valor oficial, pero el valor de éstas monedas de oro empezaría a bajar al ser demasiado abundantes, y se llegaría a situaciones en las que el valor del metal convertido en moneda podría verse muy reducido por la sencilla razón de que cualquier persona podía conseguir esas monedas.

 Recordemos que poca gente acumulaba monedas, ya que su única utilidad era intercambiarlas, por lo que los negocios de los banqueros empezaron a ser cada vez más prósperos, ya que cada vez manejaban mayores cantidades de monedas.

 De otro lado, aquellos grupos que producían la riqueza real trabajando (ya sea como empresarios o como jornaleros), tenían cada vez más productos que terminaban como mercancía, ya que la división del trabajo y la especialización de cada persona en producir un único tipo de riqueza, hacía que cada cual produjese mucho de una cosa y nada de las demás, por lo que todas las demás cosas debían ser adquiridas entregando oro, y la única forma de conseguir oro que tenían era vendiendo sus excedentes en forma de mercancía.

 Para ellos, el oro resultaba valioso y útil, por lo que pedían oro a los banqueros, mientras que los banqueros acumulaban gran cantidad de oro y conocían la necesidad de oro que tenían los demás, por lo que el negocio estaba asegurado: Prestar 10 monedas de oro a cambio de que
le sean devueltas 11 monedas de oro.

 Mientras unos se iban quedando sin oro y necesitaban trabajar para cambiar sus productos y servicios por oro, otros iban acumulando todas las monedas de oro, e incluso el oro como recurso natural.

 La aparición continua de oro hacía que el valor de las monedas de oro cada vez fuese menor en aquellos grupos en los que se iba acumulando, y a la vez, iba siendo cada vez mayor en aquellos grupos que iban siendo desposeídos del oro, ya que éstos ya tenían asumido que "sin oro no podían acceder a la riqueza real para satisfacer sus necesidades reales".

 Como quienes acumulaban el oro veían que cada vez tenían que entregar más oro para adquirir las mismas riquezas reales, y eso les suponía una pérdida de poder adquisitivo (capacidad para adquirir productos o servicios a cambio de oro), llamaron a dicha pérdida "inflación", y se pusieron a buscarle soluciones.

- - -

- __F) Sexto problema: Aparecen diferentes tipos de oro:__
 Esta aparición constante de oro no acuñado, hace que existan dos tipo de oro: el "acuñado" y el "sin acuñar". Y dentro del oro "acuñado" empiezan a aparecer diferentes tipos de oro, según haya sido acuñado por un grupo o por otro.

 El oro "sin acuñar" sigue siendo un recurso real, pero la práctica llevaba a aceptar el oro sin acuñar como un recurso valioso similar a las monedas, siempre que se respeten las unidades de peso y medida establecidas en las tablas de intercambio, lo que con el tiempo acabará por crear cierta inseguridad en las monedas de oro de los grupos concretos, ya que se sabe que el valor de las monedas de oro puede aumentar o disminuir según la riqueza real de cada grupo, y que ésta también puede aumentar o disminuir atendiendo a la cantidad de oro existente en cada momento, por lo que las monedas de oro acabarían resultando menos fiables (debido a la inflación) que el oro sin acuñar, ya que el oro en estado natural seguirá siendo un recurso real sin ningún valor institucionalmente controlado, sino que tendría únicamente su "valor intrínseco", aunque éste valor siguiera siendo "el valor que quienes lo intercambian le quieran asignar".

 __Evolución del capitalismo__: Comercio internacional y el patrón oro:
 El capitalismo entendió que la existencia de diferentes grupos creaba diferentes monedas con diferentes valores, aunque todas estuvieran hechas de metales preciosos, así que hubo dos grandes soluciones a este problema.

 La primera vino de mano de los conquistadores, que acabaron entendiendo que comerciar podía ser más sencillo, barato y beneficioso que conquistar a través de la guerra, ya que la guerra tenía unos costes elevados, tanto en recursos reales como en oro, pero también en vidas.

 Las guerras, en segundo lugar, dejaban muy perjudicada la riqueza real del lugar conquistado, y el capitalismo ya sabía que necesitaba gente produciendo riquezas reales que pudieran ser compradas con el oro para que la posesión del oro fuese más fructífera.

 Y en tercer lugar, quienes eran conquistados a través de una guerra, siempre guardaban cierto deseo de venganza contra el conquistador.

 De manera que optaron por conquistar a través del comercio.

 Para ello se iba a los lugares que se quería conquistar con mercancías.

 Si aceptaban oro y lo necesitaban, se iba con oro directamente y se les compraban los recursos, se les contrataban los servicios, y el lugar quedaba rápidamente sometido.

 Si aceptaban oro, pero no lo necesitaban, se llevaban mercancías diferentes y se les entregaban a cambio de su oro hasta haber adquirido todo el oro del lugar. Y si no aceptaban el oro como recurso, por considerarlo inútil, simplemente se les ofrecía cualquier mercancía para que entregasen su oro, y aquellos aceptaban entregar esas piedras amarillas a cambio de pieles, espejos, o cualquier otra cosa que tuviera un mínimo de utilidad.

 De esta manera las monedas de oro se expandieron a través del comercio internacional, ya que una vez entabladas las relaciones comerciales iniciales, los conquistados no tardarían en acostumbrarse al modelo económico de los conquistadores que tantas comodidades les aportaba.

 La segunda solución tuvo como finalidad unificar todos esos comercios bajo el poder del oro, que a estas alturas de la historia ya estaba controlado por instituciones económicas y políticas, y está muy relacionado con el acuñamiento de las monedas, ya que se empezaron a aceptar papeles que garantizaban la entrega de una cantidad determinada de oro, y esto permitía usar papeles para realizar intercambios. Sería el origen del papel moneda, que se explicará junto con el problema de la acuñación de monedas.

- - -

- __G) Séptimo problema: Necesidad de acuñar monedas:__
 Ya se dijo que cuantos más recursos tenía un grupo, más monedas debería tener para contabilizarlos, lo que estaría muy bien si no fuese porque las monedas de oro debían fabricarse con metales preciosos, y los metales preciosos, tal y como se dijo anteriormente, eran un bien escaso, por lo que habría grupos que no tenían metales con el que fabricar las monedas que necesitan para contabilizar e intercambiar todas sus riquezas.

 De esta manera, algo que se creó para simplificar los intercambios (una moneda no falsificable), acabó por complicar la contabilidad de la riqueza real, y dificultó la fabricación y el uso real de sus herramientas contables a causa de la escasez de metales preciosos para fabricar monedas. __Y no tener monedas era no tener riqueza.__

 Evolución del capitalismo: Creación del papel moneda y Errores de
contabilidad:

 El capitalismo útil acaba entendiendo que la circulación de las indestructibles monedas permite que una misma moneda sea utilizada para varias operaciones de compra y venta, y que distintos intercambios puedan tener lugar usando las mismas monedas físicas.

 También empiezan a notar que no todos los productos y servicios existentes son intercambiados por monedas, sino que hay personas que producen para su consumo, o que intercambian a través del trueque puro, pero son una minoría. Estos tampoco necesitaban usar monedas.

 Al ver que no hacen falta tantas monedas como riqueza tenga el grupo, reducen el número de monedas en circulación y consiguen solucionar en parte el problema de acuñar tantísimas monedas aceptando que las mismas monedas físicas se usen una y otra vez para hacer circular las diferentes riquezas reales.

 Esto resuelve el problema del número de monedas que se necesitan acuñar, pero hace más difuso aún el método para conocer la riqueza real de un país, ya que resulta imposible saber en cuántos intercambios interviene cada moneda física, y el número de monedas físicas ya no guarda relación con la riqueza real del grupo, sino con "el número de intercambios realizados con monedas dentro de ese grupo", sin llegar nunca a coincidir, porque toda moneda se usaría más de una vez.

 Con esta medida podía seguir existiendo el problema de la desaparición de las monedas de oro, ya que el oro siempre podría ser fundido y nuevamente acuñado por otro grupo para fabricar otras monedas para otro grupo, de manera que sigue existiendo la necesidad de acuñar nuevas monedas para mantener en el grupo el número de monedas establecido por el órgano de poder, pero se reducía en parte el problema de tener que acuñar tantas monedas.

 Con el tiempo, se llegaría a una situación en la que, aunque pareciese que la riqueza crecía mucho, solo estaba creciendo la cantidad de oro y el número de veces que se usaban las monedas, sin atender realmente a si la riqueza real estaba creciendo o no. Pondremos un ejemplo:

- Si A y B cambiaban una sandía por un libro, eso era un intercambio sin monedas, y la economía oficial no registraba la existencia de ninguna riqueza. Pero si A vendía su sandía por una moneda a C, y luego A entregaba esa moneda a B para que B le entregase un libro, la economía oficial registraba la existencia de 2 operaciones por valor de una moneda cada una, por lo que se consideraba que había riqueza por valor de 2 monedas. Hasta ahí todo parecía correcto ya que el libro y la sandía valían una moneda cada uno, y existían 2 "monedas" (aunque solo se hubiese utilizado una de ellas, realmente existía riqueza real respaldando el valor de esas 2 monedas, que eran el libro y la sandía, por lo que la contabilidad parecía llevarse bien, aunque faltasen monedas físicas).

 La cuantificación de la riqueza no tardaría en confundirse más aún en cuanto A volviese a vender el libro por 1 moneda a B, y B repitiese la operación vendiendo nuevamente el libro a A, ya que la riqueza real seguirían siendo una sandía y un libro, pero el hecho de que fuesen usadas varias veces con los mismos bienes, hacía que cada vez apareciesen más monedas en la contabilidad oficial, y todas ellas teniendo como recursos reales el mismo objeto.

 Y de esta manera, la contabilidad llevada en monedas se alejaría cada vez más de la contabilidad real de la riqueza de un país, y esto se hacía con el beneplácito y la aceptación de todos los economistas, ya que ayudaba a solucionar el problema de la acuñación de monedas manteniendo su concepto de que la riqueza dependía de la cantidad de monedas de oro en circulación.

 En este sentido conviene señalar que la riqueza adicional que se contabiliza, bien podría tener su origen en el uso compartido del libro, que pasa de mano en mano, ya que un libro parece mucho más productivo cuantas más veces se usa, es decir, cuanta más gente lo lee, y el coste de producción de la segunda lectura y las siguientes es inexistente, ya que basta con el simple acto de la entrega del libro.

 Este detalle no se apreciaba en la contabilidad porque el acto de compartir o de enseñar (compartir ideas y conocimiento) también fue siendo poco a poco sustituido por la "venta individual". Pero este tema sobre el acto de compartir las cosas como fuente de riqueza, sobre la realidad o ficción de "la propiedad sobre las ideas" y sobre el concepto de "propiedad colectiva", será tratado en otro momento.

 El capitalismo monetario, por su parte, vuelve a ir un poco más lejos, y para solventar el problema de la escasez de oro, decide sustituir las monedas de oro por otro tipo de monedas que resulten más baratas de fabricar. Para ello, usará materiales que nadie necesite llevarse a su grupo por carecer de valor intrínseco, y ser totalmente inútiles fuera de su utilidad como moneda.

 En contraprestación a este abaratamiento, aumentarán los mecanismos para evitar su falsificación, para que dichas monedas sigan siendo ligeras, duraderas, escasas e "infalsificables". __¡E inventaron el papel moneda!__

 Evidentemente, a este nuevo tipo de moneda hecha de papel, seguirían aplicándole las normas de la contabilidad del capitalismo útil, ya que poca gente se percataría de que esta solución sumada a la anterior, no hacía más que consolidar y aceptar la existencia del problema contable de la riqueza y permitir su manipulación, solo que ahora se cambiarían los recursos, en vez de por monedas de oro, por puro papel.

 Parece complicado hacer que la gente crea que un trozo de papel es lo mismo que un trozo de oro, pero si la gente ha dejado de ver el oro para ver un simple "valor de ese oro", y en consecuencia, es capaz de ver únicamente "el valor" de ese oro, puede que no resulte complicado hacerle entender que "ese papel" tiene exactamente el mismo "valor" que el oro en un mercado determinado.

 ¿Cómo? Dirán algunos. Pues fácilmente. Si hay gente que tiene oro, y está capacitada para coger una cantidad de oro determinada y darle a ese oro la forma adecuada, podrá fabricarse un trozo de oro que posea el "valor" adecuado a las exigencias de cualquier incrédulo.

 Una vez se tenga fabricada la moneda, a ese incrédulo se le dirá:
 - Si me das un papel que diga" juro por todos los dioses que entregaré una moneda de oro a quien me entregue este papel" y lo firmas de tu puño y letra comprometiéndote a cumplirlo, yo te entregaré esta moneda de oro.
 - ¿Me estás cambiando papel por oro?
 - A lo que se le responderá:  Claro, ¿por qué no? Me lo volverás a cambiar cuando yo te lo pida.

 Y el incrédulo, que necesitaba oro en ese momento, firmó el papel, y así asumió que debía una moneda de oro y se llevó una moneda de oro a cambio de un papel. El oro no le servía para nada, excepto para intercambiarlo, pero para menos le servía el papel. El incrédulo simplemente pensaba en ir a cambiar el oro por algo que realmente necesitara. Y así empezó a creer que era posible convertir el papel en oro.

 Los alquimistas tratando de convertir el plomo en oro, y ahora cualquiera podía convertir un papel en una moneda de oro. __¡Simplemente increíble!.__

 Socialmente, este mismo truco se le hizo a mucha gente, y la gente aceptaba el oro como un intercambio más, que además parecía justo, porque aunque se asumía el deber de entregar oro, se había recibido oro también. Pero había otras partes de la historia que la gente no se planteaba, y era la que realmente estaba teniendo lugar a los ojos del prestamista.

 Los capitalistas, incluyendo a los banqueros, a los conquistadores (tanto bélicos como comerciantes) y a los empresarios, disponían ya de muchas monedas, por lo que estaban en posición de hacer el truco sin necesidad de fabricar monedas, ya que las monedas se generaban solas al cobrar interés sobre el dinero, al quitárselo a los buscadores, al conquistárselo a los extranjeros, o al apropiárselo del trabajo ajeno.

 Ellos podían prestar oro, pero no todos lo prestaban de la misma manera, ya que algunos lo harían como el personaje de la historia, pero había otros que lo prestaban exigiendo un interés, y esa forma, por ser la más rentable, fue la que se acabó poniendo de moda.

 Hubo quienes trataron de prohibir el cobro de intereses, pero el deseo de conseguir oro se impuso, y como pocos podían realmente prestar dinero a todos, al final, el que más tardaba en quedarse sin oro para prestar era el que lo multiplicaba cobrando intereses. Y ellos fueron los que más duraron desarrollando ese negocio, hasta que el cobro de intereses se convirtió en "la costumbre".

 Aquellos papeles acabaron siendo considerados como el mismo oro, pero tenían ventajas adicionales, siendo la más importante de ellas, que ese oro se podía "personificar", y escribir en él "qué persona concreta deberá entregar el oro al portador". Y también se podía escribir "qué persona debía recibir el oro". Y eso invirtió las posiciones.

 Ahora era la gente que conseguía oro la que, para guardar su oro, acudía a los banqueros y les entregaba su oro, y a cambio exigían un papel que tuviese la promesa de "entregar tal cantidad de oro a Don Fulano Detal y Decual". Y así se justificaba el cobro de intereses, además de por todo lo dicho anteriormente, por la prestación del servicio consistente en la vigilancia del oro.

 Ellos se encargarían de que el oro estuviese a buen recaudo hasta que el propietario, o la persona designada en el papel, llegase a retirarlo mostrando el papel.

 Como muchos de quienes depositaban su dinero no volvían nunca a recogerlo, bien porque morían, bien porque se iban al extranjero, bien por cualquier otra razón, se ampliaron las posibles anotaciones en el papel, que cada vez se parecía más a un contrato, y así terminaron diciendo que "el banco entregará al portador de este documento X cantidad de oro", y esos papeles serían el nuevo oro.

 Este oro de papel sería aceptado por cualquiera que tuviese oro guardado en el banco, confiando en que el oro siempre lo estaría guardando el banco.

 De esta manera se llegó al __patrón oro__, ya que los bancos emitieron un montón de papeles que daban derecho a retirar cantidades concretas de oro, y a dichos papeles se les asignaron diferentes nombres según el grupo que los emitía, y cada grupo podía hacer unos papeles que diesen derecho a unas cantidades de oro diferentes.

 En este sentido debe entenderse por oro, cualquier metal precioso que se hubiese utilizado para la acuñación de monedas, habiendo sido los más habituales el oro y la plata.

 Usando la cantidad de metal precioso garantizada con cada papel se podía tener un referente para la equivalencia entre los "valores" de unos papeles y otros, por lo que unos papeles podían cambiarse por otros usando el patrón oro.

 Dichos papeles solían circular junto a monedas acuñadas, pero las monedas se acuñarían de metales cada vez más baratos, y el oro se mantendría a buen recaudo, para el hipotético caso de que alguien decidiese canjear sus papeles o sus metales baratos por su correspondiente en oro.

- - -


- __H) Octavo problema: Devaluación del valor de la moneda:__
 Incluso, volviendo a la época de las monedas de oro, una vez que exista el organismo que regula la emisión de monedas y se sigan emitiendo monedas de oro, podría suceder que se emitiesen muchas monedas de oro, por tener más oro que cualquier otro recurso, y saber que con las monedas de oro puede comprarse cualquier cosa a los demás.

 Esto podría suponer un problema. Pongamos por ejemplo que se emiten 1000 monedas de oro en un grupo que tiene solo 10 sandías, haciendo que cada sandía valga 100 monedas de oro. Antes vimos que un grupo podía tener 1000 sandías y 100 monedas, haciendo que una sandía valiese 1 moneda de plata, y en el otro grupo valdría una sandía 100 monedas de oro.

 Aunque resultase evidente que 100 monedas de oro no eran lo mismo que 1 moneda de plata, había gente que aceptaba ambas realidades a la vez, siempre que fuesen en lugares o momentos distintos, porque los precios variaban a causa de la fluctuación de los mercados que ya se entendía como una ley natural e inevitable.

 Usando el papel moneda, estos precios ni si quiera dependerán de la cantidad de oro disponible, sino que estarían a merced de aquél órgano que decida cuántas monedas deben emitirse para toda la riqueza real, lo que permite manipular los precios de las cosas mucho más fácilmente que teniendo que inyectar o retirar oro de una economía concreta. Para inyectar oro hacía falta oro, pero para inyectar papel, bastaba con el papel.

 La clave estaba en que ya nadie se atrevería a emitir papel-moneda que no fuese "oficial", por miedo a los mecanismos de coerción de que disponían las instituciones económicas.

 De esta forma, podemos ver que habrá grupos que vuelvan a establecer valores y precios muy distintos para los mismos recursos, atendiendo a la escasez o abundancia de los recursos reales, o atendiendo a la abundancia o escasez de monedas, sin olvidar que las monedas son creadas por los organismos económicos oficiales, únicamente.

 Como lo normal es que los avances tecnológicos y el aumento de la población hagan que cada vez se produzcan más recursos, el valor de la moneda tenderá a subir por naturaleza, y al aumentar la población y la cantidad de recursos se podrían emitir cada vez más monedas, para compensar este desequilibrio entre monedas y riqueza real y evitar que los precios se vean alterados, aunque habría que valorar las ventajas e inconvenientes de que los precios bajasen.

 Cualquier grupo que vea que se queda sin monedas podrá acuñar más monedas mientras le resulte posible, y así mantener su existencia de monedas, pero cuando a un grupo van llegando monedas sin parar éstas empiezan a abundar, y la abundancia de monedas hace que su valor baje.

 Como con el tiempo se hará costumbre emitir cada vez más monedas, y teniendo en cuenta que contar las monedas conlleva a perder la cuenta de la riqueza real de cada grupo, resultará normal que el precio de la moneda sea cada vez más bajo, a medida que se vayan añadiendo monedas.

 También será previsible que se reduzca el crecimiento natural de la riqueza real, para mantener la apariencia de escasez que tanto beneficia a los capitalistas.

 Para colmo, estas nuevas monedas no necesitaban ser creadas físicamente, sino que resultaba suficiente con que fuesen contabilizadas por los organismos oficiales de la economía, ya que las mismas monedas físicas podían volver a utilizarse en cuanto volvían al órgano emisor, y esto sucedía con frecuencia, ya que al órgano emisor es al que se debían todas las monedas existentes (más el interés solicitado en cada nuevo préstamo de monedas).

 Esto devaluaba las monedas inevitablemente y hacía crecer la inflación de manera continua, haciendo subir los precios de las cosas

 Evolución del capitalismo: Control de la inflación a través del interés:

 Una vez se entiende la enorme probabilidad de que en una sociedad aparezcan nuevas monedas, sea de una manera u otra, se puede entender con facilidad que será necesario que la riqueza real crezca al mismo ritmo al que lo hagan las monedas para evitar desequilibrios, pero ya hemos explicado las razones que llevan a los capitalistas a frenar el crecimiento de la riqueza real, y la dificultad que existe para conocer ésta de manera exacta, porque aunque quisieran hacerlo no podrían. Por lo que ahora trataremos de centrarnos en las consecuencias de que aparezcan nuevas monedas.

 La primera consecuencia de la aparición de más monedas ya se mencionó que sería una subida de los precios, por la sencilla razón de que habiendo más monedas para la misma riqueza real, está claro que dicha riqueza valdrá más monedas que antes. Si antes había 100 monedas y de repente hay 110, se sabe que la riqueza total antes valía 100 monedas, y ahora vale 110.

 Si debemos atribuir valor a la riqueza real con esas monedas, resultará evidente que lo que antes costaba 10 monedas, ahora costará 11 monedas, y lo único que habrá cambiado será que de alguna manera han aparecido 10 monedas nuevas en la sociedad. Así que ,si la riqueza real hubieran sido 10 sandías, pues cada sandía habría costado antes 10 monedas, y ahora costaría 11 monedas. Esto es una simple subida del coste de las cosas.

 Para que dicho coste no subiese, sería necesario que la riqueza real, en vez de ser 10 sandías, hubiera crecido también, y que en vez de 10 ahora fuese de 11 sandías, y si la riqueza real crece al mismo ritmo que el número de monedas, esas 11 sandías podrían seguir costando 10 monedas cada una, sumando un total de 110 monedas.

 Con este ejemplo se entiende que emitir monedas puede ser una manera de mantener los precios de las cosas cuando se incrementa la riqueza real, pero debe tenerse en cuenta que, como se ha explicado anteriormente, conocer la riqueza real resulta una labor demasiado difícil en el sistema capitalista, por lo que será muy difícil emitir el número adecuado de monedas.

 Esto hace muy probable que el número de monedas que se emita sea inferior o superior al incremento de riqueza real, siendo las consecuencias diferentes.

 Si se emiten un número inferior de monedas, el precio de las cosas bajará, y con menos monedas se podrán adquirir más riquezas reales.

 Sin embargo, si se emite un número superior de monedas el precio de las cosas subirá, y con las mismas monedas se podrán adquirir menos riquezas reales.

 Como el único dato cierto que las instituciones económicas manejan en el capitalismo es el número de monedas que se emite, se puede entender que el precio de las cosas en una sociedad capitalista tiene forzosamente que subir, ya que los capitalistas lo que producen son monedas, nada más, y cuantas más monedas se les piden, más monedas emiten.

 Eso sirve de pretexto para que, cuando alguien tiene dinero pueda pensar que ese dinero cada vez le permitirá adquirir menos riquezas reales, porque el precio de las cosas sube, e incluso ayudará a que el dinero circule rápidamente, porque al conservarlo está perdiendo parte de su "valor".

 Esta será la devaluación del valor de la moneda, o "inflación", ya que como consecuencia de la pérdida de valor de las monedas se "infla" el precio de las cosas.

 Esta inflación es la que lleva a toda la sociedad a entender que si alguien nos entrega una cantidad de monedas, con el paso del tiempo debamos entregarle una cantidad de monedas superior a la que nos entregó, para compensarle por esa pérdida de valor de las monedas, y esta compensación por la inflación es lo que se conoce como "el interés".

 Al ser este interés un dato desconocido al momento de la entrega, quienes se prestaban dinero podían pactar el interés que estimasen conveniente, así que unos podían pactar un interés mayor, o un interés menor, siendo muy difícil que las partes acertasen de cuánto iba a ser la inflación, o cuál debería ser el interés justo, ya que resultaba difícil prever cuantas monedas nuevas iban a ser emitidas en cierto periodo de tiempo (y evidentemente, apenas se lleva la cuenta de la riqueza real existente, ni de su crecimiento o ni de su disminución).

 Con este razonamiento se aceptó la existencia del interés como consecuencia natural de la inflación, sin que nadie se preguntase por el origen de esas nuevas monedas que provocaban la inflación, ya que insistimos en que, si nadie emitiese monedas, a medida que creciese la riqueza real, los precios de las cosas solo podrían bajar, y el valor de las monedas solo podría subir. Y si solo emitiesen monedas aquellos 38que producen alguna riqueza real, el valor de las monedas podría permanecer inalterable a lo largo del tiempo.

 Pero como ya se sabe, eso no fue lo que sucedió en nuestra historia, y actualmente aún arrastramos el problema de la inflación y el interés como algo natural dentro del capitalismo sin entender muy bien de dónde surgen las monedas oficiales, ni cuantas hay en total.

- - -

- __I) Noveno problema: El oro moneda deja de ser oro, y se convierte en un apunte contable no escrito:__
 Otro problema que surge con las monedas de oro es que, con el tiempo, el oro usado como moneda dejará de ser considerado un recurso real para empezar a ser considerado una pura mercancía, por lo que se asume que nunca será algo que resulte útil, y sólo servirá para canjearlo por algo que sí resulte útil, como un apunte contable (algo parecido a un saldo positivo de riqueza que podemos gastar), lo que hace que se reduzca la consideración del oro como recurso real, ya que el oro, una vez hecho moneda, permanecerá siendo moneda durante bastante tiempo, por lo que dicho oro deja de contar entre la riqueza del grupo, para empezar a funcionar como una "libreta de operaciones contables", en la que cada moneda es un apunte contable a favor de quien la posee, pero con la peculiaridad de que el saldo negativo no aparece en ningún sitio, sino que es el grupo entero, a través de uno de sus miembros que acepte monedas, quien pagará con recursos reales el valor de esa moneda.

 Así, las monedas se emiten sin saber si existe una riqueza real que la respalde o no.

 Evolución del capitalismo: Cambio del respaldo con oro de las monedas por su respaldo con papel:
 Este mismo problema se había planteado por motivos diferentes, y se había acordado que, si el "valor" de las cosas se intercambiaba por el "valor" del oro", dicho "valor", tanto del oro como el de las cosas, podía hacerse constar en un papel, y como el del oro servía del mismo modo que el "valor" de las cosas, bastaba con atribuirle al papel el "valor" del oro.

 Pero esta solución de escribir sobre el papel se parecía a una especie de contrato, con la salvedad de que solo una de las partes conservaba una  copia de él, la única necesaria para presentar la exigencia de pago al otro. Pero seguía existiendo el riesgo de que esa copia se perdiese, como sucedía con el oro, de manera que quienes custodiaban el oro de los demás, empezaron a llevar libros contables con los saldos de sus depositarios, y con el tiempo surgiría también el servicio de "vigilancia de los papeles-moneda", que sería también prestado por los bancos. Esto fue un gran alivio para todas las personas que ahora tenían miedo de perder sus «papeles».

 Seguramente habrá que preguntarse si en esos libros lo que se consideraba saldo positivo era el oro, o era el papel. Porque no sería lo mismo anotar que alguien debe monedas de oro al banco porque el banco tiene papeles, que pensar que alguien le debe papeles al banco, porque no tiene oro para pagar, o que pensar que alguien le debe algún recurso útil al banco porque el banco le entregó oro, o que alguien debe riquezas reales al banco porque éste un día le entregó papeles.

 Ahí se iban confundiendo el "valor" de los papeles con el "valor" de las riquezas reales, y con el tiempo se entendería que quien creaba esos papelitos, tarde o temprano exigiría a la gente riquezas reales. Y que lo haría a precios bajos, por haber emitido muchos papelitos, y todo ello se haría sabiendo que dichos papelitos nunca serían suficientes para pagar el importe de todos los papelitos más el interés, ya que nunca se habían emitido los papelitos oficiales de saldo positivo que respaldasen la deuda creada por el interés, sino que solo se emitían papelitos suficientes para devolver únicamente el principal de todas y cada una de esas deudas.

 Cuando la emisión de moneda empezó a ser tan abundante que tampoco habría papelitos para llevar la contabilidad, se pasó al formato del apunte contable directo, suprimiendo la necesidad de entregar un papel que sirviese de resguardo, y permitiendo que cada persona tuviese una hoja de papel en la que tenia apuntado su saldo, y el banco tenía apuntado el saldo de todos aquellos con los que mantenía algún tipo de relación económica (casi siempre de carácter monetario).

 De esta manera, las operaciones pequeñas podían hacerse ante el banco, o fuera de él usando papelitos, pero las operaciones grandes tendrían que hacerse forzosamente con la intervención del banco, por ser muy improbable que alguien reuniese la cantidad de papelitos necesaria para realizarlas sin ayuda de los apuntes contables del banco.

 La gente haría operaciones pequeñas y grandes ante el banco, convirtiendo a éste en un notario económico capaz de conocer cada transacción económica  de los ciudadanos y dar fe de su existencia.

 Pero este nuevo tipo de moneda, consistente en el apunte contable, seguía siendo una moneda, de manera que seguía sin ver aquellas operaciones económicas que se hacían fuera de sus apuntes contables, y las autoridades económicas seguían sin conocer la riqueza real, ya que una pequeña parte de ésta se movía al margen de los bancos.

- - -

### 4.2.4.- El cambio moral, social y económico.

Este es un problema generalizado que afecta a todas las esferas, ya que es el que resulta del cambio estructural sufrido en la economía a consecuencia de todos los cambios anteriores.

- - -

- __J) Décimo problema: Se pierde de vista la naturaleza del intercambio:__
 Al usarse cada vez más el intercambio a través de la compra y la venta con monedas, surge una ficción social en la que da la impresión de que las personas necesitan siempre esas monedas, y cambian cualquier cosa por ellas, ya sean de oro, de papel, o sean simples anotaciones contables. Pero dicha necesidad no era real. 

 Lo que sucedía en realidad era que la gente en un principio usaba esas monedas para realizar intercambios, pero al acostumbrarse a tener las monedas, poco a poco se acostumbraban a considerar esas monedas como algo necesario, o como un recurso útil, en lugar de considerarlo como un vale, un ticket, o una promesa de que les van a devolver algún producto o servicio, porque es un simple apunte contable a su favor canjeable frente a la sociedad.

 De haberse mantenido así, la riqueza real habría seguido creciendo a un ritmo natural, porque nadie iba a preocuparse por acumular apuntes contables, sino que dichos apuntes se habrían ido gastando rápidamente por resultar evidentemente inútiles si no se cambiaban por algo.

 Nunca habría llegado nadie a pedirle prestados apuntes contables a otra persona que cobrase más apuntes contables de los prestados, porque lo que habría necesitado no serían apuntes contables, sino riquezas reales, y las habría conseguido del vendedor directamente por el precio, sin pagar interés, aunque haciéndose un apunte contable por dicho precio, insistimos, sin ningún interés adicional, pero lo habría hecho directamente en la cuenta de la persona a la que pide la riqueza real que necesita.

 La gente habría podido utilizar la idea de "valor" sin usar ningún material físico para representarlo, sin usar el oro, ni ningún otro metal, y sin papel. Los apuntes contables habrían sido pura información, y conociendo esa información se podría haber comerciado.

 El intercambio habría evolucionado hasta el punto de que alguien prestaba un servicio a otra persona y se anotaba un saldo positivo, y quien lo recibía se anotaría el saldo negativo correspondiente.

 Quien se anotaba el saldo positivo era porque ya había realizado la entrega que le correspondía en el intercambio, y quien tuviera el saldo negativo tendría pendiente de realizar esa entrega.

 Cuando los saldos volviesen a cero, uno habría entregado su parte, y el otro habría recibido su parte, pero hacerlo a través de esos apuntes contables facilitaba precisamente que los intercambios pudiesen hacerse entre muchas personas, que los saldos hubiesen resultado canjeables frente a cualquiera que usase este tipo de saldos, y no habría tenido que aparecer alguien que se llevase una cantidad de "valor contable" por cada anotación hecha, y hacerlo por la sencilla razón de "ser quien anota las cuentas".

 Claro que, habiendo surgido las instituciones monetarias, éstas podían haber entendido esto, y haber decidido usar este sistema de apuntes contables, y aún así, seguir cobrando un interés sobre determinadas operaciones. Y estando así las cosas, llegamos a nuestra época actual.

* * *

# 5.- LA ACTUALIDAD DE LA MONEDA
Con todos estos problemas pasando desapercibidos, se había admitido el uso de monedas, pensando que estos problemas podrían solucionarse si se seguía estudiando y mejorando el funcionamiento de las monedas.

Había aparecido una nueva realidad social, que era la moneda, pero que con el tiempo empezó a poner todos estos problemas sobre la mesa, y aparecieron los órganos de poder que desempeñaban estas funciones de acuñar las monedas, y de dar las leyes que regían sus mecanismos de seguridad, sus procedimientos para convertir las monedas de oro en recursos reales y mecanismos de protección para asegurar las monedas de los ciudadanos.

Cambiaron el oro por papel, y éste por anotaciones contables, y se hicieron con el control absoluto de la legitimidad para emitir y regular las monedas. 

Pero aún quedaba pendiente el último truco de magia capitalista, que sería el de "hacer desaparecer el oro que respaldaba todos esos papeles y anotaciones contables", y establecer la nueva "unidad de valor" que no necesitaba respaldo físico alguno en oro.

- - -

## 5.1.- LA DESAPARICIÓN DEL PATRÓN ORO

Ya vimos que la implantación del patrón oro para facilitar el comercio internacional tenía como objetivo esencial de su funcionamiento la convertibilidad de cualquier moneda a una unidad común, para poder transformar unas monedas en otras usando el patrón oro.

Esto podía parecer un simple avance en la concepción del dinero, pero era algo aún mayor, ya que a efectos prácticos suponía la creación de una federación de monedas, en las que las diferentes monedas ya no sólo eran admitidas como recurso, sino que pasaban a ser "el único recurso admitido en ese mercado", del mismo modo que había sucedido anteriormente al establecer la moneda como unidad de intercambio respecto a todas las riquezas. Ahora, esas monedas intercambiables por cualquier riqueza podían transformarse unas en otras en lo que se llamaría "mercado de divisas", y así cada grupo podía tener una moneda independiente, pero todos esas monedas podían circular en único mercado.

Evidentemente, la existencia de diferentes monedas, hacía que los valores de dichas monedas no fuesen iguales, debido a que todas las variables que influían en el valor de las monedas evolucionaban de manera diferente en cada grupo. Por lo que hubo que crear una nueva tabla en la que, en lugar de establecer tablas para convertir las riquezas en monedas y luego convertir esas monedas en riquezas diferentes, ahora se convertían esas monedas en oro para luego convertirlas en otro tipo de monedas.

Con esto surgió un nuevo tipo de negocio, que consistía en comprar monedas que aumentasen de valor, para venderlas cuando dicho valor se hubiera incrementado efectivamente.

Esto supuso un interés por conocer el funcionamiento de las demás monedas, ver qué se podía comprar con ellas, e incluso permitía retirar muchas monedas de un grupo convirtiéndolas a monedas diferentes, haciendo que en ese grupo escaseasen sus monedas. La escasez de monedas haría que bajasen los precios de las cosas, y con los precios bajos, volver a reinsertar las monedas para comprar a precios bajos.

Esto permitió que aquellas monedas que resultasen más utilizadas fuesen las que más conversiones practicaran, independientemente de que todas estuvieran permitidas, y debido a la expansión comercial de unos grupos frente a otros, habría unas monedas que se impondrían a los demás como referente de conversión, ya que con el tiempo, la gente se acostumbraría a establecer los precios de las monedas usando otra moneda, en lugar de usar el oro. Y del mismo modo que el oro se convirtió en el recurso por excelencia para los intercambios, ahora habría una moneda que se convertiría en la mercancía por excelencia para las compras y las ventas.

En la práctica se exigía que cada moneda de cualquiera de las diferentes economías implicadas tuviese un respaldo en oro, pero como dichos precios fluctuaban, generaban inflación y eran controlados por las tasas de interés, llegaría un momento en el que algunas monedas valiesen mucho, y otras valiesen muy poco, tal y como había sucedido anteriormente con determinados recursos.

Y del mismo modo que los antiguos productores de riqueza real sucumbieron al juego de las monedas, ahora los grupos que aceptaron este juego empezaron a sucumbir al juego del "mercado monetario".

Este mercado monetario que mezclaba a todos los mercados que usaban monedas sometidas al patrón oro, es lo que acabará convirtiéndose en el incomprensible fantasma de "Los Mercados".

La existencia de monedas que asumen el papel de principales hace que poco a poco las diferencias entre los valores de las diferentes monedas aumente, y las monedas más valoradas empiezan a desempeñar el papel de "los recursos más valorados", como había sido el oro, haciendo que la constante emisión de moneda, la constante inflación derivada de tales emisiones y el consecuente crecimiento continuo de los intereses (entendiendo intereses como "riqueza monetaria contabilizada no respaldada con monedas reales") hace que la preocupación se centre en conseguir las monedas principales, olvidándose de la cantidad de oro que respaldaba a las diferentes monedas.

Y así, todos los grupos empiezan a entregar sus existencias de oro a cambio de poder emitir más monedas, sabiendo que dichas monedas van a ser utilizadas como la moneda imperante después de aplicar la tabla de conversión monetaria.

Cuando los grupos se queden sin oro, podrán seguir pidiendo monedas al grupo que emite las monedas imperantes (que serán las que acepte el mayor número de grupos), pero este emisor de monedas, tal y como hicieron los banqueros, empiezan a cobrar unos intereses adicionales, y el juego se repite en un tamaño superior, dejando a unos grupos como los grupos capitalistas del mundo, y a otros grupos, como simples grupos productores, comerciantes y consumidores.

Todos esos cambios en el valor de las monedas, lleva a situaciones en las que el valor intrínseco de las monedas, incluso las de papel, empieza a no corresponderse con el oro que lo garantiza, de manera que hay monedas de plata cuyo valor respaldado en plata supone solo la mitad de la plata con la que está hecha la moneda, siendo más rentable fundir esa moneda que canjearla por su valor nominal en plata.

Cuando esta realidad empieza a hacerse evidente, los grupos empiezan a entender que la promesa de pago de los billetes no funciona por su respaldo en oro, sino por la confianza que la gente tiene en dichas monedas, ya sean de papel, de metal, o sean simples anotaciones contables, por lo que se decide abandonar el patrón oro, y acogerse directamente al mercado de divisas.

Esto podría verse como haber llegado finalmente al "trueque de dinero por dinero", con la salvedad de que la nueva unidad de conversión sería una unidad que estaba controlada por la voluntad y el capricho de unas personas concretas que tenían poder suficiente para emitir o retirar monedas de cada grupo, para aumentar o disminuir el valor de cada moneda, e incluso controlar mediante la influencia económica la producción de riqueza real, ayudando o fastidiando a aquellos sectores económicos que le agraden o desagraden.

La desaparición del patrón oro no fue simultanea en todos los lugares, puesto que no todos tomaron la decisión a la vez, y aunque muchos tenían claro que eso de usar el oro como medida de todas las cosas no tenía mucho de riqueza real, había otros que veían en el patrón oro el último resto de garantía para que la existencia de las monedas tuviera algo de comprensible, medible y controlable.

A principios del Siglo XXI el patrón oro había sido abandonado por todos, y la única economía oficial que existía era la que había dado en llamarse "economía de los mercados", por lo que en adelante hablaremos de "monedas" o de "dinero" entendiendo como tales la "unidad de valor abstracta de cada grupo social", aunque del mismo modo que el trueque puro fue conservado por algunas personas tras la aparición de las monedas, ahora seguirá habiendo un mercado del oro en el que se compra y se vende oro, pero el oro ha vuelto a ser una mercancía de la que no depende el valor de las cosas, sino que es el valor del oro el que depende de "los mercados".

- - -

## 5.2.- LAS REDES BANCARIAS Y LAS NUEVAS TECNOLOGÍAS

En esta nueva realidad en la que deja de existir el patrón oro, la banca se abre a sí misma un camino ancho y rentable en el que puede moverse con mucha más libertad de la que tenía cuando estaba sometida al patrón oro, y ello por varias razones.

En primer lugar, mientras existía el patrón oro, quienes guardaban el oro que respaldaba el dinero emitido debían mantener una cierta proporción entre el dinero emitido y la cantidad de oro guardado, por lo que el sentido común les hacía establecerse un límite de dinero que podían emitir, para que fuese proporcional al oro real que tenían guardado.

Dicho porcentaje pudo ser de un 10%, de un 20% o de un porcentaje diferente, según lo sensato que fuese el emisor del dinero, pero dicha cantidad seguiría creciendo a medida que la cantidad de beneficios obtenidos permitiese adquirir nuevas cantidades de oro para guardar, y de esa forma, aumentar el máximo de dinero que se podía emitir.

Este sistema se conocía como "sistema de reserva fraccionario", ya que solo reservaba una fracción del oro que realmente necesitaría para compensar todo el dinero emitido.

La desaparición del patrón oro supuso la desaparición de ese límite, por lo que se abrió la posibilidad de emitir todo el dinero que fuese solicitado, si bien resultaba necesario tener en cuenta lo aprendido sobre la devaluación monetaria y la inflación por la emisión excesiva de moneda, de manera que habría que tomar precauciones y asegurar que siempre que se emitiese dinero, éste fuese a ser pagado por alguien, y a ser posible, que ese alguien no fuese el emisor, ya que si el emisor tiene que pagar todas aquellas monedas que emite no emitiría tantísimas monedas. Evidentemente es un objetivo complicado, pero así son los capitalistas cuando de dinero se trata.

Para ello, la banca acudió a la externalización del servicio de préstamo, creando una red muy extensa en la que diferentes bancos pequeños dependientes del banco central quedaban autorizados a prestar dinero emitido por el banco central. Ese dinero se prestaría cobrando un interés a quien lo pidiera, y dicho interés siempre sería superior al interés que pediría el banco central a los bancos pequeños.

De esa forma el banco pequeño siempre tendría beneficios por prestar dinero, y todo ello se haría sin cuestionar la actividad económica del banco central, que obtendría sus beneficios de la simple emisión de dinero y su entrega a los bancos pequeños.

El desarrollo de las nuevas tecnologías, sobre todo en el ámbito de la información, pronto permitiría supervisar y controlar toda la red de bancos que emergería por todo el territorio, ya que ser uno de estos bancos pequeños se convertía en un negocio de beneficios asegurados, o al menos, lo sería mientras la gente pidiese préstamos y los pagase.

El trabajo de estos bancos pequeños consistía entonces en colocar el dinero emitido por el banco central, y garantizar el cobro de los mismos. El trabajo del banco central se limitaría a emitir dinero para prestárselo a los bancos pequeños y asegurarse la devolución de dicho dinero con sus respectivos intereses.

Los bancos pequeños estarían pagando un interés por un dinero creado de la nada, pero eso no les suponía un problema si ellos iban a cobrar un interés mayor a cambio de ese dinero que se les prestaba.

De esta forma, cada grupo se abriría un banco pequeño, y habría gente que montaría pequeños bancos independientes, y todos ellos se encargarían de garantizar el pago de los préstamos con garantías que ofreciesen quienes les pedían el dinero.

Como todo el mundo estaba acostumbrado a usar el dinero, y cuando no se tenía dinero la gente podía pedirlo prestado a un banco, el trabajo de colocación de dinero no resultó complicado, por lo que la red bancaria creció sin problemas.

Incluso aparecieron diferentes redes bancarias que competían por ser las que prestaban el dinero, aunque todas esas redes formaban parte de la red del banco central, por lo que en esencia podría entenderse que existía una única red controlada por el banco central.

Pero no debe olvidarse que podrían existir otros bancos centrales con otras redes diferentes, y serían dichas redes económicas competitivas el único problema al que podía enfrentarse un banco central.

Los bancos centrales no tardarían en darse cuenta de ese problema, y podrían haber empezado a cooperar entre ellos para evitar el conflicto, pero el funcionamiento del mercado, la economía, las relaciones entre los agentes económicos y el avance de la realidad social también hacían que las diferentes redes de los bancos centrales fueran entablando relaciones entre ellas, y podía suceder que antes de llegar a un acuerdo de colaboración, algunas de esas pequeñas redes bancarias hubieran sido eliminadas, bien por disposiciones legales del banco central que, poniendo requisitos difíciles de cumplir, impidiesen su existencia, o bien porque el negocio empezase a irles mal debido a un excesivo impago de los préstamos concedidos, con la consecuente imposibilidad de devolver al banco central el dinero que les prestó.

Ese crecimiento del dinero sin ningún límite, sumado a la posibilidad de contabilizar cantidades astronómicas a base de añadir dígitos a la contabilidad informática, pronto permitió la aparición de una gran cantidad de dinero contable circulante, pero a la vez, permitió que aquellas pequeñas cantidades que desajustaban la cantidad de dinero emitido y la cantidad de dinero existente, que suponían el interés de cada préstamo, también crecieran rápidamente.

Esto trajo como consecuencia inevitable la aparición de extensas redes bancarias por todos los grupos, y además, la aparición de deudas en forma de interés que, por la misma lógica del sistema, nunca podrían ser pagadas con dinero, por lo que terminarían siendo pagadas con riquezas reales.

De esta manera se puede entender que tanto el banco central como los banqueros pequeños verán recompensado su trabajo de emitir dinero, y de colocarlo en el mercado, con riquezas reales de la población, y todas esas riquezas se ganarían por el simple trabajo de practicar "anotaciones en cuenta".

- - -

## 5.3.- LA EMISIÓN PERSONAL DE DINERO

En el sistema de reserva fraccionaria era necesario tener una cantidad de oro que fuese al menos el 10%, o que fuese el 20% de la cantidad de dinero emitido, ya que el oro era la garantía de la validez del dinero, pero al suprimirse dicho sistema deja de haber oro respaldando el dinero emitido, y el respaldo del dinero empieza a ser el mismo dinero.

Esto lleva a una situación un poco absurda en la medida en que si todo el mundo que posee dinero tratase de canjearlo ante el órgano emisor, dicho órgano emisor no tendría nada que garantizase el valor de ese dinero, por lo que sólo podría entregar más dinero.

De una parte, eso no le supone un problema, puesto que está legitimado para emitir dinero, pero la gente que trata de canjear su dinero lo hace precisamente porque no quiere tener ese dinero, y quiere tener alguna otra cosa que le resulte más útil.

Si la gente creyese que por el dinero emitido por el banco, éste solo va a entregar una cantidad de dinero idéntica, evidentemente no aceptaría ese dinero como forma de pago, y muchos menos, estaría dispuesta a pagar un interés por él. De manera que habrá que ver qué es lo que le otorga algún tipo de validez material a ese dinero.

Ya se explicó que el dinero que se entregaba a los pequeños bancos se prestaba para que fuese nuevamente prestado a condición de ofrecer garantías de pago, por lo que dichas garantías de pago podrían cubrir, si no todo, al menos una parte del dinero, por lo que podría desempeñar el mismo papel que el oro en el sistema de "reserva fraccionaria", solo que ahora, aunque siga siendo solo una parte del dinero total emitido, puede servir para garantizar algo.

Dicho conjunto de garantías se compone, entonces, de todas las promesas de pago que los ciudadanos han emitido ante los pequeños bancos, y de las garantías ofrecidas por aquellos.

Eso puede servir para ofrecer una relativa tranquilidad a los poseedores de dinero, ya que al menos, les asegura que los bancos podrán reclamar todos esos pagos y convertirlos en riquezas reales para entregar algo a cambio del dinero.

Pero en este caso, si quien garantiza el valor del dinero no es el banco, sino las promesas de los particulares que lo pidieron prestado, habrá que comprobar si dichas riquezas reales resultan suficientes para cubrir todo el dinero emitido por los bancos, y posteriormente comprobar si los bancos estarían dispuestos a entregarnos esas garantías en algún caso a cambio del dinero que ellos han emitido.

Llegados a este punto podríamos seguir contemplando el equilibro económico como en la antigüedad, y confiar en que todo el dinero emitido, equivale a toda la riqueza existente. Pero para que eso fuese cierto, tendrían que estar todas las riquezas existentes ofrecidas como garantías de pago a los pequeños bancos, y eso está claro que no sucede mientras quede un sólo bien cuya entrega no haya sido prometida al banco.

De esto podemos deducir que no toda la riqueza existente respalda el dinero del banco, ya que con seguridad hay bienes que no están hipotecados, aunque puede que no sean muchos. Pero también habrá que ver si esas garantías ofrecidas por los particulares son efectivas y si están ya en condiciones de ser exigidas.

Para entender la naturaleza de estas promesas que respaldan el valor del dinero emitido por los bancos, hay que destacar su cualidad de "promesas", ya que eso es lo que son en esencia, aunque algunas sean susceptibles de acoplarse a algún bien concreto y convertir dicha obligación de pago en una cualidad de dicho bien, como sucede con los inmuebles hipotecados, ya que la promesa de pago de esa persona, en caso de no ser cumplida, podrá ser cobrada directamente con el inmueble. Son los llamados "préstamos hipotecarios".

Aunque atendiendo a las fluctuaciones en los precios del mercado, puede resultar posible que el precio de venta de dicho inmueble no resulte suficiente para pagar el importe de toda la promesa garantizada con él. O que haya que pagar varias deudas, sabiendo que un inmueble no resulta tan divisible como podrían resultar todos los billetes y monedas que garantiza.

También hay otras promesas que no llevan acoplada ninguna garantía real, como son los llamados "préstamos personales", y en esos préstamos la garantía ofrecida suele ser la simple promesa de la persona de trabajar en el futuro, generar el dinero necesario y entregarlo al titular del derecho a cobrar, que suele ser el banco. En este caso es la fluctuación del mercado laboral la que puede hacer posible que dicha persona se quede sin trabajo, y que en consecuencia no pueda pagar. O puede suceder que dicha persona se muera, en cuyo caso tampoco podrá pagar. O puede suceder que su salario se vuelva tan escaso que, aunque esté vivo y tenga trabajo, le resulte imposible pagar. Por lo que estas garantías serán menos fiables que las garantías hipotecarias.


De esta forma, vemos que existen "promesas de pago" hechas por los particulares a los bancos, y que dichas promesas son todo el respaldo en valor que tiene el dinero emitido por los bancos. Así que habrá que explicar de qué manera están sucediendo las cosas para que, siendo los particulares quienes ofrecen sus promesas de pago para posibilitar la emisión de dinero, resultan ser los banqueros quienes cobran el interés al emitir y prestar dicho dinero.

Para ello, pondremos un ejemplo de emisión para este tipo de dinero.

Supongamos que A tiene una casa y que B quiere comprar esa casa.

En una economía de trueque, B necesitaría tener algo que A quisiera, y debería darse la condición de que ambos atribuyesen un valor similar a cada una de esas cosas, para que pudieran trocarse sin perjudicar a ninguno de ellos.

Como ya se conoce la herramienta del dinero, sería posible que B entregase dinero a A para que A le entregase la casa, pero para ello sería necesario que B tuviese tanto dinero como "valor" acuerden entre A y B que tiene la casa.

Como B no dispone de ese dinero, tendrá que prometer que va a tenerlo en el futuro y que cuando lo tenga se lo va a entregar a A. Pero puede suceder que A no se fíe de la promesa de B.

Podrían buscar la forma de ofrecerse garantías, como por ejemplo, hipotecar la casa para el caso de que si el comprador no paga, el vendedor pueda vender nuevamente la casa y cobrarse la parte del precio no satisfecha.

También podría tratarse de generar una confianza mutua, o incluso de incluir a una persona o comunidad de la que A se fíe y que respaldase la promesa de B. Y de esa manera A entregaría la casa a B, y ambos realizarían una operación contable en la que A quedaría con un saldo positivo de 50 millones, y B con un saldo negativo de 50 millones.

Como existen los medios informáticos necesarios para ellos, A y B deciden realizar dicha operación.

Pero en nuestra sociedad, para hacer dicha operación es necesario irse a un banco, por lo que los personajes A y B se van a hacer la operación a un banco.

Una vez dentro del banco aceptan el uso de las monedas emitidas por el banco central, confiando en que dicho dinero está respaldado por las autoridades oficiales, y en consecuencia son aceptadas por toda la sociedad, por lo que exponen sus intenciones y el banco les dice que él adelantará el dinero y lo entregará al señor A anotándole en su cuenta corriente un saldo positivo de 50 millones.

Pero a B le explica muy por encima toda la historia de la emisión de monedas del banco central, de la subida de los precios a causa de la inflación y de la necesidad de cobrarle algún interés a cambio de entregarle el dinero para que pueda comprar la casa del señor A.

El señor B, que necesita la casa y que no entiende mucho esto que le cuentan del interés, pero que tampoco le parece mal que el banco cobre algo por prestarle el dinero, simplemente dice que de acuerdo.

El banco añade la condición de que el pago de la deuda, tanto del principal como del interés, quedará garantizado con la vivienda que B se quiere comprar, así podrá comprarla y pagarla en cómodos plazos, y si algún día no pudiese pagar, al menos estará la casa ahí para que el banco cobre.

El señor B vuelve a aceptar, y asume el pago de unos intereses y acoplar la vivienda que quiere a su promesa de pago, dejando la vivienda hipotecada.

Todo ello se hace por escrito, y en cuanto el señor B firma la promesa de pagar esos 50 millones más los intereses (es decir, ha emitido el documento que respalda el dinero que se va a emitir), el banco hace una anotación por valor de 50 millones en la cuenta corriente de A, y el negativo de 50 millones, más los intereses también en negativo, en la cuenta de B.

Como esta operación la repite una gran cantidad de gente, resulta muy
sencillo que 50 millones entren en circulación en el mercado mientras siga habiendo gente que se comprometa a pagar 50 millones más los intereses, así que cualquiera que pida un préstamo puede confiar en que de todo ese dinero emitido por los miles de préstamos que concede el banco, tarde o temprano caerá en sus manos el dinero suficiente para pagar la deuda completa, incluyendo los intereses.

Pero aquí es donde entra en juego el "problema de la caja única", un problema sin solución creado por los "intereses" del banco.

Hay que volver a mirar al banco central, que es la caja única de la que procede el dinero, independientemente o no de que este dinero se encuentre garantizado por riquezas reales, o por promesas personales de trabajo en el futuro.

Este banco central emite una determinada cantidad de dinero, y siempre que lo emite, espera que le sea devuelto con un interés añadido. Es decir, que si emite una cantidad X, quien lo recibe se compromete a devolverle X + Y, siendo Y la cantidad correspondiente a los intereses.

Como este banco quiere cobrar con dinero, los bancos pequeños tendrán la desconcertante misión de multiplicar ese dinero, pero tendrán que hacerlo sin que les esté permitido crear dinero, ya que si alguno de ellos quiere crear dinero, deberá pedirlo al banco central.

Suponiendo que los bancos pequeños devolviesen todo el dinero existente al banco central, solo podrían entregar X dinero, ya que es X el dinero efectivamente creado, y el dinero, una vez entregado al banco central, desaparece de la circulación.

Este banco central sólo presta dinero oficial a los pequeños bancos y solo acepta dinero oficial de los pequeños bancos, por lo que las entradas y salidas de dinero oficial están perfectamente controladas para que este problema de la caja única y el pago de los intereses no se pueda resolver.

Con el banco emisor no se puede utilizar dos veces el mismo billete para pagarles, ya que una vez entregado al banco central, dicho billete es apartado de la circulación, y sólo volverá a ser puesto en circulación si alguien vuelve a asumir la obligación de devolver ese billete, junto a una nueva cantidad en concepto de intereses. Y lo mismo que con los billetes sucederá con los saldos positivos que representen esos billetes.

De esta manera resulta evidente que el banco central nunca podrá cobrar todas sus deudas en dinero oficial, ya que no existe dinero suficiente para hacerlo, por lo que no le quedará más remedio que cobrar en riquezas reales.

Como los bancos pequeños han estado acumulando garantías de pago consistentes en bienes reales, hay una parte que saben que podrán pagar al banco central con dichas riquezas reales, y el resto, tendrán que confiar en que les sea pagado con el dinero que emitieron, aunque saben que ese dinero realmente no les sirve para nada si no se convierte en riquezas reales.

De esta forma, el banco central se asegura la adquisición de riquezas reales con el único esfuerzo de emitir el dinero de la nada, y cobrar un interés al prestárselo a un banco pequeño, ya que así, el beneficio obtenido de los intereses no puede hacer más que crecer.

Es cierto que dichos beneficios serán créditos contra los pequeños bancos, pero éstos siempre podrán haber tenido suerte y haber prestado el dinero a los particulares que consiguen hacerse con el dinero de los demás particulares para pagar las deudas completas, y así poder pagar al banco central.

Si un banco pequeño tiene mala suerte y sus clientes no le pagan, tendrá que convertir las garantías en dinero para ver si así consigue pagar sus deudas al banco central. Esto viene a ser como comprar esos bienes hipotecados con el dinero que se les adeuda, obligando a esos deudores a entregar sus riquezas reales a cambio de dinero oficial, lo que hace que el dinero oficial sea de gran utilidad para estos bancos, ya que les permite tener gente trabajando para ellos a cambio de dinero oficial, tener cedido el uso de los inmuebles que quieran a cambio de alquileres, y poder acceder a todos los bienes y recursos de la sociedad, a la vez que inyectan dinero en la sociedad para que ésta pueda seguir trabajando para pagarles lo que le deben en concepto de préstamo, incluyendo los intereses.

De esta manera veremos a los bancos pelearse entre ellos por conseguir a los clientes más solventes, y en caso de no haber clientes solventes, al menos conseguir el mayor número posible de ellos, confiando en que por estadística haya algunos que paguen sus deudas, sea de la manera que sea.

Si los clientes no pagan a los bancos pequeños, estos bancos se verán abocados a la desaparición, ya que no podrán pagar sus deudas y el banco central dejará de contar con ellos para el juego del dinero.

También podrán conceder moratorias en las que solo paguen el interés, o concederles nuevos préstamos con nuevos intereses, para ver si dichos bancos consiguen más garantías reales que puedan ser convertidas en dinero, o buenos avalistas, como podrían ser una empresa multinacional, o simplemente una entidad cualquiera que pueda gestionar e invertir el dinero de una gran cantidad de personas sin importarle las pérdidas, como podría ser un Estado.

- - -

## 5.4.- LA TITULARIDAD DE LA DEUDA

Aunque muchos Estados tienen montados sus bancos pequeños como "bancos centrales nacionales", eso no tiene por qué resultar incompatible con que un Estado cualquiera haya pedido préstamos a un banco pequeño y privado, y haya contraído deudas que no pueda pagar, o que simplemente prefiera no pagar, y negocie con el banco pequeño que le prestó el dinero.

Si el banco no le reclama las deudas que tiene su partido político, su partido político puede elaborar leyes muy beneficiosas para los bancos. Y además, si tuviera mayoría absoluta, podría llegar a avalar cualquier préstamo que pidiese con todos los recursos del Estado, incluido el trabajo futuro de toda la población, todos los servicios públicos y todas las propiedades, y así permitir al banco que le cobre a todos los ciudadanos de ese país las cantidades que el banco necesite para pagar sus deudas, a cambio de no reclamar determinadas deudas al partido político concreto que le hiciese semejante favor. Y podría desatender cualquier otra necesidad de la población para darle prioridad al pago de las deudas, y poner el pago de esa deuda por encima de cualquier otro gasto nacional.

Una vez que se firman ese tipo de pactos, la población de ese Estado ha sido vendida junto con el país, por lo que los vendedores tratarán de gastar el dinero recibido cuanto antes y dejar el país a la deriva para que se lo repartan los bancos.

Privatizarán y venderán todo cuanto puedan para ahorrar todo el dinero posible antes de abandonar su posición de poder, y a ser posible, más que dinero, tratarán de ahorrar riquezas reales (inmuebles, empresas, servicios públicos privatizados...) que son las que podrán conservar, utilizar y vender pase lo que pase en un futuro.

Es cierto que el Estado, como cualquier empresa o ciudadano que juega al capitalismo, acepta que para no ser eliminado, debe conseguir que se eliminen otros, porque las reglas del juego impiden que todos paguen sus deudas, por la sencilla razón de que no existe el dinero suficiente para pagarlas todas.

Por ello asumen que alguien va a tener que perder sus riquezas reales, sin entender que, si se mantiene el juego, a largo plazo todos acabarán perdiéndolo todo.

Entonces se adopta la solución de que sean los demás los que pierdan, y para ello, los políticos empiezan a jugar al capitalismo y a apropiarse del dinero de la gente, pero teniendo cuidado de no enfadar a los bancos, para que éstos no les reclamen la devolución de todo el dinero que les han prestado.

De esta manera, los políticos se someten al poder financiero, y empiezan a dar todas las leyes que el poder financiero les exige, y de paso, leyes que les permitan seguir enriqueciéndose.

Esto coloca al ciudadano en una pinza económica en la que de un lado están los bancos asfixiándolos con la reclamación de los préstamos, y del otro lado estará el Estado cargándolos cada vez con mayores impuestos, y recortándoles las ayudas, las prestaciones y los servicios públicos con la excusa de contener el gasto del Estado para poder pagar la deuda pública que tiene el Estado.

Para colmo, los ciudadanos, siguen peleando entre ellos para apropiarse del dinero y así poder pagar sus deudas, tanto bancarias como fiscales, y para ello se ven obligados a vender su fuerza de trabajo cada vez por un salario menor, y vendiendo sus mercancías cada vez a precios menores.

Algunos deciden adaptarse al sistema capitalista y tratan de adoptar actitudes capitalistas frente a sus semejantes, pero sin tener nunca la posibilidad de alcanzar la posición de quien controla la emisión del dinero o la elaboración de las leyes, ya que estas materias estarán vetadas a la ciudadanía bien por medio de las leyes, bien por falta de medios económicos, o bien a través del uso de la violencia legítima, que es otro monopolio que el capitalismo se ha guardado para sí.

Viendo así el panorama, resulta lógico pensar que todo el mundo tiene deudas con el banco, ya sean propias, o de su empresa, o de su familia, o de su Estado, y que de manera directa o indirecta, todo el mundo va contribuir al pago de esas deudas, bien pagando dinero de manera directa al banco, bien pagando impuestos al Estado para que éste le pague al banco, o bien sufriendo recortes en sus derechos sociales para que el ahorro pueda ser entregado al banco.

Y lo peor de todo es que los ciudadanos asumen esa deuda sabiendo que no existe dinero para pagarla, por lo que una gran parte de ella tendrá que ser pagada con riquezas reales, no con dinero. Esta apropiación por parte del banco de las riquezas reales de la sociedad es lo que llamaremos "la desamortización bancaria", por ser la forma en la que los bancos desposeen a los ciudadanos de sus tierras, de sus casas, de sus bienes y de su trabajo, para cobrarles el interés que ha generado todo el dinero emitido por su catastrófico sistema.

- - -

## 5.5.- LA SOBERANÍA ECONÓMICA NO RESIDE EN EL PUEBLO

Evidentemente, para solucionar este problema, además de una gran reforma del sistema económico, debería suprimirse el pago de todos los intereses pagados en la historia y los pendientes de pago en la actualidad, así como suprimir los intereses en el futuro. También habría que volver a calcular todas las devoluciones de préstamos habidas y por haber, para suprimir en ellas el interés cobrado y devolverlo a quienes lo pagaron.

Pero como a estas alturas eso sería prácticamente imposible, lo máximo a lo que se aspira es a aplicar esta solución sólo a los préstamos vigentes y a aquellos de los que aún se tenga memoria, y dejar prohibido el interés para el futuro.

También deberá darse otra regulación muy diferente a la fabricación y circulación del dinero, prohibiendo su emisión a quien no respalde el dinero emitido con una riqueza real.

Y por último, atendiendo a que los bancos ya se han apropiado una gran parte de las riquezas reales del planeta, será necesario declarar ilegítima cualquier deuda que haya tenido su origen en préstamos con interés, abriendo procedimientos para solicitar las correspondientes indemnizaciones por los daños y perjuicios ocasionados a los distintos países, empresas, familias y personas, así como establecer "la emisión de moneda sin un respaldo en riqueza real" y "el cobro de intereses" como delitos económicos contra la humanidad en los códigos penales de todo el planeta.

Pero como hacer todo eso es algo que corresponde al titular de la soberanía, habrá que esperar a que el pueblo soberano se decida a tomar cartas en el asunto, porque de momento dicha soberanía parecen tenerla unos señores a los que nadie ha elegido democráticamente, y que la usan a su antojo sin que nadie les cuestione la titularidad de esa soberanía económica que dicen tener.

La soberanía es el poder para decidir sobre las cosas, y pueden existir diferentes tipos de soberanía: política, económica, alimentaria, tecnológica... pero aquí nos centraremos en la "soberanía económica" para tratar de entender quien la ostenta hoy día, si es quién debería ostentarla, y cómo debería gestionarse para evitar una tiranía económica.

En la actualidad existen unos cuantos bancos centrales que son los que tienen la soberanía económica en el planeta, y la tienen por la sencilla razón de que les ha sido cedida burocráticamente por los representantes de los países. Algunos de esos representantes tenían asumida esa competencia por la sencilla razón de que han sido elegidos a través de procedimientos democráticos, sin entrar en este momento a valorar la pureza o validez democrática de dichos procedimientos.

Tales procedimientos han sido establecidos legalmente de conformidad con unos ordenamientos jurídicos que en la actualidad tienen vigencia y, para gran parte de la población, tienen incluso legitimidad.

A pesar de que dichos ordenamientos suelen sostenerse vigentes sobre la premisa inicial de que la soberanía reside en el pueblo, dicha soberanía se ejerce únicamente eligiendo a unos representantes que tendrán libertad para gestionar dicha soberanía de la manera que quieran, y esos representantes han querido ceder la soberanía económica a organismos supranaconales que no han sido elegidos democráticamente, del mismo modo que tampoco se elige democráticamente a ningún ministro, ni a los titulares del poder ejecutivo, ni del poder judicial, ni del poder mediático.

En estas democracias, cada 4 años se suele elegir una candidatura para que elija a las personas que ostentarán la soberanía legislativa, ejecutiva y judicial, aunque a través de las leyes también ostentan de hecho la soberanía alimentaría, tecnológica e informativa, y pudiendo ostentar la soberanía económica, ésta última ha sido cedida a unos bancos centrales, que son los que emiten el dinero para los bancos pequeños que posteriormente prestan ese dinero a los Estados y a los ciudadanos de los distintos Estados.

De esa manera, no solo se ha privado a los ciudadanos de su soberanía económica durante el tiempo para el que son elegidos los representantes que la ceden a esos bancos centrales, sino que una vez cedida, se pretende que dicha soberanía económica no pueda volver a recuperarse por ningún Estado, ya que los tratados internacionales en los que se ha cedido siguen vigentes a pesar de que cambien losrepresentantes elegidos por cada Estado.

De esta forma dichos organismos pueden regularse libremente, cambiar su funcionamiento interno, sus órganos de gobierno, sus tendencias morales, y cambiar toda la política monetaria a su antojo, sin necesidad de respetar las voluntades de los ciudadanos o de los Estados a los que resultarán aplicables dichas políticas económicas.

Serán los ciudadanos y los Estados los que deban respetar las decisiones de estos organismos porque, supuestamente, gozan de una legitimidad democrática que les ha sido transmitida desde el pueblo soberano a través de los representantes políticos elegidos en procesos electorales.

Con esto queda suficientemente claro que la soberanía económica actualmente no reside en el pueblo, ni tampoco en los representantes elegidos por el pueblo, sino en los bancos centrales, y en los organismos supranacionales que se crean fuera del ámbito nacional.

Esto choca directamente con la realidad social que subyace bajo toda transacción económica, ya que como se ha visto, la gestión de la riqueza tiene como base el intercambio, y el uso de la moneda es algo que las personas aceptan voluntariamente de manera personal como herramienta para facilitar el intercambio.

Esto abre la posibilidad a las personas de volver a la realidad natural del intercambio tal y como éste existía en sus orígenes, volver a cuantificar el valor de las cosas, atribuirles una nueva unidad de valor y utilizar esa unidad de valor como una “alternativa” al dinero que llamamos “oficial” y que es emitido por los bancos centrales en régimen de monopolio.

De esta forma, la soberanía puede que no resida en el pueblo dentro del marco económico actual, pero si nos colocamos en el orden económico natural de las cosas, cada ciudadano podrá reformular sus concepciones sobre la economía, y adaptar algunas de sus transacciones económicas a un nuevo modelo de economía alternativa que le resulte más satisfactorio a sus intereses personales que el régimen económico impuesto por las autoridades oficiales que creenostentar la soberanía económica.

Si las leyes dicen que la soberanía reside en el pueblo, tal vez lo digan por la sencilla razón de que, en el estado natural de las cosas, la soberanía es realmente un poder para decidir del que no se puede privar a las personas.

La soberanía económica no residirá en el pueblo mientras el pueblo siga creyendo que dicha soberanía esta fuera de su alcance, pero podrán hacerla suya en cuanto entiendan que todos tienen plena libertad para usar el modelo económico que quieran, para usar varios sistemas económicos a la vez, o no usar ninguno de ellos.

Quizás este epígrafe debería haberse llamado “La soberanía económica sí que reside en el pueblo, aunque el pueblo no lo sepa”.

Pero hay que marcar la diferencia entre la economía “legal” que se nos trata de imponer desde fuera por imperativo legal, y la economía “legítima o natural”, que es el funcionamiento de la economía tal y como ésta acontece en el universo, siendo la segunda la que realmente importa.

- - -

## 5.6- EL PASO CLAVE: DEL MISTERIO A LA TRANSPARENCIA

Para terminar esta breve historia de la economía, creemos necesario señalar el gran problema que encierra la economía en la actualidad, y dicho problema es recisamente el halo de misterio, complejidad e incomprensibilidad con el que se trata de presentar a la economía ante la gente, haciendo que quienes no se atrevan a entender el funcionamiento de la economía no estén nunca en condiciones de cuestionar dicho funcionamiento, ni mucho menos de imaginar una alternativa.

Eso permite que las autoridades económicas puedan seguir controlando todo el juego capitalista de manera exclusiva y en su propio beneficio, y para ello crean palabras, términos y conceptos económicos que, aunque en esencia no son más que piezas que describen el funcionamiento económico que se ha explicado aquí,hacen que la gente no llegue a entender realmente lo que sucede, y que soporte la economía más como un “acto de fe en los mercados” que como un “acto libre de aceptación”.

Pondremos algunos ejemplos a título meramente ilustrativo.

El primer ejemplo es el concepto de “dinero”, que ya se acepta universalmente como un recurso útil en sí mismo, y no como un instrumento que facilita el intercambio de productos. Además se acepta la existencia de “dinero oficial”, haciendo creer a las personas que cualquier dinero que no sea aceptado por las autoridades económicas oficiales carece de validez. Y a esto se suma la falta de toda explicación sobre la creación del dinero oficial y su respaldo en riquezas reales.

Esta historia de la economía ha tratado de hacer comprensibles tales cuestiones.

Un segundo ejemplo es la “inflación”, que se explica como el incremento natural del coste de las cosas y la consecuente pérdida de valor del dinero con el paso del tiempo. Y también se ha explicado ya que dicha inflación tiene su origen la misteriosa creación y aparición del dinero cuando éste aparece en cantidad superior a la aparición de nuevas riquezas reales.

Como tercer ejemplo citaremos el “interés”, que se explica como la compensación justa que deben recibir quienes prestan dinero durante un tiempo por la devaluación del valor que sufre el dinero a consecuencia de la inflación. Ya se ha explicado que si se entendiese y controlase la creación y aparición de dinero, no habría inflación y por lo tanto no debería existir el interés, por lo que hasta ahora, estos tres ejemplos, ya habían sido explicados en apartados anteriores.

Un cuarto ejemplo podría ser la “deuda pública de los Estados”, ya que ésta se presenta como la petición de dinero que realiza un estado emitiendo unas cartas de pago que llevan aparejado el pago de intereses, sin explicar a los ciudadanos de esos Estados que dicha deuda pública es en realidad comprada por los pequeños bancos, y que éstos cobran un interés superior al que el banco central les cobra a ellos, estando prohibido por las autoridades económicas que los Estados puedan pedir dinero directamente al banco central pagando un interés menor. Y lo que es más grave, es que también se prohíbe a los Estados emitir ellos mismos el dinero que necesitan, suprimiendo así la necesidad de pedir prestado y de pagar intereses.

Ahora ya podemos hablar de ejemplos de conceptos algo más incomprensibles, como puede ser la Tasa Anual Equivalente (también conocida como TAE), que no es más que el interés que se recibe por el dinero depositado en los bancos, una vez deducidos todos los gastos y todas las comisiones que se van a cobrar sobre dicho dinero en el plazo de un año. Digamos que es como “la rentabilidad bruta” que nos proporciona el dinero cuando lo depositamos en un banco, y suele ser muy inferior al interés que el banco cobra a otras personas cuando les presta ese mismo dinero.

Existen otros conceptos creados para referirse a diferentes tipos de interés que pueden pactarse en los contratos, como puede ser el IPC, que suele usarse para los casos en los que dicho interés será aplicado a las deudas que cobren los ciudadanos.

El IPC se supone que se corresponde con la subida del coste de la vida, aunque la experiencia demuestra que el coste de la vida sube a un ritmo mucho más rápido de lo que lo hace el IPC, y ello por la sencilla razón de que como las deudas con los bancos suelen aplicar intereses superiores, y dichas deudas son parte de los gastos comunes de la sociedad, la diferencia entre los gastos de las deudas que se pagan al banco y los incrementos en los ingresos de los ciudadanos que supone el IPC, hace que cada año, los ciudadanos tengan menor poder adquisitivo, porque los gastos fijos crecen más rápido que los ingresos fijos (en aquellos casos en los que existan ingresos fijos). El IPC se calcula atendiendo a la subida de precios que hayan experimentado una lista concreta de productos a lo largo del año, que nunca son productos financieros ni bancarios, y dichos productos suelen ser diferentes cada año, lo que permite subir el precio de unos productos diferentes cada año sin afectar al IPC, siempre que dichos productos no hayan sido elegidos como patrón para el cálculo del IPC el año en que suben de precio.

La economía también crea otros conceptos relativos a la cantidad de dinero que corresponde ingresar a las personas, como sucede con el IPREM o el SMI. El IPREM es el Indicador Público de Renta de Efectos Múltiples, y en vez de ser un porcentaje, como el IPC, es una cuantía que se utiliza para calcular aquellas prestaciones o ingresos de los ciudadanos, y viene establecido por cantidades en euros por día, por mes o por año. El IPREM ha reemplazado al SMI (Salario Mínimo Interprofesional) en todos los ámbitos excepto en el laboral, suele ser de menor cuantía que dicho SMI. Como ambos índices los establece el Estado de forma unilateral, puede parecer que dichos indicadores sean más políticos que económicos, pero debe tenerse en61cuenta que sus cuantías se establecen atendiendo a criterios económicos de la sociedad, por lo que se ven muy afectados por la situación económica de cada momento para crecer más o crecer menos, aunque rara vez crecen al mismo ritmo al que crecen los gastos reales de los ciudadanos.

Otro ejemplo de concepto extraño es el de “mercado financiero”, que viene a ser como el mercado en el que se venden y se compran las “finanzas”. Esas “finanzas” no son más que las fianzas o garantías en riquezas reales ofrecidas por los ciudadanos y los Estados a los pequeños bancos. Como ya se sabe que muchas de esas garantías no serán cobrables o no alcanzarán nunca el valor de la deuda garantizada, ya que al banco pequeño le interesa más tener derecho a cobrar intereses que ofrecer una garantía real del cobro, dicho mercado financiero está muy sometido a la especulación, a la confianza y, en cierta manera, a la suerte de cada uno de los deudores, ya que si estos pagan el mercado financiero funcionará, pero si los deudores dejan de pagar, el mercado financiero se tambaleará hasta caerse por su propia inconsistencia, al ver que no existen garantías reales suficientes para cubrir todos los pagos.

Cuando las deudas empiezan a ser evidentemente impagables, osimplemente impagadas, es cuando llega la crisis del mercado financiero. En estos momentos es cuando el mercado financiero empieza a exigir el pago de todas las deudas para hacerse con lamayor cantidad posible de riquezas reales, antes de volver a reactivar la economía introduciendo nuevas cantidades de dinero al tráfico económico.

Cada una de estas crisis es una criba social en la que aquellos que no han podido pagar sus deudas son desposeídos de sus bienes, y son eliminados del juego capitalista si no aceptan quedarse como simples trabajadores, que deberán trabajar el resto de sus vidas para pagar las deudas que no hayan conseguido pagar con la entrega todos sus bienes.

Estos son solo algunos ejemplos a través de los cuales se conceptualiza como una realidad establecida el sistema capitalista, y como se hace a través de organismos oficiales, rara vez son cuestionados por la gente. Si a esto añadimos la complejidad del mercado financiero entendido como “negocios sobre las deudas y los riesgos”, podremos hacernos una idea de por qué es tan poca la gente que comprende las valoraciones de los riesgos, el crecimiento continuo del riesgo, y la continua subida de los intereses basada tanto en la inflación como en el riesgo.

Respecto al riesgo, bastará entender que si el sistema está diseñado para que, tarde o temprano, resulte imposible pagar las deudas, dicho riesgo nunca dejará de existir, y en algunos sectores de la sociedad, nunca dejará de crecer, haciendo que se pidan cada vez más préstamos, a intereses cada vez más elevados, haciendo las deudas cada vez más impagables. Y todo ello sin necesidad de hablar nunca sobre el origen y la aparición del dinero, ni de la trampa que se esconde tras la inflación y los intereses.

Este misterio en el funcionamiento, sumado a la opacidad existente en el estado actual de la economía, no solo hace que resulte imposible conocer y entender la situación económica real de la sociedad, sino que solo resulta posible conocer la realidad económica capitalista, y ésta, como todo el mundo sabe, se encuentra en muy mal momento para la inmensa mayoría de la población.

Ahora bien, ¿y si fuese posible conocer toda la información económica?. Conocer toda la información económica supondría saber, en primer lugar, quién genera el dinero, y con qué riqueza real ha garantizado la emisión de dicho dinero, es decir, qué piensa aportar a la sociedad para que ésta le otorgue validez a su dinero, y que lo haga sabiendo que dicho dinero no es un simple papel, o una mera anotación contable. Esto permitiría a las personas no aceptar dinero que no corresponda a alguna riqueza real.

En segundo lugar, supondría conocer las operaciones comerciales y las transacciones económicas en las que interviene cada sujeto, permitiendo de esta manera evitar la subida de precios a través del simple comercio, ya que si A comprase a B narajas a 3 monedas el kilo, y luego las vende a C a 5 monedas el kilo, C podrá saber si A plantó las naranjas o las compró, y si las compró, podrá saber a quién y a qué precio, y tendrá posibilidad de comprar las naranjas directamente a B, ahorrándose el incremento de valor que supone A. O podrá decidir comprarlas de todos modos a A, porque le compense de alguna manera ahorrarse el viaje.

En tercer lugar, la gente podrá saber quienes se dedican a emitir monedas a cambio de riquezas reales para luego entregar dichas riquezas reales por un número superior de monedas, lo que supone obtener monedas sin producir riqueza real, lo que permite a la gente evaluar, de manera libre, si acepta el pago de dichas monedas adicionales al intermediario o no, atendiendo a la necesidad real de dicho servicio de intermediario.

Por último, se permitiría a la gente garantizar el pago de los precios directamente a quienes le entregan el bien, sin necesidad de pagar una comisión en concepto de intereses a quienes se dedican a realizar las anotaciones contables, ya que dicha transparencia de la economía permite que dichas operaciones sean conocidas, supervisadas y controladas por todos.

Esta transparencia de las operaciones económicas eliminaría muchos misterios de la economía actual, y si bien podría tener algún conflicto con el derecho a la intimidad que debería solucionarse atendiendo a los casos concretos, permitiría una economía más justa, evitaría la inflación injustificada de los precios, e impediría el uso comercial del dinero que se crease de la nada sin ningún tipo de garantía. Pero como la economía capitalista se sustenta precisamente sobre este misterio para funcionar, no podemos confiar en que esto sea aceptado por las autoridades económicas oficiales, ni en que dichas autoridades vayan a cambiar el sistema económico que tantos beneficios les ha reportado durante tanto tiempo.

Así, contra el sistema económico actual no se puede hacer más que señalar sus imperfecciones y sus errores, desvelando los misterios que le han permitido mantener oculta esta gran estafa cometida contra la humanidad, y confiar en que sea la gente la que, una vez entienda el funcionamiento de la economía actual, se atreva a cuestionarla, deje de alimentarla, y empiece a crear otros sistemas económicos más justos, democráticos, naturales y transparentes.

Resulta evidente que el mismo sistema capitalista acabará poniendo sobre la mesa estos problemas, pero puede que eso solo facilite el hecho de que la gente pierda la fe en dicho en sistema, sin tener ninguna otra alternativa económica a la que aferrarse. Y será esa falta de alternativas la que mantendrá a la gente dentro del sistema económico capitalista actual.

Es por ello que, una vez se ha explicado que la gente conserva su soberanía económica natural para intercambiar sus riquezas dentro del sistema económico que elija, se hace necesaria la construcción de alguna alternativa que pueda nacer y convivir con el sistema capitalista actual, pero corrigiendo los errores de éste. Y esa es la razón por la que, a continuación, pasaremos a exponer en la segunda 64parte de este libro, una propuesta de economía alternativa basada en lo que comúnmente se conoce como “las monedas sociales”.

El texto que se pone a continuación ha sido extraído con leves modificaciones del libro "El Orden Económico Natural", de Silvio Gessel, y explica muy bien el valor del papel moneda actual: Fundados en los hechos trascriptos, afirmamos de una manera categórica que puede hacerse dinero de papel que, sin promesa alguna de conversión, sin respaldo de mercancía determinada (oro, por ejemplo) lleve la inscripción:

“Un euro” (marco, chelín, franco, etc.)o dicho con otras palabras:
“Este papel es por sí un euro” o: “Este papel vale en el comercio, en las cajas públicas y ante los tribunales 100 euros”, o para expresar mi parecer de una manera más drástica aunque sin pretender mayor claridad:
“Quien presente este papel para su conversión al Banco Emisor recibirá allí 100 latigazos (promesa de pago negativo). Pero en los mercados, en las tiendas y comercios, recibirá el portador en mercancías o servicios lo que asigne la demanda y la oferta; dicho en otras palabras, lo que él pueda canjear en el planeta con este boleto, es todo lo que puede pretender que valga”.

Creo haberme explicado con suficiente claridad sin dejar duda alguna acerca de lo que entiendo por papel-moneda actual.

* * *


# EKONOMIKON SEGUNDA PARTE. ECONOMÍA ALTERNATIVA Y MONEDAS SOCIALES.

# 1.- INTRODUCCIÓN

## 1.1.- El concepto

Las monedas sociales se entienden como sistemas económicos alternativos que tienen por finalidad devolver al ciudadano la soberanía económica, tratando de crear un sistema económico que no cometa los errores que ha cometido el sistema capitalista, pero aprovechando las ventajas que dicho sistema ha demostrado tener como sistema contable. Por ello sus elementos configurativos serán similares a los de toda economía (satisfacer las necesidades de sus integrantes utilizando para ello la riqueza real disponible en un grupo determinado), con el añadido de que pone de manifiesto la relevancia que tiene la confianza en la moneda alternativa, en oposición a la obligatoriedad de la moneda oficial impuesta por las instituciones oficiales, y por extensión, la necesidad de transparencia en el sistema económico para que dicha confianza no sea un acto de ciega fe.

Digamos que una economía alternativa será una manera diferente de llevar las cuentas a cómo nos ha hecho llevarlas el sistema capitalista, y su utilización siempre será voluntaria.

Por eso en economía conviene distinguir entre 3 conceptos esenciales que van interactuar entre ellos: las necesidades del grupo, los recursos del grupo, y el sistema contable del grupo. La economía alternativa trata de diseñar un nuevo sistema contable que permite gestionar las relaciones entre necesidades y recursos de una manera diferente a la que utiliza el capitalismo, usando para ello las monedas sociales.

- - -

## 1.2.- La confianza

Ya se explicó en la primera parte que la economía tenía sus inicios en las redes de confianza surgidas por la rutina del trueque, que posteriormente dichas redes asumieron monedas o patrones de intercambio comunes, y que más tarde, dichos patrones de intercambio se fueron modificando para su mayor eficiencia, terminando en última instancia por aparecer unas instituciones que controlaban dicho patrón y la emisión de monedas, hasta desembocar en una economía financiera en la que la riqueza eran las monedas, en lugar de los recursos disponibles.

En las monedas sociales, el principio motor seguirá siendo la confianza y las redes de personas que confían en el sistema económico alternativo, pero la riqueza deben seguir siendo los recursos disponibles, entendiendo que la moneda es únicamente una forma de contabilizar la confianza y la riqueza de sus integrantes, sin llegar a constituir riqueza propiamente dicha, al carecer de valor como recurso fuera de la red de confianza, e incluso careciendo de valor en sí mismas como riqueza real, ya que la moneda social no es más que un tipo de memoria o de apunte contable, o una unidad empleada para cuantificar la confianza de las personas.

La red de confianza formada por todos los integrantes de la red deberá reemplazar a las instituciones financieras del capitalismo, permitiendo que sean la democracia y la transparencia el tablero sobre el que circule la confianza de sus integrantes como herramienta para la cuantificación de la riqueza de un grupo.

Posteriormente será posible entender como surgirá una confianza que resulte perfectamente cuantificable entre los integrantes de la red, así como entre la red y cada uno de sus integrantes, sin necesidad de una institución que pueda controlar a las personas o cobrarles intereses por usar dichas unidades de cuantificación, en oposición a lo que hace el sistema económico oficial cuando cobra intereses cada vez que emite moneda para su uso por parte de los ciudadanos.

La confianza existirá directamente entre las personas que integran la red de moneda social, sin necesidad de un intermediario que cobre por gestionar o contabilizar esa confianza, debido a que la contabilizarán esas mismas personas, de forma horizontal, sin pagar a nadie por ello.

- - -

## 1.3.- La riqueza

Se entenderá por riqueza todos los bienes y servicios existentes en la comunidad, al ser dichos bienes y servicios la verdadera energía de la comunidad para satisfacer sus necesidades, entendiendo como bienes todos los productos disponibles en la red, y como servicios todas las habilidades y conocimientos que poseen las personas adscritas a la red.

Esto es realmente la riqueza de un grupo, con la peculiaridad de que los miembros de cada grupo entregan sus bienes o prestan sus servicios a quien estiman pertinente, y lo hacen preferentemente con gente de su propio grupo, o con gente en la que confían.

En la actualidad, la mayoría de la gente pertenece al sistema económico oficial, por lo que suelen producir y trabajar para quienes les entreguen monedas de curso legal oficialmente aceptadas por las instituciones económicas, al tener depositada su confianza en que, obteniendo dicho dinero oficial, cualquier otra persona confiará en ellos, utilizando el dinero como herramienta para contabilizar la confianza que unas personas tienen en otras, al ser todas ellas personas pertenecientes a la economía oficial de las monedas institucionales.

Esto, evidentemente, no es percibido por quienes forman parte de dicho sistema, ya que el sistema económico actual se ha encargado de que todas esas personas crean que ese dinero constituye realmente algún tipo de riqueza, de la manera en que se aludió a la "riqueza monetaria" en la primera parte de este manual.

La aparición de otras redes económicas permite aplicar el mismo patrón de funcionamiento, pero tratando de garantizar que nadie pueda crearse la confianza de los demás sin merecérsela (como sucede cuando una institución emite dinero sin garantizar su respaldo con ningún bien o recurso, ni aportar nada a la comunidad, y sin tener que conocer ni tener confianza real con nadie)

También trata de evitar que nadie pueda cobrar por prestar confianza, ya sea ésta propia o ajena (como sucede con el cobro de intereses cuando se presta dinero), ya que en ambos caso se comete el error de pensar que las monedas son riqueza en sí mismas, se las considera como un bien o un objeto útil, y se finge que prestarlas supone algún tipo de entrega de bienes que merece ser retribuida por encima del valor de dichos bienes.

En este sentido, las monedas sociales deben tener claro que, además de los bienes y servicios de una red, otra parte de la economía consiste en la confianza que las personas se tienen mutuamente, entendiendo esta confianza cuantificada en forma de moneda social como simples "promesas", sin que una promesa sea una riqueza real, sino que, en el mejor de los casos, será una riqueza en potencia, ya que solo se convertirá en riqueza cuando sea efectivamente convertida en un bien o servicio frente a la red.

Esta idea de considerar la confianza como parte de la economía puede sonar extraña, pero si se piensa detenidamente, el sistema capitalista sabe que la confianza es mucho más importante de lo que pueda parecer a simple vista, pero no debe olvidarse que las promesas en sí mismas no son ni bienes ni servicios, y su valor dependerá siempre de la confianza que se tenga en la persona que ha prometido entregar o hacer algo, por lo que en realidad son un apunte contable que cuantifica nuestra confianza en que la persona que hizo la promesa representada en la moneda acabará cumpliéndola.

En la actualidad, el dinero institucional emitido por el sistema económico oficial no ha prometido entregar ni hacer nada por aquellas personas que les entreguen dinero oficial, excepto devolverles más dinero oficial, resultando de esta manera que la economía existente lo único que promete entregar son más promesas, pero nunca nos entregará ninguna riqueza real, dejando que sean los creyentes en dicho sistema quienes entreguen sus bienes y servicios a cambio de dichas promesas.

Si a esto se añade que esas instituciones pueden emitir las monedas cuando quieran, en la cuantía que quieran, y sin tener que respaldarlas con ningún bien o servicio, no resulta complicado entender por qué pueden acumular tanta riqueza y tanto dinero quienes trabajan cerca de dichas instituciones, aunque nadie confíe en que vayan a recibir ninguna riqueza real de dichas instituciones oficiales, a excepción del servicio que pueda suponer ser controlados y dirigidos por ellos a través de su sistema contable.

- - -

## 1.4.- Las necesidades

Por necesidades entendemos todas aquellas necesidades de consumo (tanto en bienes como en servicios) que tienen los miembros de la red, y que precisan ser satisfechas con los recursos disponibles. La economía no es más que una manera de cuantificar lo que cada persona aporta a la red, y lo que recibe de ella, para poder comprender y conocer tanto el funcionamiento, como el éxito o fracaso, del sistema económico utilizado, teniendo en cuenta que el sistema de monedas sociales es compatible con otros sistemas económicos que funcionen simultáneamente, como pueden ser el trueque, la economía de gratuidad o familiar, o incluso cualquier otra moneda oficial o alternativa.

Otra función de la economía debería ser la de conocer las necesidades del grupo, para permitir que dichas necesidades sean satisfechas, y que todo lo necesario para satisfacerlas pueda ser producido, hecho o adquirido por los miembros de la red, e incorporado a la misma en la cantidad necesaria.

Esto lleva a tener en cuenta que las necesidades tienen un factor cualitativo, relativo a cuáles son las necesidades que hay que satisfacer, y un factor cuantitativo, que atiende a la cantidad necesaria de un recurso determinado para satisfacer dicha necesidad. Esta función de la economía ha sido totalmente olvidada por el sistema capitalista.

Así se puede entender que tener naranjas es bueno para satisfacer la necesidad de alimento, pero tener miles de toneladas de naranjas, a lo mejor es tener demasiadas naranjas, y convendría más producir otro tipo de alimentos para tener una alimentación más variada. Saber cuántas naranjas se necesitan y quién se encargará de producirlas, ayuda a la economía transparente a que no se produzca un excedente que pierda su utilidad real al no poder ser consumido. Por eso un sistema contable eficiente debe tener en cuenta dichas necesidades.

Está claro que tener un pequeño excedente puede ser bueno para comerciar o intercambiarlo por otros recursos, pero tener un excedente excesivo te obligará a deshacerte de dicho excedente después de haber invertido tiempo y recursos en producirlo, y sin recuperar ni ese tiempo, ni esos recursos, ni ningún otro bien o servicio del mismo valor.

Esto exige que un nuevo sistema económico no se centre únicamente en comprender los medios de producción de riqueza, sino que debe atender también a los medios de distribución de toda la riqueza producida, y para ello resultará beneficioso poder contabilizar las aportaciones de cada persona a la producción de bienes y servicios, así como el consumo de bienes y servicios que realiza o quiere realizar cada persona.

- - -

## 1.5.- Las personas implicadas y la eficacia de las monedas sociales.

Debe tenerse en cuenta que en estos sistemas económicos no es necesaria la existencia de dinero físico, ya que el verdadero recurso existente en ellos son precisamente las personas con sus habilidades productivas, por lo que la riqueza no depende de la existencia de moneda, sino de la satisfacción de las necesidades de sus integrantes.

Cualquier moneda social que no tenga gente implicada en hacer realidad dicha moneda, estará condenada al fracaso, al ser la moneda una cuestión de confianza recíproca entre las personas usuarias de la red.

La existencia de moneda no hace más que poner de relieve la incapacidad de las personas individuales para satisfacer sus propias necesidades, por lo que resulta una consecuencia destacable del funcionamiento de las monedas sociales la "tendencia a la desaparición de monedas o la reducción de las mismas al menor número posible", ya que siempre que existan monedas, supone que alguien ha aportado a la red más de lo que ha recibido, y que alguien ha recibido más de lo que ha aportado, por lo que será una misión de la red compensar a quien ha aportado a la red más de lo que ha recibido, así como ofrecer posibilidades de aportar a quienes han recibido de la red más de lo que han podido aportar en compensación.

Es por ello, que las personas tienen un doble prisma desde el momento en que se implican:

1. 	Deben poder satisfacer todas sus necesidades.
2. 	Deben poder aportar a la red aquello que esté a su alcance para que los demás puedan satisfacer también sus necesidades.

__El objetivo es que todos puedan aportar aquello de que disponen para poder acceder a todo aquello que necesitan.__

Así, el objetivo no será la existencia de muchas monedas sociales, sino precisamente la presencia de muchas personas implicadas relacionándose entre ellas, a través de los intercambios, conforme a unas reglas de transparencia que les permitan conocer el estado de la economía en cualquier momento, lo que aumenta la probabilidad de que las necesidades de los demás puedan ser satisfechas, así como de tener posibilidades más abundantes para poder aportar habilidades, servicios o bienes que sean demandados por cualquier persona de la red.

A efectos prácticos, una moneda social es más eficaz cuantas más personas se implican en ella, pero también es una señal de eficacia la existencia del menor número posible de unidades monetarias.

En este sentido, conviene distinguir a la persona implicada de la
persona censada, ya que no toda persona que se inscriba o se adhiera a una red de moneda social, tiene por necesidad que participar activamente en ella.

A efectos de eficacia, solo deben computarse como personas implicadas a aquellas que, efectivamente, hacen uso de la red, y que en adelante llamaremos "personas usuarias".

En el sistema económico oficial resulta imposible medir esta eficacia porque no se sabe cuánto dinero hay emitido exactamente, pero si hubiese manera de hacerlo, seguramente el ratio de eficacia sería bastante bajo, y se evidenciaría que el sistema actual resulta extremadamente ineficiente si es considerado como una moneda social. Este Ratio de eficacia de las monedas sociales se puede resumir con la formula siguiente: Ratio de eficacia = Número de personas implicadas / número de monedas en circulación, en un tiempo dado.

* * *

# 2.- EMISIÓN DE LAS MONEDAS SOCIALES

## 2.1.- La emisión personal, la emisión colectiva y la emisión mixta.

Para que una moneda social sea creada basta con que una persona emita una moneda social y otra persona acepte dicha moneda como unidad de medida de intercambio. Esto parece simple, pero para que suceda, resulta necesario que la primera persona conozca la idea delas monedas sociales (la inmensa mayoría de la población desconoce este concepto mientras se escribe este manual), y también hace falta que se anime a crear una moneda, dándole un nombre, un formato, y debe añadirle un valor del que se responsabilice (un trabajo de fontanería, una bicicleta, una cantidad de alimentos, o cualquier otra cosa que pueda satisfacer alguna necesidad).

En segundo lugar resulta necesario que exista otra persona que confíe en la primera, que sepa que la moneda social que ha creado la primera persona tiene el valor que le ha sido asignado, y debe aceptar ese valor confiando en que la primera persona va a responsabilizarse de que le sea entregado dicho valor cuando devuelva la moneda. Eso habría sido una emisión personal.

Esta sería una moneda eficaz, en la medida en que solo existe una moneda y dos personas implicadas, lo que otorgaría a la moneda un ratio de eficacia de 2 (2 personas / 1 moneda = 2). Si ambas personas realizasen la misma operación con el mismo valor real, pero creando 10 monedas, el ratio de eficacia sería de 0'2 (2 personas / 10 monedas = 0'2), lo que no haría más que reducir el valor de la moneda social a su décima parte, pero la moneda seguiría funcionando exactamente igual entre ellos.

El mismo proceso podría repetirse con una emisión colectiva, en la que un grupo de 10 personas decide crear 10 monedas para montar una red de moneda social y se las ofrece a otras 10 personas, que las aceptan porque confían en que las 10 primeras van a devolver el valor asignado a dichas monedas cuando las monedas sean devueltas. Esta habría sido una emisión colectiva, ya que la responsabilidad por las 10 monedas recae sobre el grupo de 10 personas que las emitió de forma solidaria (es decir, la moneda puede presentarse a cualquier de esas 10 personas para que el valor de dicha moneda sea entregado), pero su ratio de eficacia seguiría siendo de 2 (20 personas / 10 monedas = 2).

Si la emisión colectiva se hace con más monedas, el ratio de eficacia de dicha moneda social se reducirá. Si emiten 50 monedas será de 0'25 (20p / 50m =0'25), y si emiten 100 monedas será de 0'1 (10p /100m = 0'1). El Ratio de eficacia disminuye cuando aumenta el número de monedas.

La diferencia esencial entre un tipo de emisión y otro es la responsabilidad, que se asume de manera individual o colectiva atendiendo a si la moneda la emite una persona, o si la emite un grupo.

Cada persona debe responder de aquellas monedas que emite personalmente, pero también asume la responsabilidad solidaria frente a aquellas monedas que emite de manera colectiva, y esto por la sencilla razón de que quien acepta monedas de emisión colectiva está confiando en que el grupo emisor va a responder por el valor de todas las monedas emitidas por el grupo, y no en que cada miembro del grupo emisor responderá de una parte proporcional de las mismas, ya que en dicho caso, lo correcto habría sido realizar 10 emisiones personales de 1 moneda en lugar de hacer una emisión colectiva de 10.

No respetar este tipo de lógica de la responsabilidad solidaria llevará con facilidad a que la confianza en el grupo se reduzca o desaparezca, haciendo necesario recurrir a la emisión personal de monedas por quienes no asumen responsabilidades del grupo, o bien a formar nuevos grupos entre aquellas personas que asumen la responsabilidad solidaria del grupo que integran.

En este sentido, conviene tener claro que asumir la responsabilidad que le corresponde es lo que hace a una persona digna de confianza, y del mismo modo, es lo que hace que un grupo genere confianza y pueda operar de manera colectiva.

La emisión colectiva de monedas suele verse dificultada porque se desconoce el número de monedas que va a resultar necesario, y por resultar muy poco operativo tener que reunir a todo el grupo cada vez que se quiere emitir una nueva moneda. Es por ello, que en la práctica es más usual la emisión mixta, que unifica las ventajas de ambos tipos de emisión.

En este tipo de emisión mixta, el grupo será responsable de lasmonedas que sean emitidas por cualquiera de sus miembros, y cada miembro podrá emitir las monedas que le resulten necesarias a medida que las vaya necesitando, y lo hará teniendo en cuenta que asume la responsabilidad personal e individual de responder por el valor de dichas monedas. En este caso, los miembros de la red serán responsables también de tales monedas frente a cualquier persona, y como miembros de la red, tendrán también el derecho a recibir el valor prometido por la persona que emitió las monedas.

De esta forma, cualquier persona es libre de usar individualmente su soberanía económica en la emisión de moneda dentro del grupo sin necesidad de reunirse con el grupo, y a la vez estará asumiendo una responsabilidad compartida tanto por las monedas que ella emita, como por las que emita cualquier otra persona de la Red.

 - Emisión personal: Emisión y responsabilidad individual

 - Emisión colectiva: Emisión y responsabilidad colectiva

 - Emisión Mixta: Emisión personal, pero con responsabilidad colectiva


- - -


## 2.2.- El respaldo en riqueza real de la moneda social

Este suele ser un punto esencial de las monedas, ya que dependiendo del valor real que respalde una moneda, dicha moneda será aceptada por las personas o no. El valor más importante de todos reside en la confianza, ya que cuando existe confianza entre dos personas, es muy probable que ni si quiera necesiten entregarse unidades de confianza ni monedas para ayudarse mutuamente, pero el hecho de abrir la red a gente desconocida lleva aparejada la necesidad de que la gente que se incorpora a la red de moneda social tenga la seguridad de que si acepta un tipo de moneda social determinado, dicha moneda social va a poder ser intercambiada por algo, siendo ese algo un tipo de riqueza, ya sea en forma de bienes o en forma de servicios.

De esta necesidad de respaldar la moneda surge la consecuente necesidad de que quien emita monedas sociales ofrezca algún tipo de bienes o servicios a la red para garantizar que algún recurso estará disponible para quien acepte las monedas sociales emitidas.

Así, un pintor puede decidir emitir 10 monedas y entregárselas a un mecánico para que éste le repare el coche, y al hacerlo dejar ofrecidos servicios de pintura. El mecánico poseerá esas 10 monedas sociales, y el pintor tendrá un saldo negativo de -10 monedas sociales, pero ambos saldos se tienen frente a la red de moneda social, lo que les permite usar dichos saldos frente a cualquier otro miembro de la red.

De esta manera si el mecánico quiere comprar naranjas, y en la red hay alguna persona que ofrece naranjas, las 10 monedas que posee el mecánico (y fueron emitidas por el pintor) podrán ser entregadas al productor de naranjas a cambio de las naranjas, y el productor de naranjas podrá entregarlas a otro miembro de la red a cambio de aquello que necesite.

Haciéndolo así, las monedas podrán utilizarse como unidad de confianza entre los miembros de la red hasta que alguno de ellos necesite que alguien le ayude a pintar una casa, en cuyo caso, ese miembro entregará las 10 monedas al pintor a cambio de su ayuda, y cuando el pintor acumule esas 10 monedas a su saldo negativo de -10, su saldo volverá a ser 0, y habrán desaparecido las 10 monedas monedas sociales que el pintor emitió. El efecto es parecido al de una cadena de favores, solo que al usar la moneda social, el valor de los favores queda cuantificado y resulta divisible o acumulable respecto al de otros favores.

Esto permite que la moneda social pueda verse en situaciones en las que sin existir monedas sociales en circulación, sigan existiendo las riquezas reales ofrecidas por cada miembro de la red, ya que los miembros conservan sus habilidades y sus bienes a disposición de los miembros de la red, y cuando alguien les pida algún servicio o algún bien, podrán intercambiar dicho recurso por otro, sabiendo que los miembros de la red aceptarán moneda social, y en caso de que quien le pida el recurso no tenga nada que interese a quien lo ofrece, podrá entregarle monedas sociales.

Dichas monedas podrán entregarse bien del saldo positivo de la persona que solicita el recurso, o en caso de que esta persona no tenga saldo positivo, dicha persona tendrá que proceder a la emisión de nuevas monedas sociales para poder entregarlas a quien le entrega el recurso o le ofrece el servicio.

Para cuidar este extremo es muy aconsejable que, al crear una moneda social, o al incorporar una persona a una red de moneda social existente, las personas señalen qué bienes o servicios están dispuestos a ofrecer a la red, de manera que esas ofertas son realmente el respaldo en riqueza real de las monedas que estas personas emitan.

Esas ofertas serán la riqueza disponible en la red.

Del mismo modo, resulta muy práctico que cada persona de la red tenga la oportunidad de informar a la red de aquellas necesidades que tiene, ya que eso permite agilizar enormemente el funcionamiento de  la moneda y la expansión de los servicios ofertados, ya que hay muchas cosas que nadie ofrece por la sencilla razón de que piensan que nadie las necesita.

El hecho de que la gente pueda concretar lo que necesita, permite que cualquier miembro de la red pueda solventar dicha necesidad, aunque a esta segunda persona nunca se le hubiese ocurrido que alguien iba a necesitar algo tan rebuscado como puede ser una rueda de bicicleta, alguien que le recoja a los niños del colegio, o alguien que tenga tiempo para guardar una cola y sacar entradas para un concierto, ya que estas son cosas que puede hacer cualquier persona con tiempo libre, pero son cosas que pocas personas ofrecerían como un tipo de habilidad al incorporarse a una red de moneda social .

Del mismo modo, conocer las necesidades de la gente, ayudará a focalizar la búsqueda de nuevos miembros de la red, ya que si en la red se demandan servicios de informática, la red intentará localizar a una persona con conocimientos de informática y satisfacer alguna necesidad de esta persona para que dicha persona acceda a ayudar a aquellos miembros de la red que necesitan sus servicios.

Es necesario añadir que el recurso más importante que respalda una moneda social es la disponibilidad de los miembros de la red para ayudar a los demás, ya que los servicios y bienes concretos que se ofrezcan o necesiten en una red son algo que solo se puede conocer con el paso del tiempo, y se van descubriendo a medida que la red va creciendo, prosperando, conociéndose, elaborando proyectos comunes y mezclando los recursos de todos sus miembros en un organismo social mucho más poderoso que la suma de todos ellos.

Finalmente, decir que la riqueza real que respalda una moneda social son las relaciones existentes entre sus miembros, ya sean estas relaciones consistentes en colaboraciones, ayudas, entregas de utensilios, herramientas o productos, o cualquier otra relación económica que necesitaría de dinero para ser llevada a cabo, y que, precisamente por falta de dinero, no suelen llevar a cabo las personas.

Obsérvese que si en la parte primera se dijo que el dinero actual era una manera de simbolizar la confianza en el sistema económico actual, ahora las monedas sociales presentan la oportunidad de representar la confianza en un sistema económico ciudadano. Y es cierto que el sistema económico actual tendrá más riquieza real disponible y más personas implicadas que cualquier moneda social al momento de crearse, pero si la moneda social se materializa correctamente, crece a su alrededor, y se mezcla con otras economías alternativas, llegará un momento en el que esté en condiciones de sustituir al sistema económico actual, pero para ello no debe perderse de vista que el objetivo es enseñar a la gente que la soberanía económica consiste en gestionar nuestra confianza dentro del sistema económico que elijamos, y en ganarse la confianza de aquellas personas que puedan ofrecer a la red aquellos bienes y servicios que resultan necesarios para sus integrantes.


- - -

## 2.3.- El formato de la moneda social.

Por formato entenderemos la forma en la que se mueve la moneda social, es decir, la forma en la que una moneda social es representada por sus usuarios en el tráfico social. Es un elemento importante porque del formato de una moneda dependerá el que la moneda pueda ser confundida con un recurso en sí misma, como le sucede al capitalismo, que considera las monedas como recursos o como riqueza.

Debe recalcarse que las moneda sociales tendrán claro que no son un recurso en sí mismo, sino que son una unidad de valor que cuantifica la confianza que hemos depositado en la red, pero carece de valor fuera de la red, mientras que los recursos son aquellas riquezas que tienen un valor tanto dentro como fuera de la red.

Existen varios formatos posibles, aunque enumeraremos los más comunes, dejando la puerta abierta para que cualquier red pueda usar un formato distinto de los enumerados.

1. __El papel moneda:__
 Este será el formato más tradicional y primitivo, ya que fue el que reemplazó al patrón oro en el sistema capitalista, y consiste en papeles al portador que representan un valor social determinado. Así, del mismo modo que existen los billetes, pueden crearse monedas sociales con forma de billetes que repliquen el formato del sistema capitalista, con la salvedad de que dentro de las redes de moneda social, a estos billetes no se les atribuye ningún valor intrínseco más allá del que tiene el propio papel y la confianza que representan dentro de la red.

 Esta ausencia de valor económico fuera de la red es lo que distingue una moneda social de un objeto cualquiera, como puede ser un sello, un cromo, una estampa o una fotografía. Los pagos "con billetes" tienen lugar mediante la entrega física de los billetes, y eso sucederá en el capitalismo, o con las monedas sociales que se emitan en formato billete...

 Para usar este formato, la red deberá imprimir las monedas sociales con el diseño que prefieran, pero debe tenerse en cuenta que el papel moneda es susceptible de ser falsificado o robado, es susceptible de extraviarse, resulta muy complicado de gestionar a la hora de contabilizar las transacciones, y es fácilmente confundible con un bien mueble, por lo que una compra hecha en moneda social puede  fácilmente confundirse con un trueque completo, ya que puede pensarse que entregar las monedas sociales supone la entrega de un bien mueble. Este formato puede resultar muy práctico en eventos de corta duración en los que vaya a haber un gran número de transacciones en poco tiempo, pero le resta transparencia a la moneda...

2. __Las libretas personales:__
 Este segundo formato es un poco más avanzado que los billetes, por cuanto que las libretas no se entregan ni se intercambian, sino que son pequeños libritos o trozos de papel que pertenecen a cada persona de la red, y en ellos se reflejan cada una de las transacciones en las que interviene su titular, así como el saldo positivo o negativo de cada una de dichas libretas, sin perjuicio de que puedan añadirse otras informaciones adicionales, como la fecha de las transacciones, en concepto de las mismas, o cualquier otro dato que los miembros de la red puedan considerar relevante.

 Los pagos mediante libreta suelen hacerse mediante la firma de cada una de las personas que interviene en una transacción en la libreta de la otra persona. Así, si A vende a B algo, ambos escribirán la transacción en sus libretas, y A firmará la libreta de B, y B firmará la libreta de A, haciendo que cada miembro funcione como notario o fedatario de las transacciones en las que interviene.

 Este formato sigue teniendo posibilidades de que alguien realice anotaciones falsas en su libreta, ya que en cada transacción solo se puede consultar la libreta de las dos personas que intervienen, pero no se puede comprobar la aceptación de todas las personas que han firmado en el historial de esas libretas. Si bien, permite no tener que imprimir los billetes, evitando la posibilidad de que sean extraviados o robados, ya que la libreta no puede ser utilizada por quien no sea el titular de la misma, y del mismo modo, favorece la transparencia en la medida en que permite a los miembros de la red conocer el saldo en moneda social y el historial económico de la persona con la que realiza cada transacción sin la necesidad de usar un ordenador.

3. __Anotaciones virtuales:__
 Este formato es el más avanzado, y por ello es también el más complejo, ya que al consistir en una contabilidad gestionada a través de internet, supone una barrera tecnológica para aquellas personas que no estén familiarizadas con las nuevas tecnologías. Las anotaciones virtuales son una contabilidad transparente en la que las operaciones de los miembros de la red se registran en una aplicación informática a la que todos los miembros tienen acceso, lo que les permite conocer el saldo de cada uno de los miembros, así como el número de miembros que hay en la red. Si la aplicación está mejorada, puede facilitar más información, ya que también puede utilizarse para publicar los bienes y servicios ofertados por cada persona, o las demandas que cada miembro de la red ha realizado, lo que agiliza mucho la información entre necesidades y recursos dentro de la red de personas que integran la moneda social.

 No debemos olvidar que esta información sobre la riqueza disponible y las necesidades reales es casi más importante que llevar la cuenta de las monedas, y dicha información sería muy caro y engorroso facilitarla a través del formato billete o del formato libreta.

 También podrían incorporarse el historial económico de cada persona de la red, el tiempo que llevan en la red, o incluso tener opciones más complejas que permitan realizar cálculos generales sobre la totalidad de la red o sobre grupos concretos de personas o transacciones, para ver qué bienes o servicios faltan, de cuáles hay excedentes, qué ratio de eficacia tiene la moneda, con qué frecuencia usa la moneda un usuario determinado, o ver quienes acumulan demasiado saldo, ya sea positivo o negativo.

4. __Formato Mixto:__
 Este formato no es un formato en sí mismo, sino la posibilidad de utilizar varios formatos de los anteriores atendiendo a los deseos y posibilidades de cada moneda social. De esa manera, podría darse el caso de una moneda que normalmente funcione con un formato de anotaciones virtuales, pero que esporádicamente recurra a los billetes para la celebración de eventos, o que incluso acepte también el uso de libretas junto a las anotaciones virtuales para no excluir a las personas que no están familiarizadas con la informática.

 De todas maneras, y sea cual sea el formato que se elija, lo que hay que tener en cuenta es que la moneda social no es un recurso en sí mismo, sino una unidad de valor social que cuantifica la confianza y que se utiliza como herramienta para la realización de trueques multilaterales cuyas entregas están separadas en el tiempo. Es decir, trueques en los que alguien entrega algo y recibe algo, pero no tiene por qué recibirlo en el mismo momento ni recibirlo de la misma persona a la que se lo entregó, ya que el trueque se hace con la red de moneda social, y no con la persona concreta. Por otro lado, esa persona no realiza tampoco el trueque con nosotros, sino con la misma red de moneda social.

 El formato de la moneda será algo parecido al cuerpo físico de la red de moneda social, ya que es la manera en la que se materializa y se hace visible la contabilidad de la red.


- - -

## 2.4.- La existencia de la moneda social

Una vez llegados al punto en el que un conjunto de personas ofrece sus bienes y recursos a una comunidad a cambio de unas unidades de confianza cuantitificables que le sirven para satisfacer sus necesidades dentro de esa misma comunidad, entendiendo que dichas unidades no son un bien en sí mismo, se puede decir que ya existe una moneda social, independientemente del formato que hayan elegido para su uso, y sin importar el ratio de eficacia de la misma, ya que éste ratio podrá crecer o disminuir con el tiempo.

Pero cuando hablamos de que la moneda existe, y de que no debe confundirse con un bien ni con un recurso en sí misma, resulta necesario profundizar un poco más sobre qué es lo que existe realmente, y como ya se ha dicho, lo que existe es una red de personas que tiene una forma propia de gestionar su economía.

Con la creación de esa red han empezado a recuperar su soberanía económica, pero ahora deben tener cuidado al enfrentarse a los problemas que resolvió el capitalismo "a su manera" y que nos ha llevado hasta la economía capitalista, y que recordaremos ahora brevemente para tenerlos claros:

- Personas que olvidan las necesidades reales para interesarse únicamente por las monedas: Los acumuladores del capitalismo.
- Personas que consideran como actividad productiva la obteneción de monedas, en lugar de la obtención de recursos: Los comerciantes, banqueros y empresarios del capitalismo.
- Personas que dejan de observar la riqueza real para observar únicamente el número de monedas: Los economistas del capitalismo.
- La creación de instituciones elitistas que regulan las monedas en beneficio de determinados grupos.
- La aparición descontrolada e incontrolable de monedas.
- La aparición de conflictos por la hegemonía entre las diferentes monedas.
- La falsificación de las monedas.
- Las fluctuaciones del valor de las monedas. Inflación y deflación.
- La confusión entre monedas y recursos.
- Sustitución del trueque por dos negocios de compraventa. Fractura conceptual del intercambio.

Todas estas cuestiones son cuestiones económicas con las que, tarde o temprano, se encontrará cualquier economía que prospere y crezca hasta el punto de convertirse en un sistema económico, por lo que en esta segunda parte del libro trataremos de ver de qué manera pueden solucionarse estas cuestiones respetando la soberanía económica de todas las personas, sorteando los problemas de transparencia, de fluctuaciones del valor, la rivalidad entre monedas, la fractura del concepto de "trueque" en dos "compraventas", de emisión de moneda y, lo que es la verdadera finalidad de todas estas cuestiones: La satisfacción de todas las necesidades de quienes aceptan formar parte de el sistema económico alternativo que aquí se propone.

Con esto en mente es cuando podemos decir que una moneda social ha empezado a existir, ya que ahora no solo sabemos que la moneda existe físicamente, y que físicamente no es más que una unidad abstracta que puede ser representada de diferentes maneras, sino que ahora también tiene clara su finalidad, sus objetivos y va a trazarse un plan para conseguirlos.


- - -

## 2.5.- El límite máximo de emisión y el tamaño monetario.

Por último, y antes de finalizar con la emisión de la moneda, debe tratarse la cuestión relativa al número de monedas existentes, ya que es un tema que debe ser solventado antes de empezar a analizar el funcionamiento de la moneda social, su crecimiento, sus conflictos y su desarrollo.

Cuando se crea la moneda social como unidad de confianza, dicha confianza puede ser mayor o menor, y tener unos u otros motivos para existir, por lo que veremos ahora varios criterios para definir el número de monedas sociales que debe existir en una red, o su "tamaño monetario".

- - -

### 2.5.1.- Monedas sociales basadas en dinero oficial.

Para esto, lo primero es saber qué es lo que estamos usando como confianza, porque hay redes que llaman "monedas sociales" a tickets o vales cuya confianza no se basa en la confianza de las personas en otras personas, sino que tienen su base en que esos tickets representan realmente dinero oficial.

Según hemos visto ya, estas no serán monedas sociales propiamente dichas, sino dinero oficial en forma de cheque, talón, promesa de pago o ticket de compra, y normalmente suelen utilizarse para campañas de fidelización de clientela, por lo que suelen ser los centros comerciales quienes más usan este tipo de mal llamada "moneda social". En estas monedas, el número de monedas a emitir, será evidentemente el número de monedas oficiales que respalden a dichas monedas, atendiendo a la conversión establecida entre una y otra. Digamos que serían moneda oficial en un "formato" de moneda social.

Pero hay personas que pueden iniciar una moneda social de esta manera, como sucedió con el Axarco en un pueblo llamado Vélez-Málaga (España), donde una persona depositó dinero en un banco e imprimió unos cheques con diseño de billete social. Con esos billetes le aceptaban los pagos en cualquier sitio, porque realmente eran billetes canjeables por dinero oficial, de forma que con el tiempo, quienes aceptaban esos billetes preferían usarlos para comprar, o para dar el cambio a sus clientes, en vez de tener que ir al banco a canjearlos por el dinero oficial. Llegó un momento en el que casi todo el pueblo consideraba esos billetes como moneda de curso legal y eran utilizados con frecuencia, desplegando algunos de los efectos propios de las monedas sociales, utilizando para ello la misma confianza que la gente tenía en el dinero oficial.

Un sistema de economía alternativa no puede hacer depender la emisión de monedas de la existencia o no de dinero oficial dentro de la red, pero es cierto que este tipo de moneda puede utilizarse como medio de expansión o de creación de una moneda social, exigiendo dinero oficial a cambio de dichas monedas, para evolucionar posteriormente.

Pero a nosotros nos interesan aquellas monedas sociales que se emiten sin necesidad de tener dinero oficial, sin perjuicio de conocer estas pseudomonedas crecimiento.


- - -

### 2.5.2.- Monedas sociales basadas en productos

Existen otras monedas sociales en las que para emitirlas, lo que se necesitan son recursos reales, y son algo parecido a los mercados alternativos que aparecieron en Argentina durante "el corralito", periodo en el que los bancos paralizaron toda su actividad y dejaron a la población sin acceso al dinero oficial depositado en ellos.

En estos mercados del corralito, una persona obtenía monedas sociales a medida que iba entregando productos, y luego cambiaba esas monedas por los productos que necesitaba, lo que no hacía más que facilitar los trueques indirectos o multilaterales, que viene a ser otra funcionalidad de las monedas sociales.

Este tipo de monedas sociales hace depender la emisión de monedas de la cantidad de productos que han sido entregados al mercado, llámese mercado, central de abastecimiento, local de intercambio, o cualquier otro nombre que quiera darse al lugar donde se guardan los productos entregados por las personas a cambio de monedas sociales, para que luego cualquier persona que tenga monedas sociales pueda llevárselos a cambio de sus monedas.

En este tipo de monedas tampoco hay más límite de emisión para cada persona que el de la cantidad de productos aptos para su uso o consumo que cada persona pueda llevar a la central de intercambio. Si bien, debe tenerse en cuenta que este tipo de monedas sociales suelen tener a alguien en dicha central de intercambio que es quien decide si acepta o no un producto, por lo que la emisión de las monedas depende en cierta medida de esa institución que es la central de intercambio, y no hay manera de emitir moneda si no es bajo la supervisión y aprobación de la central de intercambio.

Ni este modelo ni el anterior requieren la existencia de una red de confianza previa, porque la confianza se basa bien en dinero oficial, o bien en productos reales, que se entregan al momento de adquirir las monedas, pero en ambos casos, existe una institución que es la que decide los horarios para la adquisición de moneda, o la que impone los requisitos de idoneidad para aceptar el producto con el que se pretende adquirir la moneda.

Entendiendo esto, podemos comprender que los órganos emisores no son realmente quienes aportan el dinero ni quienes entregan los productos, sino que esta institución entrega las monedas a quienesaportan dinero o productos a esa institución, por lo que son esas personas quienes están depositando su confianza en dicha institución, y son quienes que están autorizando a las personas de la central de intercambio para emitir monedas sociales que respalden esos productos.

Estas dos monedas tienen en común que al basar su confianza en la entrega previa de dinero o productos, no utiliza realmente la confianza como unidad de intercambio, sino que utiliza bienes con valor real (suponiendo que quienes lo hacen creen que el dinero oficial tiene un valor real intrínseco, y por eso lo consideran tan válido como un recurso real).

Las monedas sociales a las que nos queremos referir son aquellas en las que, dentro de la red de moneda social, las personas emiten el dinero con base en la pura confianza, sin necesidad de entregar nada previamente, sino asumiendo el compromiso de devolver una cantidad igual de monedas sociales a la red, en vez de entregarlas a una institución de la red.

Estás monedas que aceptan los saldos negativos basados en la confianza, son las monedas denominadas como tipo LETS (acrónimo de Local Exchange Trading Sistem, que significa Sistema de Intercambio Local), y en ellas pueden existir unas normas de gestión para la emisión de monedas, pero son las personas quienes emiten las monedas sociales a medida que las van necesitando, las entregan cuando consumen algún bien o servicio de la red, las recuperan cuando prestan o entregan algún servicio o bien a la red, y las hacen desaparecer de la red cuando sus saldos negativo y positivo coinciden, volviendo a tener un saldo cero.

La gran diferencia en este tipo de monedas es que no se necesita una institución, sino que todas las personas de la red son esa institución y todas son responsables de la transparencia y la gestión. Son estas monedas las que nos interesan.

- - -

### 2.5.3.- Monedas sociales basadas en la confianza (tipo LETS)

Es en estas monedas tipo LEDS donde será habitual dar unas normas comunes a todos los miembros de la red por las que todos ellos estarán autorizados a emitir monedas de forma personal, por las que responderá de forma solidaria toda la red, y toda la red estará legitimada para requerir a esa persona bienes o servicios por el valor de las monedas sociales que haya emitido. Estas monedas, evidentemente, resultan más complejas de comprender, pero son a su vez, las que permiten a los ciudadanos tener un control directo sobre su sistema económico y, al basarse precisamente en la confianza, son las que requieren de una red de confianza para funcionar adecuadamente.

Las monedas tipo LETS también adoptan diferentes formas de funcionamiento, pero en todas ellas resulta crucial establecer la existencia o inexistencia de ese límite máximo de emisión al que aludíamos al inicio de este apartado, siendo lo aconsejable, y por lo tanto, lo habitual, establecer la existencia del mismo, ya que no poner un límite máximo de emisión a cada persona, hace que la cantidad de moneda existente pueda crecer indefinidamente hasta números exorbitantes, haciendo bajar mucho la eficacia de la moneda, y poniendo además en peligro a la red si alguna de esas personas acumulase un enorme saldo negativo y decidiese dejar dicho saldo en la red, ya que la red es responsable de todas esas emisiones individuales.

Es por ello que partiremos de la base de que las monedas sociales tipo LETS tendrán un límite máximo de emisión, o también llamado "limite de saldo negativo", aunque cualquier red de moneda social puede crearse sin este límite, asumiendo que su sistema económico permite una emisión descentralizada e "infinita" de moneda.

El limite máximo de emisión supone fijar el número máximo de monedas que una persona puede emitir, y dependerá del valor que cada moneda social atribuya a su moneda, pero para explicar los diferentes tipos de moneda LETS atendiendo a esta emisión de la moneda, supondremos la existencia de una moneda social que ha establecido dicho límite en 100 unidades monetarias, y veremos las consecuencias, efectos y posibilidades que ofrece la existencia de este límite.

La primera consecuencia inmediata y lógica es que, al establecer el límite de saldo negativo por persona en 100, ya resulta posible calcular el número máximo de monedas que puede existir en la red, multiplicando el número de personas que hay en la red por ese limite máximo permitido a cada una. De esta manera, se puede conocer qué cantidad de monedas necesita una persona para colapsar totalmente la red moneda social.

Una persona que consiga acumular todas las monedas sociales, colapsa la moneda social, porque dejaría a todas las demás personas sin monedas. Por eso, cuando alguien acumula muchas monedas sociales, sería indicio de que algo está fallando en la moneda social, ya que esa persona, solo puede hacer eso de dos maneras: O bien está aportando a la red mucho más de lo que está recibiendo, por lo que está siendo un esclavo de la red y habría que buscar la manera de gratificarle o premiarle ofreciendo desde la red algo que le permita gastar las monedas que acumula, o bien esa persona tan acumuladora está utilizando algún truco para acumular muchas monedas sin aportar a la red una riqueza real, como sucedería en el caso de que alguien comprase en la red algo a 3 monedas, y lo vendiese luego en la misma red por 10 monedas, obteniendo un beneficio de ello. Cuando alguien hace eso, en la red deber ser posible conocer laactividad de esa persona, para que quien vendía por 3 pueda contactar con la persona que compra por 10, y puedan evitar a ese intermediario, salvo que realmente ambas partes prefieran mantener ese trabajo de intermediación por resultarles útil o beneficioso, como sucedería si esa persona realiza labores de transporte, reparación, o transformación, que ninguna de las dos personas implicadas, o una tercera persona, quiera realizar por un precio menor.

También podría deberse la acumulación positiva a que la red no es capaz de ofrecer nada útil a esa persona, lo que le impide gastar las monedas que obtiene, pero en dicho caso, lo normal es que dicha persona dejase de aportar a la red hasta que la misma no le ofreciese algún bien o servicio que necesitase y le permitiese gastar esas monedas sociales. Por lo que en estos casos, la persona dejaría de acumular monedas antes de colapsar la red.

Otra consecuencia es la limitación del tamaño de las operaciones realizables, ya que una persona, en principio solo podrá realizar operaciones con base en su confianza por el importe máximo establecido, que sería de 100, y cuando consiga acumular más monedas en un saldo positivo, su operación tendrá como importe máximo su saldo positivo más esas 100 unidades negativas que puede emitir personalmente. Esto, relacionado con el punto anterior, lleva a entender que nadie de la red podrá realizar una operación cuyo importe sea superior al número total de monedas de toda la red, que sería el número de personas inscritas multiplicado por 100, y para ello necesitará haber conseguido previamente que todas las personas le hayan entregado sus monedas emitidas, cosa que nos llevaría al problema del colapso de la red.

Las monedas sociales de este tipo se crean inicialmente para operaciones de escasa cuantía, y solo a medida que vaya creciendo la red, habrá que ir solventando la necesidad de realizar operaciones de cuantías grandes. Para ello, podrá autorizarse por la red que algún saldo pueda rebasar el límite de saldo negativo para las operaciones de importes elevados, o podrá centralizarse dicha compra a través de una cuenta colectiva y pública. pero en dichas operaciones habrá que ver la manera en la que una persona concreta consigue obtener tanta confianza de la red, o si pone algún tipo de garantía adicional para el cumplimiento de la obligación que va a contraer con la red.

Esto tiene una influencia determinante sobre el funcionamiento que podrá tener la red atendiendo al limite máximo de emisión, por lo que debe tenerse en cuenta que cuanto más pequeño sea este número, menor es el margen de actuación y operabilidad entre sus integrantes, y también será menor el tamaño máximo de sus operaciones. Por el contrario, si el límite se pone demasiado elevado o no se pone límite, el riesgo que asume la red por cada persona podría resultar inasumible con que solo una de esas personas se dedicase a emitir monedas sin intención de aportar nunca nada a la red.

Es por eso que este límite variará de unas monedas a otras, atendiendo a la confianza que se tengan entre sí las personas que la integren, a la aceptación de personas desconocidas, o a cualesquiera otras circunstancias personales o ideológicas de cada grupo dinamizador. A largo plazo, lo ideal es que las monedas aspiren a no necesitar un límite máximo de emisión, lo que significaría no tener límites en el tamaño de sus operaciones, pero ese grado de confianza requiere un
trabajo previo de la red en escalas de menor tamaño.

Una vez que se tiene definido el límite máximo de emisión, cada moneda social podrá elegir la manera en que funcionarán la emisión y la puesta en circulación de las monedas, ya que una vez que se conoce este límite máximo, puede optarse por imprimir las monedas, anotarlas en alguna libreta, anotarlas en alguna cuenta virtual, o dejarlas sin emitir sabiendo que podrán emitirse en el futuro.

Si las monedas se emiten directamente, las personas usuariasdispondrán de un saldo positivo desde el inicio, ya que las monedas emitidas serán repartidas entre ellas, por lo que no será necesario aceptar saldos negativos, pero será necesario saber que todas y cada una de esas personas ya debe a la red bienes y servicios por valor de las monedas recibidas, y se sabrá que en la red existen ese número de monedas emitidas.

Lo normal es que las monedas no se emitan directamente, sino que se queden en estado latente en poder de cada persona usuaria, autorizándola a emitir monedas cuando le resulte necesario, y sin rebasar el límite máximo. De esa manera se conoce el tamaño máximo de la red, pero se permite reducir el número de monedas en circulación, y hacer más visibles los desajustes entre necesidades y recursos, ya que es más fácil medir la aproximación al cero, que la aproximación al 100, suponiendo que fuese 100 ese límite máximo para cada usuario, ya que en ese caso, el saldo 100 de cada usuario sería el punto cero de la moneda social.

Desde este manual preferimos usar la aproximación al cero, ya que con el tiempo, suele emitirse una determinada cantidad de moneda que nunca desparece de la red, ya que siempre hay algunas personas que conservan el saldo en positivo o negativo, dando lugar a un espacio interno dentro de la economía que también resulta importante.

Aludimos al espacio interno porque imaginamos la moneda social como una circunferencia que se amplía a medida que va apareciendo más moneda social, pero que va adquiriendo forma de donut cuando hay una parte de esa moneda que se inmoviliza, dando lugar a una circunferencia interna. Los flujos reales de la moneda social, serán los que vengan representados entre una circunferencia y otra, y será dicho donut el que refleje el tamaño real de la red en movimiento. Como dicho movimiento irá vinculado también a todas aquellas transacciones que se puedan realizar sin usar moneda dentro de la red, la red siempre será más fuerte que la representación gráfica de sus monedas.

* * *

# 3.- FUNCIONAMIENTO DE LAS MONEDAS SOCIALES

## 3.1.- El valor de la moneda social. Bienes, servicios y confianza.

Hemos visto anteriormente que durante la emisión de las monedas sociales podían usarse diferentes cosas, como podían ser el dinero, los bienes o la pura confianza. Ahora conviene mencionar otro tipo de riqueza que está a medio camino entre ser un bien y ser confianza, como sería la prestación de servicios. Y se dice que está a medio camino porque un servicio no es un producto que pueda ser llevado a una central de abastecimiento para ser almacenado, sino que, al consistir en una acción concreta que debe desarrollar una persona, hay que ver de qué manera este tipo de riqueza puede incorporarse a las monedas sociales. Para ello, haremos un pequeño inciso para referirnos a la prestación de servicios.

### 3.1.1.- Las Prestaciones de Servicios
Los servicios pueden ser quizás el recurso más importante de una sociedad, ya que es realmente la acción en la que se materializa el trabajo de las personas, sus habilidades, sus conocimientos y, en definitiva, todo el potencial humano que existe en una sociedad. Pero tiene la peculiaridad de que es un tipo de riqueza que está latente, y que no se puede apreciar hasta que la acción de prestar el servicio es llevada a cabo.

Por ello, una persona puede decir que aporta a la red 200 kilos de naranjas procediendo a la entrega de las mismas, pero no resulta tan sencillo hacerlo cuando una persona decide aportar a la red 20 servicios técnicos de reparación de vehículos, porque esas promesas de prestar servicios de reparación son promesas, y no son productos, por lo que necesitan de la confianza en que dichas promesas serán cumplidas para poder ser aceptadas en la red de moneda social.

Es por esta razón que muchas monedas sociales surgen en un principio como bancos de tiempo, o redes de apoyo mutuo, en las que el recurso por excelencia es el tiempo y el apoyo, y la forma en que se entrega el tiempo y el apoyo es a través de la prestación de sevicios o promesas de hacerlo.

Estas redes suelen tener como unidades contables la hora, o el minuto, o unidades temporales, de manera que quien presta un servicio durante cierto tiempo, tiene derecho a que se le preste un servicio durante el mismo tiempo. Estos bancos de tiempo requieren desde el principio la existencia de confianza, una confianza que tenga fe en que los servicios ofrecidos por sus integrantes van a ser realmente prestados cuando sean solicitados por otros integrantes.

En estos colectivos resulta imprescindible, tarde o temprano, prestar un servicio confiando en que la red nos devolverá el favor más adelante, aunque hay muchos que tratan de existir exigiendo el intercambio inmediato y directo de servicios, pero los bancos de tiempo que tratan de funcionar siempre así, por norma general terminan desapareciendo, bien porque quienes ofrecen algo que otra persona necesita no necesita nada de esa persona concreta, o bien porque quienes entienden que hay que avanzar hacia la confianza dentro de la red, terminarán separándose de una red en la que no existe confianza entre sus integrantes.

Otro problema que se plantea en los bancos de tiempo es la necesidad de incorporar productos a la red para satisfacer algunas de sus necesidades, resultando muy complicado valorar todos los productos en tiempo, sobre todo cuando no son elaborados artesanalmente. Para incorporar productos a un banco de tiempo y poder valorarlos, resulta necesario transformar la unidad de tiempo en una unidad de valor más abstracta, y cuando surge esta necesidad, la unidad de tiempo tiende a equipararse con otro tipo de unidad de valor que, con el tiempo, terminará funcionando como una moneda social, o terminará poniendo precios en dinero oficial y apartándose de la economía alternativa.

De esta manera, tenemos que tanto los productos como los servicios son realmente la riqueza de una red, si bien resultará posible incluir, además de estos dos, los otros dos elementos mencionados anteriormente, como son el "dinero oficial" y la "confianza".

Una vez se tiene claro que son esos 4 elementos los que supondrán lariqueza de una red de moneda social, resultará necesario conocer la manera en que estas 4 cosas son valoradas, para poder conocer la manera en que las monedas sociales tendrán un valor u otro, y ante todo habrá que ver frente a qué, o frente a quién, tienen ese valor.

- - -

### 3.1.2.- La valoración de los bienes
En primer lugar, hablaremos brevemente del dinero oficial, por ser el más comprensible para cualquier persona. La mayoría de la gente atribuye al dinero oficial el valor que las instituciones económicas dicen que hay que atribuirle, por lo que este dinero, considerado en la red de moneda social como un bien intercambiable por otros bienes o servicios fuera de la red, tiene frente a la sociedad el valor del dinero oficial, y para los miembros de la red, puede tener ese mismo valor o no tener ninguno.

Pero debe tenerse en cuenta que dicho valor está sometido a devaluaciones arbitrarias por parte de esas mismas instituciones que lo controlan, y es posible que haya personas dentro de la red de moneda social que prefieran no utilizar este dinero.

Lo que debe tenerse claro es que la moneda social tendrá que lidiar tarde o temprano con la moneda oficial, y permitir su existencia dentro de la red, permite influir en su funcionamiento y utilizarlo para establecer relaciones económicas con personas ajenas a la red, lo que ya veremos más adelante de qué manera supone una ventaja para la red de moneda social.

En un principio, las monedas sociales suelen establecer una conversión de 1 a 1, es decir, que una moneda oficial suele equivaler a una moneda social, con la excepción de que las monedas oficiales siempre se podrán canjear por las monedas sociales que emite la red, pero las monedas sociales no pueden ser canjeadas por monedas oficiales, por la sencilla razón de que no todas las redes de moneda social van a tener siempre monedas oficiales como respaldo de sus monedas sociales. También porque uno de los objetivos de la redes de moneda social es utilizar la economía de las monedas sociales como sistema económico, por lo que fomentarán el uso de sus propias monedas frente al uso de monedas de curso oficial.

Todo ello evolucionará a medida que crezca la red, pero de momento basta con saber que la moneda social tendrá que interactuar con la moneda oficial y, que de momento, nos centraremos en la moneda social, ya que el choque entre la moneda social y la oficial se explica en un capitulo a parte al final de este manual.

En segundo lugar, habrá que ver de qué manera se establece el valor de los productos, y para ello debemos recordar que el valor de las cosas es siempre subjetivo, por lo que al tratar de objetivizar el valor de las cosas siempre nos alejaremos, aunque sea un poco, de la subjetividad que caracteriza al valor. Para ello, debemos tener claro que en toda transacción económica en la que se entrega un bien o servicio, dicho bien o servicio no vale lo mismo para quien hace la entrega que para quien la recibe, aunque para llevar a cabo la transacción, ambas valoraciones deberán ceder en favor de una tercera valoración que pueda ser aceptada por ambas partes, y a menudo es esa tercera valoración, que no coincide con el valor que realmente atribuye ninguna de las partes, la que se acaba considerando como valor real.

Para tratar esta cuestión conviene distinguir primero entre lo que en economía se entiende como "valor de mercado" y "valor de uso", ya que son conceptos usados en economía y que actualmente tienden a confundirnos.

Para entender el valor de mercado de las cosas, hay que atender al valor que un bien o servicio tiene en el mercado en el que se ofrece, es decir, hay que atender a cuánto paga la gente que puede comprar dicho bien por otros bienes similares o idénticos.

Para comprender el valor de uso, lo que hay que tener en cuenta no es cuanto pagarían los demás por ese bien, sino qué utilidad tiene ese bien para la persona concreta a la que se le ofrece. En una valoración prima el precio, mientras que en la otra prima la utilidad, por lo que la segunda es una valoración más alejada de los precios y del dinero en sí mismo.

Existe una tercera valoración, que sería el "coste de producción", ya que hay quien considera que el valor de las cosas debe ser el coste que ha supuesto producirlo. Para ver la diferencia entre estos valores basta con imaginar un cuadro artístico que en el mercado puede venderse por un precio elevadísimo, pero que a la mayoría de la gente no le serviría en su casa más que para estorbar, porque no sabrían dónde colgarlo en su pequeña casa, si es que tienen una casa.

Si tratamos de poner un valor objetivo a dicho cuadro, seguramente entremos en un debate sin final, ya que habrá quienes traten de otorgarle el valor del mercado artístico, que será muy elevado, ya que algún rico que admire al autor podría pagar una gran suma por dicho cuadro. También habrá quienes digan que en comprar ese cuadro no se gastarían nada, porque no lo necesitan y les supondría un estorbo.

Otros dirían que si el lienzo vale 10, la pintura vale 5 y el dibujante ha invertido 6 horas, calculando a 10 monedas la hora, estaría dispuesto a pagar 60 monedas por el tiempo más 15 monedas de los materiales, haciendo una propuesta de 75 monedas como valor objetivo del cuadro.

En todos estos casos, quien define el precio es la persona que va a adquirir el cuadro, bien por su valor artístico, o bien por su valor útil, bien porque valora el trabajo de producirlo, y todas son posiciones totalmente comprensibles. Pero dichos valores tienen su origen en un mercado irreal, ya que parece que el precio es objetivo y se fija atendiendo a la valoración que el comprador hace del bien, y luego el vendedor dirá si lo vende o no, o en caso de que haya varias personas dispuestas a comprar, decidir a cuál de ellas se lo vende.

El mercado a veces se vuelve mucho más irreal, y se puede llegar a la situación en la que el vendedor establezca unilateralmente un precio, usando cualquiera de esos sistemas de valoración, y sea el comprador quien acepte o rechace dicho precio. Si bien, no debemos olvidar que existen bienes que son necesarios para todos, y en estos bienes, resultará muy peligrosa la situación social que se genera cuando es el vendedor quien puede imponer el precio y el comprador solo puede aceptar o rechazar ese precio, porque si un bien necesario es ofrecido desde un monopolio, al comprador no le quedará más remedio que pagar el precio que se le pida, aunque no sea para nada un precio realmente justo el que deba pagar por algo que es realmente necesario. Y es posible que se quede esa necesidad sin cubrir.

Si bien, en el mercado de la economía alternativa, el valor de las cosas dependerá más de cuánto vale ese bien para quien lo vende, que de cuánto vale para quien lo compra, y con esto tendremos que volver a recordar lo que se explicó en la parte primera respecto de las mercancías: Las mercancías son aquellas cosas que se introducen en el mercado porque no tienen ningún valor útil para su poseedor, siendo el ejemplo más frecuente el mismo dinero, ya que éste solo sirve para ser entregado a cambio de algo, pero no tiene casi ninguna utilidad por sí mismo, excepto su destrucción (quemar billetes para producir calor, usar los billetes para escribir, fundir la monedas para obtener metales... y otras cuantas cosas que normalmente podrán hacerse con otros bienes mucho más baratos que el propio dinero oficial).

Este es el cambio de paradigma económico que subyace en la economía alternativa, ya que si la gente entiende que las personas sólo venden aquello de lo que pueden desprenderse, debe entenderse que la gente solo venderá voluntariamente aquellas cosas que no necesita en un momento determinado y puede desprenderse de ellas, ya sean sus habilidades, sus conocimientos, o bienes que ya no utiliza.

Los servicios se valorarán exactamente igual que los bienes, atendiendo a cuánto vale el servicio para quien lo presta, en vez de cuánto pagaría por dicho servicio la persona que lo necesita. Digamos que lo que la gente suele vender puede identificarse con el excedente de riqueza que es capaz de generar, y aunque dicha riqueza pueda ser útil para otras personas, a esa persona no le reporta ninguna utilidad conservar dicha riqueza para sí misma. Sin embargo, existe una gran cantidad de recursos y habilidades que, en el sistema capitalista actual, o se venden o se pierden, dependiendo de si hay o no hay dinero oficial para hacer funcionar la economía oficial.

Es por ello, que a medida que crece una red de moneda social, sus integrantes acaban desarrollando una economía en la que entre ellos dejan de poner precio a las cosas, y terminan por crear una red en la que todos participan en los procesos productivos de la red intentando que todos los miembros tengan aquello que necesitan. Pero llegar hasta ese punto requiere tiempo, y haber vencido a todos los problemas que la economía oficial va a plantear a la red de moneda social, ya que existen diversos bienes y servicios que son necesarios y están monopolizados por la economía oficial.

Mientras se avanza hacia la economía de gratuidad, las monedas sociales desempeñan un papel fundamental, ya que permiten ir liberando sectores productivos de la economía oficial, y a medida que estos sectores se vayan ampliando, la red irá creciendo y ganando terreno.

Si las monedas sociales siguen el criterio de valorar las cosas según las leyes del mercado, ya sea por el "valor de uso", ya sea por el "valor de mercado", o por el "coste de producción", es muy probable que acaben imitando los errores del capitalismo, porque los integrantes terminarán buscando la manera de obtener siempre el mayor número posible de monedas a cambio de cualquier cosa que para ellos carece de valor, como sucederá cuando ofrezcan una bicicleta que ya no usen, o cuando ayuden a alguien a hacer algún trabajo durante un tiempo que no tenían pensado aprovechar en nada.

A modo de ejemplo, estará bien mencionar a aquellas personas que dedican su tiempo a producir cosas que no necesitan con la única finalidad de venderlas, y que llegado el momento en el que ven que han producido más de lo que pueden vender se dan cuenta de que gran parte de su producción no solo le ha supuesto unos costes en tiempo, energía y recursos que no va a recuperar, sino que además, dichos bienes producidos empiezan incluso a molestarle en casa, o van a ponerse en mal estado por ser bienes perecederos.

Esta mención se hace a los simples efectos de plantear los posibles errores que existen en la manera en que organizamos nuestros procesos productivos, ya que muchos de nosotros nos limitamos a producir únicamente excedentes, que también podrían llamarse "mercancías", "cosas que no necesitamos", o simplemente "basura", en lugar de producir cosas que realmente necesitamos, que serán aquellas que producimos porque las necesitamos personalmente, o porque sabemos a ciencia cierta que alguien las necesita.

Todos esos bienes que no necesitamos pueden llegar a la red de moneda social, donde puede que alguien los necesite, y a todos ellos se les atribuirá un valor en moneda social en cuanto se incorporen a la red porque alguien de la red los necesita. Habrá que ver qué criterio sigue cada integrante de la red para poner el precio a esas mercancías, ya que de ello dependerá en parte el éxito o fracaso de la red. Cuando la red crezca, las personas tendrán un espacio económico en el que poder aportar sus excedentes personales a la red, y podrán disponer personalmente de los excedentes de la red, bajo el prisma de esta nueva economía.

A continuación veremos cómo funciona la transmisión de dichos bienes y servicios, y en consecuencia, como se transmite la moneda social, para distinguir este proceso de aquél que tiene lugar al transmitirse el dinero oficial.

- - -

## 3.2.- La transmisión de la moneda social. Las transacciones

Por lo explicado hasta ahora, ya sabemos que tanto en la economía oficial como en la alternativa, hay personas que ofrecen bienes y servicios a otras personas a cambio de unas unidades monetarias, si bien con las peculiaridades citadas respecto de las monedas que pertenecen a economías alternativas, que serían a groso modo las siguientes:

1. Una emisión descentralizada.
2. Un sistema contable transparente y horizontal para la totalidad de la red económica.
3. Unos precios puestos con base en la utilidad para el vendedor, y no en base a la necesidad o posibilidad económica de los consumidores.
4. Un sistema productivo donde se produzcan recursos necesarios para la red, a los que sí podrá aplicarse el precio equivalente al "coste de producción".
5. Las relaciones entre la persona productora y la consumidora se basan en la confianza más allá de la simple moneda.

Esto hará que no sea lo mismo entregar un bien o un servicio a cambio de monedas oficiales, que hacerlo a cambio de monedas alternativas, ya que en el sistema oficial basta con la simple entrega de billetes o la transferencia bancaria para recibir el producto o servicio, mientras que en las economías alternativas, realmente no hay que efectuar un pago, sino que hay que comprobar la existencia de confianza con esa persona, y publicar en la red la transacción económica que se haga, para que toda la red tenga conocimiento de la relación que ha tenido lugar entre esas dos personas.

Para transmitir las monedas sociales de un sujeto a otro, habrá que adaptarse al formato de cada moneda, ya que no será lo mismo las monedas que hayan elegido el soporte en libreta, que las que hayan elegido el soporte virtual, ni será lo mismo transmitir moneda social impresa en papel, que transmitirla cuando es una contabilidad llevada a través de libretas personales.

- - -

### 3.2.1.- La red virtual como sistema contable
La operación resulta bastante sencilla cuando las monedas sociales se transmiten por la simple voluntad de la persona que las posee, o de la persona que las emite, ya que lo normal es que las monedas sociales estén contabilizadas en sistemas informáticos tipo LETS, y resulte necesario, de un lado, la petición de cobro de la persona que presta el servicio, así como la aceptación de la persona que ha recibido el servicio. Esto se hace manifestando la voluntad de cobrar y la voluntad de pagar en el sistema contable, que será quien contabilice ambas voluntades y las reflejará en el sistema transparente al que todos los miembros de la red tendrán acceso, y por lo tanto, del que cualquier persona que utilice dicho sistema funciona como un fedatario en caso de conflicto.

Lo que realmente atribuye un valor real a la transmisión de monedas sociales es la publicación de dicha transmisión en la red, por lo que debe tenerse claro que, a pesar de existir otros mecanismos que agilicen o faciliten las transacciones, la validez de las mismas de cara al sistema contable de la red requiere que dicha operación haya sido transmitida al sistema contable transparente y horizontal de la red.

Dicho sistema contable será una aplicación informática alojada en un servidor de internet que permita a cada usuario de la red el acceso, previa identificación, y que le permita consultar su saldo personal y realizar sus movimientos económicos. Además de eso, la red de moneda social debe tener otros elementos en dicha aplicación informática para desarrollar correctamente todo su potencial, como los siguientes:

- Debe permitir a los usuarios acceder a información de los demás usuarios, como sus saldos, los servicios o bienes que ofrecen a la red, los servicios o bienes que demandan de la red y las transacciones que realizan unos con otros.

- Debe permitir a cualquier usuario conocer el estado de la red, permitiéndole consultar qué cantidad de moneda hay emitida en cada momento, a qué velocidad circulan las monedas dentro de la red, quien posee dichas monedas, el límite máximo de moneda de la red, y cualquier otra información que resulte necesaria para que los usuarios de dicha red puedan conocer en todo momento el estado y funcionamiento de la red de moneda social.

- Debe permitir a los usuarios interactuar entre ellos, incorporar información al sistema sobre la calidad de los bienes y servicios ofertados, y solicitar informaciones de este tipo para que cualquier usuario que lo desee pueda proporcionarla. Así como informar a la red sobre cualquier necesidad de un servicio o producto, o sobre cualquier otro extremo que permita mejorar la red o los servicios ofrecidos por la misma, o por cualquier persona que forme parte de ella.

- De esta manera, la plataforma virtual que sirva de sistema contable será la contabilidad oficial, si bien, como ya se expuso al hablar de los distintos formatos, es posible que dentro de algunas monedas sociales existan otras maneras de transmitirse monedas sociales en determinadas ocasiones, como sucede con las libretas, con el papel moneda, o con las aplicaciones para móviles.

Este sistema contable virtual servirá también para llevar la contabilidad de la riqueza disponible (bienes y servicios ofrecidos), para llevar la contabilidad de las necesidades insatisfechas, y también para que las personas que integran la red puedan comunicarse entre ellas aportándose ideas, recomendaciones, consejos o críticas, haciendo que la red pueda enriquecerse y mejorarse a través de este flujo de información. Además, servirá para llevar la contabilidad oficial de las monedas sociales en la red.

- - -

### 3.2.2.- Las libretas
Las libretas sirven para que dos personas reflejen sus voluntades recíprocas de realizar una transmisión de monedas sociales, y en ellas, cada usuario firma en la libreta del usuario con el que realiza la transacción, pero la libreta, aún siendo un formato elegido por la red, no está integrada en el sistema contable virtual.

Estas libretas suelen incorporar información personal del titular de la libreta, y además, en cada transacción se añade la información del usuario con el que se realiza la operación, e incluso el servicio o bien que motiva las transmisión de monedas. Si bien, dicha información puede ser ampliada o modificada por cada red.

Dicha libreta funciona como un contrato entre ambos usuarios, de manera que cada uno guarda en su libreta la firma del otro usuario, lo que permite realizar transacciones sin necesidad de tener a mano un ordenador con conexión a internet.

Las anotaciones de cada libreta serán trasladadas al sistema contable virtual cuando quien prestó el servicio o entregó el bien acceda a internet e informe a la red sobre la realización de la transacción. En ese momento, el sistema restará el importe de la cuenta del usuario que realizó el pago, y sumará dicho importe a la cuenta de quien realiza el cobro. De esta manera la persona que realiza el pago ve que se le restan las monedas que constan en su libreta por dicha transacción, y en caso de existir algún error, puede informar a quien le ha cobrado erróneamente y puede informar a la red de dicho error. La libreta sirve como prueba en caso de conflictos sobre la corrección o incorrección de las anotaciones realizadas en la red.

- - -

### 3.2.3.- Las monedas de papel
En el caso de usar papel moneda, cosa frecuente cuando se organiza un encuentro con personas ajenas a la red, resulta mucho más sencillo realizar las operaciones en el momento sin necesidad de andar escribiendo y firmando en las libretas, y no se necesita acceso a internet para su realización. Si bien, resulta necesario establecer una duración determinada para la validez del papel moneda, con la intención de que dichas operaciones sean totalizadas a la mayor brevedad posible y trasladadas al sistema contable general, para evitar incongruencias entre los saldos reales y los saldos publicados en el sistema contable.

Este tipo de formato tiene además una pérdida de transparencia añadida, ya que las operaciones realizadas en papel moneda no detallan el servicio prestado ni el bien entregado, y tampoco reflejan todos los intercambios que pueden hacerse con las mismas monedas durante la duración de dichas monedas, lo que dificulta la tarea de medir el número de transacciones totales y el importe de las mismas, ya que durante el tiempo que se usan las monedas de papel no se anotan las operaciones, sino que solo se anotarán las monedas con las que cada persona termina el evento, y las monedas que cada persona ha convertido a papel durante el evento.

- - -

### 3.2.4.- Las aplicaciones para móviles:
Las aplicaciones de móvil son un paso más avanzado, ya que permiten anotar las transacciones directamente en el sistema contable general mediante el envío de una orden de cobro realizada por el usuario que presta el servicio o entrega el bien, y una aceptación del pago por parte del usuario que lo recibe. Ambos realizarán la operación previa introducción de su nombre de usuario (o un código QR que pueda leer el móvil) y su contraseña, pero esta medida tampoco suele ser accesible a todos los usuarios actualmente, por lo que su uso no resulta siempre posible entre todos los miembros de una red de moneda social, aunque con el tiempo, resultará la forma tradicional en que se realicen los pagos dentro de las economías alternativas.

Lo normal será realizar las transmisiones de monedas sociales en el momento en el que se recibe el bien o servicio, si bien, esto no es un requisito obligatorio, ya que el hecho de que la moneda tenga su base en la confianza, permite que la prestación del servicio y el momento del pago difieran en el tiempo, según lo deseen las personas implicadas. pero debe tenerse en cuenta que mientras ambas personas no informan al sistema contable general, dicha transacción solo ha existido entre esos dos usuarios, sin que los demás puedan comprobar la realidad de la misma, por lo que los efectos de dicha transacción no alcanzarán a la red hasta que sea anotada en el sistema contable horizontal.

- - -


## 3.3.- La transparencia en la economía alternativa.

Ya se ha dicho que la transparencia es uno de los pilares básicos de las monedas sociales, en oposición a la opacidad que rige el sistema económico capitalista, ya que las redes de moneda social tratan de permitir a la gente conocer el estado real del sistema económico, así como el papel que cada persona desempeña dentro de dicho sistema, ya sea como productores, como prestadores de servicios, como consumidores, o como simples participantes de la misma.

Esta transparencia conlleva varias ventajas que benefician al sistema económico en general, por lo que pasaremos a enumerar y explicar algunas de ellas:

- La horizontalidad en la gestión: Cualquier usuario que tenga acceso a la información sobre cuánta moneda existe, cuántas monedas tiene cada usuario, cada cuanto tiempo realiza transacciones un usuario determinado, o qué transacciones realiza, permite que todos los usuarios sepan cuánto paga un usuario determinado por cada cosa, o saber de quién adquiere cada cosa, facilitando de esta manera la posibilidad de conocer las necesidades reales de las personas, identificar a quienes comercian con tales cosas, y aprender de ellos, adquirirles dichas cosas a ellos, u ofrecer cosas similares de mejor calidad, o de la misma calidad pero a menor precio. Esta horizontalidad permite que cualquier usuario que detecte uncomportamiento fraudulento pueda alertar a la red sobre el mismo, permitiendo una respuesta colectiva más rápida y eficiente de la que podría tener si dicha tarea estuviera encomendada a una sola persona.

- Ausencia de una autoridad institucional: La transparencia permite que los usuarios puedan comprobar por sí mismos, consultando la información facilitada en la red, sobre las decisiones o informaciones que puedan llegarle a través de los órganos colectivos de la red o de otros usuarios, permitiendo que cada usuario pueda decidir con qué usuarios decide comerciar, y con qué usuarios prefiere no hacerlo, qué precio quiere ponerle a un producto o a un servicio, o cuánto está dispuesto a pagar por dicho servicio. Y esto resulta posible gracias a que no existe un órgano centralizado que describa unilateralmente el estado de la moneda, sino que dicho estado, así como el funcionamiento de la misma, es descrito por todos los usuarios simultáneamente a través de la cogestión del sistema contable general.

- Identificación de productores y prestadores de servicios: La transparencia permite saber si la persona que vende algo ha producido ese algo, o si lo ha adquirido de otro miembro y funciona como simple intermediario. En caso de ser productores, se permite al usuario saber quién es la persona que produce ese bien, y preguntarle sobre su modo de producción o sobre cualquier otra cosa que pueda resultar de interés para el consumidor.

- Eliminación de los intermediarios: Al saber si una persona produce los bienes que vende o los adquiere de una tercera persona, se ofrece al consumidor la posibilidad de saltarse al intermediario y adquirir directamente los productos y servicios al productor de los mismos. Del mismo modo que permite identificar a aquellos usuarios que participan como intermediarios de manera habitual, y alertar sobre su existencia a la red, con la intención de que todos tengan conocimiento del incremento en el precio que dicho intermediario supone. Esto abre la posibilidad a que algún usuario decida no comerciar con intermediarios, bien no comprando aquello que venden, y tratando de comprar directamente al productor, o bien decidiendo no venderle a estos intermediarios y tratando de vender únicamente a quienes adquieren los productos como consumidores finales. También podrán presionar a los intermediarios que resulten necesarios para que bajen sus precios, o podrán coordinarse para remplazarlos.

- Identificación de los acumuladores de moneda: La transparencia permite saber el saldo de los usuarios, lo que permite saber qué usuarios están acumulando un número muy elevado de monedas, permitiendo adoptar medidas para frenar dicha acumulación, como puede ser el dejar de comprar a ese productor y comprar a otros que ofrezcan productos similares, ofrecer servicios o bienes parecidos a los suyos para que los demás usuarios tengan otras alternativas de consumo, o bien preguntando directamente a ese acumulador qué cosas necesita para ofrecerlas en la red y permitirle gastar las monedas que acumula.

También permite detectar y reaccionar frente a las personas que rebasan su limite de saldo negativo, o aquellas que mantienen sus saldos negativos durante un periodo prolongado de tiempo, poniéndose en contacto con ellas para facilitarles alguna posibilidad de recuperar saldos positivos, o informando a la red de la situación.

Hay redes que incluso permiten la expulsión de las personas que acumulan un saldo negativo excesivo, pero para ello debe haber personas que asuman ese saldo negativo a modo de garantía, y si la persona expulsada decidiera volver a la red, tendría que devolver dichas monedas sociales a las personas que asumieron su saldo negativo antes de hacerlo.

La misma red irá poco a poco descubriendo otras muchas ventajas que tiene la transparencia económica, ya que la transparencia es la que permite que un sistema económico sea realmente una herramienta al servicio de los ciudadanos, en lugar de una caja negra dentro de la que puedan suceder cosas que los ciudadanos ignoren, y que, a largo plazo, pueden terminar perjudicándolos.

Hay quienes verán un problema en que cada persona tenga la oportunidad de consumir 100 unidades sin entregar nada a cambio y aceptar su posterior expulsión, pero eso no es algo que deba preocuparnos cuando en el sistema oficial hay banqueros que hanconsumido y siguen consumiendo millones de unidades monetarias sin ofrecer nada a cambio, y que a día de hoy siguen sin ser identificados por la sociedad, lo que impide que la misma sociedad tome medidas contra ellos ni pueda expulsarlos de su sistema económico.


- - -


## 3.4.- La responsabilidad heredada por la emisión de moneda social.

A raíz de lo dicho sobre la transparencia, podemos deducir que la red no solo permite saber quienes poseen las monedas de la red, sino que también resulta posible saber quiénes son las personas que han emitido las monedas que circulan en la red, ya que, como se dijo, para la emisión de monedas es necesario una persona dispuesta a emitir la promesa de devolver un bien o servicio a la comunidad, y también hace falta una persona que tenga confianza en que esa persona va a cumplir su promesa.

La transparencia permite identificar de manera clara a la persona que ha emitido moneda social, en la medida en que quienes tienen un saldo negativo en la red, es porque han emitido moneda social debido a que necesitaron un bien o servicio y no tenían moneda social en ese momento, de manera que, dentro de los márgenes máximos que permita cada red, dicha persona decidió cuantificar la confianza depositada en ella, y emitió moneda social para poder comerciar.

En este sentido resulta interesante señalar que dicha persona no necesita pedir prestadas esas monedas a otro usuario, sino que ella misma es la que emite dichas monedas y es quien se compromete a producir riqueza dentro de la red por una cantidad equivalente, y lo hará sin pagar intereses a nadie por dicha emisión.

Pero la responsabilidad que asume la persona que emite moneda social, va vinculada de manera estrecha a la persona que acepta en nombre de la red dicha emisión, ya que la transparencia permite conocer el saldo de cada usuario, y es responsabilidad de quien acepta moneda social comprobar la existencia de un saldo disponible y una oferta realizada en la cuenta de quien emite esas monedas, y de garantizar que la confianza que deposita la red en esa persona no es una confianza inexistente.

Pondremos el ejemplo de una persona que ha llegado a su límite máximo de emisión y trata de realizar una nueva operación en moneda social. En dicho caso, la persona que va a prestar el servicio o entregar el bien, debe ser consciente de que dicha persona no dispone de capacidad para emitir más moneda social dentro de la red, por lo que es responsabilidad de esta persona informar a quien pretende emitir más moneda de que no puede hacerlo hasta que haya generado monedas sociales y haya reducido su saldo negativo.

Los usuarios que no cumplen dicho deber, serán responsables frente a la red de la emisión de esas monedas, ya que dichas monedas realmente no pueden ser emitidas por quien pretendía entregarlas.

Este usuario que hereda la responsabilidad se dará cuenta de ello cuando trate de realizar el cobro y vea que el sistema no le permite cobrar debido a que el otro usuario no puede sobrepasar el límite negativo en el que se encuentra.

También puede suceder que un usuario que aún puede emitir moneda social trate de comerciar la adquisición de algún producto con otro usuario al que no conoce, pero al hacerlo, el usuario que acepta sus monedas reduce la transacción a un simple intercambio de monedas, aceptando las monedas y comprobando posteriormente que quien le ha adquirido el bien es un usuario nuevo que aún no ha ofrecido ningún servicio o producto a la red y al que ningún otro miembro de la red conoce ni puede garantizar que preste servicios o produzca ningún tipo de bien.

En este caso, el usuario tendrá la responsabilidad de contactar con este usuario nuevo, e informarle de que el funcionamiento de la red exige haber ofrecido algún bien o servicio en la plataforma general contable, y que exige también la intención de aportar a la red bienes o servicios por un valor equivalente a lo recibido. En caso de detectarse irregularidades que hagan pensar que un usuario determinado no va a devolver nunca el valor recibido, habrá que plantearse hasta qué punto dichas unidades monetarias deben ser pagadas por la red, o deben ser pagadas por el usuario que atribuyó confianza a una persona con la que no tenía confianza personal, y no comprobó si la red tenía confianza en esa persona.

El sistema económico alternativo permite que sea la red quien asuma este tipo de emisiones fraudulentas o defectuosas que son realizadas por terceras personas que se incorporan a la red para emitir unas cuantas monedas y desaparecer tras llevarse pequeñas cantidades de productos o servicios con ellas, pero resulta interesante señalar que la confianza es el pilar clave de las monedas sociales, y que, teniendo clara esa premisa, se evitarán este tipo de situaciones dentro de las redes de moneda social.

Por suerte, estos casos de responsabilidad heredada son muy escasos y poco frecuentes, ya que debido a la naturaleza y el funcionamiento de la red, cualquier persona que se aproxima a una red de moneda social que funciona de manera transparente, y funciona de una manera eficaz, termina comprendiendo que siempre será más rentable permanecer en la red participando en ella, que coger unos cuantos productos y ser expulsado de la misma, ya que fuera de esa red de moneda social, lo que existe es el capitalismo puro y duro, o en el peor de los casos, otras monedas sociales iguales a esta de la que se autoexcluye con sus acciones.

Con esto deseamos poner énfasis en que no debe ser un motivo de gran preocupación la aparición de estas terceras personas que traten de aprovecharse de la red, pero hemos considerado necesario mencionar la existencia de estos mecanismos de responsabilidad heredada para aquellas redes que pasen por alto el requisito previo de la confianza como base de toda red de moneda social. Si no existe confianza, es mejor crear una red de confianza con forma de banco de tiempo antes de crear una red de moneda social.

- - -

## 3.5.- El balance total a cero. Equilibrio y Gravedad

El balance total a cero debe ser tratado desde dos prismas distintos, ya que en primer lugar hay que enfatizar sobre el hecho de que el balance total de la red de moneda social debe ser siempre cero. A la dinámica que hace inevitable este balance total a cero, en sentido general, la llamaremos "Ley de Equilibrio" Pero también aludimos con balante total a cero a la dinámica especial de las monedas sociales en la que las personas que pertenecen a la misma deben tratar de tener sus saldos siempre lo más cercanos al cero que les resulte posible. A esta segunda inercia especial la llamaremos "Ley de Gravedad", por su semejanza con la atracción gravitacional de los cuerpos entre sí. Así que veremos ambas cuestiones por separado.

La Ley de Equilibro es la que establece que "independientemente del número de monedas generadas, y del movimiento de las mismas, el saldo total de una red de moneda social debe ser siempre cero". Es por ello que cada vez que en la red aparezca una moneda social, dicha moneda aparece generando dos saldos, uno positivo y uno negativo, de idéntico valor. Dichos saldos podrán moverse dentro de la red, y ser traspasados de unas personas a otras, haciendo que en la red haya siempre el mismo número de monedas positivas que de monedas negativas. Cualquier acontecimiento que perturbe esta Ley, estará alterando el normal funcionamiento de la moneda y podría ser considerado como un error del sistema económico. Esto sirve para impedir que alguien pueda generar e incorporar monedas a la red de moneda social, a menos que haya alguien que asuma el saldo negativo de esas monedas. Esta ley resulta de extremada importancia a la hora de establecer relaciones entre distintas monedas sociales, ya que en dichos casos, cada moneda social funciona como si fuera una persona dentro de una red moneda social, solo que aquí son las diferentes redes de moneda social las que empiezan a crear una nueva red de monedas sociales federadas.

Los saldos positivo y negativo que suponen la creación de moneda social, existen en la red hasta que ambos saldos coinciden en una misma persona de la red, momento en el que dichos saldos se compensan y se neutralizan, haciendo desaparecer las monedas sociales del sistema contable. De esta manera la red podrá tener un número mayor o menor de monedas en cada momento concreto, ya que si las personas que prestan servicios a la red, reciben servicios de la red por idéntico valor, la red estará en equilibrio, y todos tendrán sus saldos en cero, sin que nadie deba nada a la red, ni resulte acreedor de la red por ningún servicio prestado.

Sin embargo, si las personas que prestan servicios a la red son distintas de las personas que los reciben, habrá personas con saldos positivos y negativos, y en consecuencia, existirá moneda social circulando por la red, reflejando con ello un desequilibrio entre las necesidades y los recursos de la red, ya que habrá personas que estén aportando a la red más de lo que reciben de ella, y del mismo modo habrá quienes estén recibiendo de la red más de lo que aportan a la misma. La Ley de Equilibro es la que permite que la existencia de moneda social pueda ser utilizada como referente para analizar el equilibrio entre los miembros de la red, así como saber a qué personas son a las que debe tratarse de solventar necesidades, y con qué personas debe contarse a la hora de pedir ayuda.

Del mismo modo, esta Ley de Equilibrio es la que impide que una persona determinada pueda incorporar fraudulentamente monedas a la red, ya que en cuanto el saldo total de la red resulta positivo o negativo, es porque existe algún error, y ello alertará a todos los miembros de la red para localizar el problema y adoptar las medidas necesarias para solucionarlo y evitar que dicho problema se pueda repetir en un futuro.

La Ley de Gravedad es la segunda perspectiva del balance a cero, y es la tendencia de las personas que forman parte de la red a tener sus saldos lo más cercanos posible al cero. Y esto sucede por varias causas.

La primera causa es que la gente tratará de evitar tener saldos negativos, ya que eso les supone un compromiso con la comunidad que tratarán de cumplir a la mayor brevedad posible, ya que al existir un límite negativo a partir del cual la red dejará de interactuar con esas personas, la gente trata de alejarse de dicho límite, además de por la costumbre heredada del capitalismo de considerar mejor tener un saldo positivo que un saldo negativo. La otra causa es que el ahorro no se premia de ninguna manera dentro de la red, por lo que acumular un saldo positivo cada vez mayor no reporta ningún beneficio a su titular, por lo que la tendencia de las personas suele ser la de gastar las monedas sociales en cuanto ve algo que le pueda ser útil ofertado dentro de la misma.

A esto se suma la circunstancia de que las redes no suelen tampoco ofrecer todos los servicios y productos necesarios, por lo que puede darse el caso de gente que al ver que acumula muchas monedas, y no vea la manera de gastarlas, deje de prestar servicios en la red para dejar de acumular monedas que no sabe si podrá gastar. De esta manera, la gente huirá de una acumulación excesiva de monedas, y del mismo modo huirá de una acumulación de saldos negativos, haciendo que la gente solo pueda encontrase bien en la red de moneda social en la medida en que se mantienen sus saldos cercanos al cero.

Poniendo en relación ambas leyes, podemos entender que una persona utilizará la red para satisfacer sus necesidades, y no para hacerse rico, por lo que a nivel individual, todos tratarán de mantenerse en equilibrio con la red, y del mismo modo que el balance total de la red siempre es cero, los miembros de la red, tratarán de que sus balances individuales también lo sean durante el mayor tiempo posible, o tratarán de alejarse lo menos posible de dicho estado de equilibrio.

Quienes acumulan un saldo positivo, saben que han trabajado para la red, y que la red deberá devolverles el favor. Quienes tienen el saldo negativo, saben que la red ha trabajado para ellos, y tratarán de devolver el favor para que siga haciéndolo en el futuro.

De esta manera, ambos tipos de saldos tendrán una tendencia a atraerse, ya que quienes tienen saldos negativos pueden conocer quienes poseen un saldo positivo, y entablar relaciones con dichas personas, sabiendo que estas personas tienen la necesidad de gastar sus monedas sociales. Y en sentido inverso, quienes tienen un saldo positivo, saben que pueden acercarse a las personas de saldo negativo para pedirles ayuda, ya sea en aquello que ofertaron, o bien con cualquier otra cosa que consideren factible para cualquiera. No debemos olvidar que muchos usuarios solo ofrecen en la red aquello que prefieren ofrecer, omitiendo muchas otras cosas que pueden hacer, y dichas cosas pueden ser demandadas por los demás miembros de la red.

Con estas dos leyes, se tiende a minimizar el número de moneda social circulante, ya que si recordamos la eficiencia de la moneda social que se explicó al principio de este manual, "siendo constante el número de operaciones en un periodo de tiempo, cuanto menor es el número de monedas utilizadas, más eficiente resulta la red".

Aplicando la fórmula de dividir el número de personas usuarias entre el número de monedas, podremos obtener el ratio de eficacia, y cuanto mayor sea dicha eficacia, más eficaz estará resultando la moneda. Para poder valorar este ratio, tendremos como premisa que dichos ratios serán siempre un número positivo, ya que siendo elnúmero de personas un número positivo, y el número de monedas en circulación también un número positivo, por fuerza el resultado será un número positivo. Del mismo modo, como lo normal es que haya más de una moneda por persona al emitirse una moneda, dicho ratio comenzará siendo inferior a 1, por lo que será un número decimal, y cuanto más se acerque al número 1, más eficaz estará siendo dicha red.

Una vez que se alcanza una eficacia superior al número 1, es señal de que se avanza por el buen camino, ya que en la red habrá más personas usuarias que monedas (incidiendo en la diferencia entre las personas usuarias y las personas censadas). La eficacia infinita sólo puede obtenerse cuando el número de monedas sociales llegue a ser cero, en cuyo caso, cualquier número de personas, dividido entre cero monedas, arrojará un resultado infinito. De ahí esta tendencia a la desaparición de las monedas sociales como máxima demostración de eficacia en un sistema económico alternativo.

Para representar gráficamente estas dos leyes, se recurre al símbolo del círculo, como representante de los ciclos, mostrando gráficamente un círculo en el que la mitad se recorre al aportar un bien o servicio a la red (venta), y la otra mitad se recorre cuando se adquiere un bien o servicio de la red (compra), dejando el círculo cerrado y volviendo al saldo cero. También es una casualidad gráfica que el cero sea representado precisamente por un círculo. Así, en cada operación con moneda social se estará creando medio círculo, y es bueno tener claro que el funcionamiento de la red depende de que dichos círculos terminen siendo completados.

Así podemos entender que cada círculo representa realmente un intercambio que ha tenido lugar dentro de la red de moneda social, recuperando en cada círculo completo la concepción unitaria de la compra y la venta como partes de una misma realidad llamada intercambio, lo que supone un avance de comprensión respecto a la economía que nos ha impuesto el sistema capitalista. Una compra aislada, o una venta aislada no supone una operación económica completa, sino que las operaciones económicas reales en la economía alternativa son estos intercambios que tratamos de representar con la forma de círculos.

- - -

## 3.6.- La entrega real del producto o servicio

Hasta ahora hemos visto la entrega y transmisión de las monedas sociales, por lo que ahora haremos un inciso para analizar algunos aspectos de la entrega de los bienes y servicios, ya que hemos visto que la contabilidad de las monedas resulta transparente y supervisable por todos los usuarios, pero ¿existen mecanismos que garanticen la correcta entrega de los bienes y servicios que justifican la entrega de monedas sociales?. Si no los hay, deben ser creados. Las redes de moneda social deberían disponer de los siguientes mecanismos de garantía para la entrega de bienes y servicios.

- a) Pago simultáneo a la entrega: La persona que entrega las monedas sociales no tiene por qué entregar las monedas hasta que haya recibido el bien o servicio, y si lo hace es porque confía plenamente en la persona a la que entrega la monedas. De esta manera, se puede examinar el bien o la prestación del servicio antes de aceptar la transmisión de monedas sociales.

- b) Transparencia previa al pago: Antes de realizar la entrega de monedas, e incluso antes de pactar con alguien la entrega de un bien o servicio, la red nos permite comprobar si dicha persona ha realizado servicios o entregado productos similares con anterioridad. E incluso podemos contactar con aquellas personas que los adquirieron preguntarles sobre la calidad del servicio o producto.

- c) Rechazo del pago: Otra posibilidad es la de rechazar el pago si el producto no resulta ser el pactado después de la entrega, o si el servicio no se presta adecuadamente, ya que la red ofrece un plazo para rechazar un pago, que suele ser de 7 días a contar desde que se carga en la cuenta de quien recibe el bien o servicio.

- d) Publicidad de los conflictos: Para mejorar este servicio, la red  puede incorporar una herramienta de evaluación en la que cada persona puede evaluar la calidad de la persona que le ha prestado un servicio, o la de un bien que le haya sido entregado, así como puede evaluar la calidad de la transacción en general, señalando si alguna persona se negó a pagarle, o si le rechazó el pago después de entregado el bien o prestado el servicio. De esta manera, si una persona tuviera conflictos con algún miembro de la red, podría hacerlo constar en la red, para que los demás usuarios tengan conocimiento de los conflictos que pueda haber. Esto permite conocer e identificar a las personas que suelen tener conflictos con los miembros de la red, y alertar a los demás sobre la posibilidad de tener conflictos al pactar con determinadas personas. Y del mismo modo, esta publicidad, permite a los demás miembros adoptar soluciones para evitar este tipo de conflictos en un futuro.

- e) Respuestas de la red: Cada red puede ofrecer a su vez diferentes herramientas para evitar perjuicios a los miembros de la red a consecuencia de malas prácticas de alguna persona de la red. Dichas herramientas pueden ir desde la publicidad formal de los conflictos, hasta una caja de garantía que pueda responder por el pago de los bienes o servicios prestados cuando algún miembro de la red se niegue a aceptar un pago al que esté obligado, o la prestación de los servicios no prestados, o incluso la recomendación de no pactar con determinados usuarios debido a su carácter reincidente en tener conflictos, o por una mala fe manifiesta en alguna transacción.

Evidentemente, la red, al ser una aglomeración de personas, no deberá tomar una decisión de manera colectiva, sino que serán todas las personas que la integran quienes decidirán individualmente si siguen confiando o no en una persona conflictiva, sabiendo que para este tipo de casos existe la responsabilidad heredada dentro de la red. Esto incorpora un mecanismo democrático al funcionamiento de la red, ya que si la mayoría de la gente se niega a pactar con una persona, quien acepte pactar con ella sabe que se expone a que esa mayoría de la red deje de pactar con ella también. Pero por suerte, en las monedas sociales que tienen su base en la confianza entre las personas, estos casos no suelen darse, y si se diesen, sería por una persona ajena a la red que se introduce en ella para crear este conflicto, pero que es detectada rápidamente, y suele ser neutralizada antes de poder ocasionar ningún daño serio.

Esta fragmentación de la economía en núcleos de confianza es una consecuencia inevitable del sistema económico alternativo mientras la gente siga tratando de engañar, falsear o no respetar los principios económicos generales de cada red de moneda social, lo que llevará a que primero surjan diferentes redes de moneda social entre personas de confianza, y en sus fases de crecimiento interactúen y construyan federaciones de redes entre aquellas redes que respeten dichos principios. Quienes pretendan no respetar la transparencia, la confianza y la democracia de la red, deberán tratar de buscar otras redes económicas en las que la democracia, la confianza y la transparencia no resulten pilares esenciales. En este manual nos referimos exclusivamente a las redes de monedas sociales que, más allá de sus nombres, asumen estos principios en su funcionamiento.

- - -

## 3.7.- La desaparición de las monedas sociales.

Después de ver que el balance total a cero, en sus dos vertientes como Ley de Equilibrio y Ley de Gravedad, crea en la red una tendencia a reducir el número de monedas todo lo posible, y añadiendo a dicha tendencia los problemas reales que puede haber a la hora de hacer efectivas las entregas de bienes y servicios, o los pagos efectivos en moneda social, podría pensarse que las monedas sociales están destinadas a desaparecer desde el momento en que empiezan a funcionar conforme a dichos principios y frente a tales problemas. Y tendrán razón para pensar así.

Pero no debe olvidarse lo que realmente supone una moneda social. La moneda social es "el reflejo contable de un desequilibrio en la economía solucionado provisionalmente con la primera parte de un intercambio, dejando pendiente de realizarse la entrega de la cosa o servicio intercambiado". De esa manera, la existencia de monedas no es más que el apunte contable de una persona que ha confiado en otra, y ambas personas han cuantificado esa confianza en un número determinado de monedas sociales. Por eso una persona tendrá un saldo negativo siempre que alguien tenga un saldo positivo, y ambas personas tenderán a atraerse para neutralizar sus saldos y recuperar sus equilibrios personales, y devolver algo de equilibrio a la Red.

Esta atracción puede considerarse como una metáfora de la entropía del Universo, en la que, tal y como el Universo tiende a equilibrarse, lo hace la moneda. Y cuando el Universo alcance su punto máximo de entropía, no habrá movimiento, y el Universo, tal y como lo conocemos, habrá dejado de existir. Igualmente, la moneda social, al alcanzar su punto máximo de equilibrio, en el que no existan monedas sociales dentro de la red, detendrá esta manifestación contable de la economía, y ello podrá deberse a varias razones.

Una de ellas puede ser que la gente haya perdido la confianza en la red, y en ese caso, la red, efectivamente, habrá desaparecido. Esto sería una moneda social que ha fracasado.

Pero el balance total a cero también puede deberse a que dentro de la red, ya todas las personas tienen cubiertas sus necesidades básicas en un grupo de confianza, y entre ellos, ya no se apuntan los favores, sino que saben con toda certeza que la red va a responder a sus demandas, porque a la vez, saben que esa persona responde a las demandas de la red. Con ello habrán suprimido de la red el problema de valorar los bienes y servicios entre esas personas, y no tendrán necesidad de utilizar ninguna moneda entre ellas. Esto supone una victoria de la red, que se verá de repente en una fase en la que, como red, ahora interactúan con otras redes económicas, pero entre ellos, serán una red económica que no necesitará monedas. Las familias suelen ser un ejemplo de este tipo de economías, en la que las personas que la integran tratan de ayudarse y no suelen pedirse dinero por ello, aunque también haya familias en las que se haya preferido optar por otro tipo de economías con contabilidades entre sus integrantes.

Si se alcanza una fase de victoria en la que los miembros, aun teniendo un sistema contable en monedas, consiguen funcionar como una red, el uso de las monedas que para ellos resulta innecesario, seguirá siendo una herramienta que les permita diferenciar sus principios económicos, de los de aquellos otros sistemas económicos con los que interactúen, ya sea de manera individual, ya sea de manera colectiva. Y lo que resulta más importante y beneficioso es que esa red, sin monedas, conserva toda la información sobre productores de bienes, prestadores de servicios y consumidores de ambas cosas que siguen formando parte de la red de moneda social, lo que permite que se conserven las relaciones económicas entre las necesidades a satisfacer y los recursos disponibles para ello. La Red necesitará bienes y servicios ajenos a la red durante bastante tiempo, pero sus victorias como red consisten en que cada vez sea menor la parcela de sus economías que deban ser solucionadas fuera de su propio sistema económico, y crecen a medida que consiguen que el uso de su sistema económico sea utilizado cada vez más a menudo, por más gente y para más cosas, independientemente de que el sistema conserve su dinámica tendente al balance cero y a la desaparición de las monedas. Para ello, habrá que mencionar el problema crucial de las monedas sociales en sus inicios: La financiación, o mejor dicho, su afán por financiarse en moneda oficial.

- - -

## 3.8.- El sostenimiento o financiación de la moneda social.

Cuando un red de moneda social empieza a ser sólida, es cuando las personas que forman parte de ella empiezan a pretender que la moneda social les permita cubrir todas sus necesidades, porque la moneda deja de ser percibida como un juego económico, para empezar a convertirse en un sistema económico de verdad. Y al tratar de vivir utilizando únicamente este sistema económico es cuando empieza a ponerse de manifiesto la realidad de que "no todo el mundo entiende este nuevo sistema económico", y el impedimento de que "hay mucha gente que sigue utilizando únicamente el sistema económico oficial".

Con ello, las redes empiezan a darse cuenta de que necesitan moneda oficial para acceder a aquellos servicios que no produce la red, ya que aunque la red haya recuperado la soberanía económica, en la medida en que ahora puede generar dinero dentro de su sistema económico, en dicho sistema económico no se producen todos los recursos, por lo que la red puede carecer de soberanía alimentaria, soberanía energética o de otros tipos de soberanías. De esa manera, verán que para alquilar o adquirir inmuebles suelen necesitar dinero oficial, así como para pagar los impuestos, o servicios monopolizados por el sistema económico oficial, como pueden ser la luz, el agua, el teléfono o el gas.

La red que llega a esta fase, se ve inevitablemente en un punto en el que le resulta necesario interactuar con el sistema económico oficial para seguir cubriendo estas necesidades, lo que hará que solo consiga funcionar en un tanto por ciento de su economía total, ya sea un 10%, un 30%, un 50% o un 80%. Pero no se puede considerar que existe un sistema económico alternativo hasta que dicho sistema permite vivir al 100% dentro de él, sin tener necesidad de utilizar moneda oficial.

Al principio es cuando más evidente resulta que la red debe moverse sin dinero oficial, o con el menor dinero oficial posible, y por eso el primer acto colectivo de cara a la sociedad suele consistir en un mercado que tiene lugar en un lugar público y de uso gratuito, y se hace con el trabajo personal y directo de las personas que desean implicarse en dicho mercado. En dichos mercados los miembros de la red ofrecen sus excedentes de productos y servicios (aquello que no es consumido dentro de la red) a personas ajenas a la red, con la peculiaridad de que, durante ese día, las personas ajenas a la red pueden entrar a formar parte de la red adquiriendo moneda social a cambio de aportar moneda oficial a la red.

De esa manera la red consigue cambiar los bienes y servicios de las personas de la red por monedas oficiales de personas ajenas a la red, y con ello se consigue un ingreso en moneda oficial que permite a la red adquirir en el sistema económico oficial aquellos recursos que no puede producir por sí misma, y ponerlos a disposición de la red para  su adquisición con moneda social. Esta actividad es la que se conoce como descapitalización, porque a medida que esta función de las monedas sociales se extiende, hay cada vez una mayor parte de la economía que va teniendo lugar dentro de la red de moneda social, lo que va permitiendo que cada vez haya más gente queriendo moneda social y aceptando moneda social. Pero esta función se verá con mayor detalle en la parte final de este manual.

De momento, a efectos de la financiación de la moneda social, lo que conviene tener claro, es que la red debe entender que su fuerza motriz, y sus recursos, son realmente la confianza que se tienen las personas entre ellas, así como sus capacidades, habilidades y conocimientos, y los recursos y bienes que están dispuestos a integrar en el sistema económico alternativo. Todo ello habrá que hacerlo tratando de no necesitar dinero oficial, o necesitando poco, ya que solo podrán obtener dinero oficial a través de esos mercados en los que se permita a terceras personas adquirir productos de la red a cambio de monedas oficiales, y esto debe hacerse cuidando que la red no se convierta en una herramienta de propaganda que utilicen personas concretas para conseguir dinero oficial, ya que ello hará que la red no consiga llegar a la fase de descapitalización, y termine funcionando únicamente como una red para la fidelización de la clientela en la que cada persona sigue ganando monedas oficiales para sí misma, y sigue haciendo su vida personal totalmente dentro del sistema económico oficial.

Esto sucede con frecuencia cuando las redes de moneda social empiezan a considerar los pagos en moneda social como ofertas, descuentos o simples inversiones en promoción y propaganda. Pero insistimos en que este tipo de usos defectuosos de la moneda social, se verán más en detalle en capitulo relativo al "ecolatipac".

De momento empezaremos a ver los primeros pasos que darán las monedas sociales y los sistemas económicos alternativos cuando han empezado a funcionar, y pretenden moverse en busca del crecimiento.

* * *

# 4.- CONSOLIDACIÓN DE LAS MONEDAS SOCIALES

## 4.1.- Las estructuras legales

Uno de los primeros debates que suele abrirse en las monedas sociales es precisamente la necesidad de adoptar alguna forma jurídica que permita a la red de moneda social tener una personalidad jurídica capaz de interactuar en el sistema económico oficial como una persona independiente de las personas que la componen. Con ello se permite a esa persona jurídica ocuparse de solucionar los problemas de interacción con la economía oficial, permitiendo a las personas usuarias llevar una economía que pueda permanecer al 100% dentro del sistema económico alternativo.

Esto solo podrá hacerse de manera correcta si las personas de la red tienen clara la necesidad de transparencia y participación dentro de la red, ya que crear estructuras jurídicas (ya sean asociaciones, cooperativas, fundaciones o sociedades) que no resulten transparentes o que no permitan la participación de las personas implicadas, suelen llevar a la desaparición de la confianza, por la sencilla razón de que la gente tiene tendencia a recordar que "quien algo esconde, es porque algo tiene que esconder".

Del mismo modo, no resulta suficiente con una forma jurídica transparente y participativa para que funcione la moneda social, ya que, como hemos dicho antes, si no existe realmente una red de moneda social a la que dotar de personalidad jurídica, esa personalidad jurídica será realmente un traje vacío, muy bonito y funcional, pero sin un cuerpo al que vestir. Redactar unos estatutos no hace que aparezcan de la nada las personas productoras ni las que prestan servicios, ni las que entienden el sistema económico alternativo, ni las que se tienen confianza entre ellas.

Es por ello que debe tenerse claro que la personalidad jurídica es una herramienta a disposición de las redes de moneda social, pero no son realmente una red de moneda social. Y es por eso que la utilización de la forma jurídica no se ha explicado hasta este momento, ya que antes de que exista la red de moneda social tampoco tiene sentido tener una forma jurídica para ella.

- - -

### 4.1.1.- La voluntad de la forma jurídica
Esta herramienta jurídica que supone para la red dotarse de unos estatutos de funcionamiento, equivale a la creación de una nueva persona que será una mezcla de todas las personas implicadas en la red, cuyo código genético, o cuyo código moral, será redactado y aceptado por las personas físicas que la integran. Dicho código podrá regular más o menos cosas, pero existen una serie de cosas que deben ser establecidas en el código de esa persona jurídica, ya que ha sido creada con el objetivo de dotar de una forma legal a un sistema económico alternativo. Dichas cosas básicas serán las siguientes:

 - Objetivos: Habrá que definir los objetivos de la red, y si bien estos objetivos pueden ser muy variados atendiendo al caracter de cada red, al tener como base los principios de la economía alternativa deberán incluir entre sus objetivos el relativo a las necesidades, que sería algo así como "satisfacer las necesidades de consumo de todas las personas que formen parte de ella". De esta manera, podrá decirse que estas estructuras jurídicas deberán constituirse como "grupos de consumo", bajo cualquiera de las formas legales que permiten realizar de manera colectiva este tipo de actividades. Y de otra parte, debe incluir como forma de participación "la obligación de los socios de aportar su tiempo y esfuerzo personal para la consecución de los objetivos", lo que llevará a mezclar al grupo de consumo con la facultad de sus integrantes de aportar cuanto esté a su alcance para que el grupo pueda consumir todo aquello que necesite, sin imponer la obligación de que dichas aportaciones sean en moneda oficial, ya que eso limitaría las posibilidades de aportación de aquellas personas que están tratando de alejarse del sistema económico oficial. Lo esencial es aportar "riqueza" a la red (independientemente de si su valoración se hace en moneda oficial, si se hace en una moneda alternativa, o si se hace sin utilizar moneda) para satisfacer todas las necesidades de consumo de todas las personas de la red.

- Órganos portavoces: Serán las personas físicas encargadas de transmitir la voluntad de la forma jurídica al sistema económico oficial, y si bien las leyes pueden exigir que sean designados como órganos de representación, o establecer nombres y funciones concretas para dichos órganos, los estatutos pueden establecer mecanismos de control sobre ellos, de rotación cada cierto tiempo, de asunción de responsabilidades por quienes los ocupen, u otros mecanismos cualesquiera que priven a dichos órganos del poder de tomar decisiones en nombre de la red, y que les obliguen a rendir cuentas frente a las personas que integran la red.

- Transparencia: Debe regularse el grado de transparencia de la red, garantizando la mayor transparencia posible de cara a las personas usuarias, para que todas ellas puedan conocer en todo momento, y con todo detalle, el funcionamiento del sistema económico interno, los problemas existentes y las posibles propuestas planteadas para solucionarlos. Esta parte incluye el derecho a la información, para que cada persona pueda tomar sus decisiones individuales estando bien informado.

- Participación: Deben regularse los mecanismos con los que las personas usuarias podrán participar en la red, incluyendo tanto su aportación individual a las necesidades de la red, como su participación individual en la toma de decisiones colectivas. Para ello resultará necesario delimitar qué decisiones deben ser colectivas, y por oposición, resultará posible saber qué decisiones son individuales.

- Democracia: El código debe establecer de qué manera concreta se adoptarán las decisiones colectivas, y tratar de que en dichas decisiones puedan opinar y ser tenidas en cuenta todas las personas usuarias.

Debe tenerse muy clara la diferencia entre la democracia y la participación, ya que si bien son materias que podrían entenderse como similares, resulta del máximo interés para el funcionamiento de una red tener la participación como bisagra entre las decisiones individuales y las colectivas, ya que muchas veces, la decisión tomada colectivamente puede terminar convirtiéndose en una imposición de una mayoría sobre una minoría, mientras que haber considerado dicha decisión como individual, habría permitido a todas las personas informarse sobre la materia en cuestión, adoptar su propia decisión, y permitir que sea la dinámica y la inercia de la red la que acabe definiendo a la propia red, sin necesidad de imponer una decisión a través de un acuerdo. Reducir los mecanismos a Transparencia y Democracia, suprimiendo esta participación dificulta el objetivo de limitar el poder de los órganos de portavocía, ya que dichos órganos se verán obligados a pronunciarse en nombre de la red en muchas materias, y conviene tener claro sobre qué materias pueden pronunciarse, y sobre cuales deben abstenerse de hacerlo, por existir libertad individual sobre ellas.

Una vez que se tiene claro por toda la red que una materia es colectiva, dicha materia deberá ser tratada en términos de democracia (ya sea estableciendo el consenso, la mayoría simple, o algún otro tipo de mayoría para la adopción de un acuerdo), mientras que si una materia no se considera colectiva, deberá ser tratada según los términos de la transparencia, permitiendo a las personas usuarias informarse seriamente sobre dicha cuestión y tomar una decisión personal, sin que nadie pueda imponerle la obligación de hacer algo que no desea hacer, y sin permitirle imponer su voluntad a nadie, ni si quiera a través de una votación democrática.

- - -

### 4.1.2.- Las relaciones con terceras personas
Esta forma jurídica será una herramienta que permita a la red interactuar con terceras personas en nombre de la red, dando una mayor consistencia a la propia red, ya que la red empezará a poder actuar de manera directa en la economía, además de poder hacerlo a través de las personas que la integran, ya que dichas personas, dispondrán, a parte de su personalidad física para interactuar con otras personas, de una forma jurídica que podrán utilizar como intermediaria en su relación con personas ajenas a la red.

Esto supone una ventaja, ya que permite a las personas usuarias apartarse del sistema económico actual de una manera más fácil, dejando que sea la estructura legal la que interactúe por ella con las personas ajenas a la red en aquellas situaciones en las que resulte necesario.

- - -

### 4.1.3.- Las relaciones entre las personas usuarias
También supone una ventaja en la medida en que las relaciones entre las personas usuarias suelen estar encerradas dentro de un marco económico y legal definido por el sistema oficial y, por defecto, no tienen otro sistema que ese. Una estructura legal con personalidad jurídica independiente, tiene la facultad de definir sus reglas de funcionamiento interno, creando un nuevo marco económico y legal que, si bien solo resultará efectivo dentro de la red, servirá para crear un nuevo espacio por el que las personas usuarias podrán moverse, teniendo la posibilidad de definir ellas mismas las normas de ese nuevo espacio, tanto a nivel económico como legal, en el que se relacionarán entre ellas, dejando que sea la estructura jurídica quien se encargue de las relaciones con las personas ajenas a la red.

De esta manera se puede hablar de dos universos diferentes, uno en el que las personas funcionan de acuerdo con el sistema oficial, y otro en el que las personas que integran la red pueden definir su propio universo, que sería un sistema alternativo. Quienes forman parte del sistema alternativo se rigen por las normas del sistema alternativo cuando interactúan entre ellas, y para interactuar con personas que funcionan según el sistema oficial, pueden elegir entre adaptarse al sistema oficial para esas interacciones, o bien usar a la estructura jurídica como intermediaria.

Dicha forma jurídica usa el formato alternativo con las personas de la red, y usa el formato oficial con las personas ajenas a la misma, evitando a las personas de la red tener que estar pasando de un sistema económico al otro continuamente.

Más adelante veremos como la economía alternativa, en base al principio de participación, permite a las personas usuarias actuar a título personal, o a título colectivo, dependiendo de cada momento, o de cada relación jurídica. Pero de momento resulta suficiente con saber que existe un marco de libertad dentro de la estructura legal que concilia la actividad de la red con la legalidad vigente, y de otro, que la red podrá relacionarse con terceras personas sin que ninguna persona usuaria tenga que asumir individualmente la titularidad de esas relaciones frente al sistema oficial, independientemente de lo que suceda en el ámbito interno de la red.


- - -

## 4.2.- Infraestructuras físicas. Las centrales de intercambio.

Además del debate sobre si una red necesita dotarse de unos estatutos y de una forma jurídica que pueda servir como personalidad legalmente válida de la red, toda red de moneda social, a medida que va creciendo, empieza a verse en la necesidad de tener un espacio físico en el que empezar a acumular productos, reunirse, centralizar la gestión de la red, o realizar cualquier actividad de intercambio que necesiten (formación, investigación, arte, baile...).

Para la acumulación de productos, que analizaremos en primer lugar, suele ser más sencillo, al menos al principio, que cada persona guarde y custodie los productos que aporta a la red, o aquellos que produce para aportarlos a la red, en vez de necesitar un espacio colectivo.

Cuando se desea realizar un evento, un mercado o un día de intercambio, cada persona lleva sus productos al mercado de la red, y ese día ponen en práctica su sistema económico. Al acabar el día, cada persona vuelve a llevarse los productos con lo que haya terminado el día, y se encargará de custodiarlos hasta el próximo mercado. Eso es descentralizar el almacenamiento.

Pero llega un momento en el que la red empieza a funcionar de una manera más eficaz, y empiezan a tener lugar las compras colectivas, ya sea a miembros de la red, o a terceras personas, y cuando se realizan esas compras colectivas dentro de la red, la red empezará a necesitar un espacio donde guardar esas compras colectivas hasta que cada persona de la red vaya a recoger su participación en la citada compra.

Podrá haber personas que ofrezcan sus espacios privados para este tipo de necesidades, pero con el tiempo, las compras serán cada vez más grandes, habrá implicadas más personas, y los productos ocuparán un espacio cada vez mayor. Esta es una necesidad colectiva que llega con el crecimiento de la red.

Ello requiere la existencia de un espacio físico que pueda servir de almacén colectivo, y el uso de dicho espacio deberá ser conseguido por la red de alguna manera, ya sea mediante un alquiler, mediante una cesión gratuita, o mediante la adquisición del uso de algún inmueble, ya sea de forma temporal, permanente o periódica. A estas alturas ya debe resultar evidente que hay que intentar, si hay que pagar algún precio por el espacio, que dicho precio pueda ser pagado en moneda social o en intercambio, y solo cuando esto no resulte posible podrá asumirse un pago en moneda oficial, pero debe tenerse en cuenta que los gastos en moneda oficial deben tratar de reducirse al mínimo.

En tanto no se consiga utilizar un espacio físico como central de abastecimiento, o como almacén, o como local, los intercambios de productos serán algo más complejos en la red, ya que cada persona deberá custodiar sus productos, y quien quiera adquirirlos deberá citarse con esa persona para realizar el intercambio en algún lugar.

Cuando existe un espacio de intercambio como una central de abastecimiento, todas las personas pueden dejar allí los productos que están dispuestos a intercambiar y han sido demandados por la red, o aquellos que compran de manera colectiva, haciendo que todas las transacciones de la red puedan hacerse en un mismo lugar, en lugar de hacerlo visitando a todas y cada una de las personas cuyos productos deseamos consumir.

Esta es la función primordial de la infraestructura física que necesita una red de moneda social para expandirse correctamente, la de servir como almacén para los productores y como mercado para los consumidores, pero también desempeña otras funciones que resultan interesantes dentro del funcionamiento de la red.

La segunda función del espacio físico es la de servir como lugar de reunión, ya que para el correcto funcionamiento de la red resulta beneficioso que las personas que la forman se conozcan entre ellas y tengan oportunidades de tener tratos personales entre ellas más allá de los puramente comerciales, ya que la economía es en realidad algo más que el comercio, porque incluye también la manera de producir la riqueza y la manera de utilizar o distribuir la riqueza producida.

Todos esos aspectos de la economía que van más allá de las transacciones económicas entre las personas, deben ser hablados, debatidos, creados, propuestos, y son temas que, en esencia, deben ser objeto de estudio, análisis y diálogo dentro de las redes de moneda social, y un espacio físico en el que organizar este tipo de charlas, debates o asambleas, supone una herramienta que fortalece y consolida la red de moneda social.

El espacio físico también sirve para gestionar la red de una manera colectiva, permitiendo la presencia de muchas personas, y mantener la documentación accesible a las personas que forman la red sin necesidad de que alguna de ellas tenga que desempeñar el rol de contable o de administrador. Esta función incluye la celebración de asambleas, o las reuniones para la toma de decisiones colectivas, o exponer problemas de participación. Si bien, aunque esta función es ventajoso utilizarla como un elemento más para la cohesión de la red y facilitar el contacto personal de unas personas con otras, resulta inevitable que, con el tiempo, esta función vaya cediendo cada vez un lugar mayor a las estructuras telemáticas, ya que dichas estructuras permiten una gestión más horizontal y transparente, quedando las reuniones en espacios físicos reservadas para ocasiones más puntuales en la gestión, o quedando sobre todo como central de abastecimiento y para la celebración de eventos o actividades de reunión, ya sean lúdicas, docentes o de otro tipo.

Una última función del espacio físico es su funcionamiento como grupo de consumo en la vertiente de "alternativa de ocio", entendiendo que si existe este espacio, la red que normalmente tiene que reunirse en la calle termina consumiendo en establecimientos ajenos a la red, pero si se reúne en un espacio de uso propio, pueden realizar las compras de manera colectiva y tener en dicho espacio físico aquellos productos que consideren necesarios para su consumo durante las reuniones, ya sean frutos secos o aperitivos, o bien alimentos cocinados y para cocinar, o bebidas, ya sean refrescos, café, cerveza... o cualquier otro producto que normalmente consumirían en la calle por un precio superior al que pueda costar si se compra de manera colectiva a través de la red, o incluso que puede ser producido dentro de la misma red.

El desarrollo del "ocio alternativo" termina suponiendo una manera de ahorro para las personas de la red, pero también resulta posible su utilización para la generación de riqueza dentro de la red estableciendo unas normas que supongan el reparto de ese ahorro entre las personas y la propia red, o destinarlos a pagar gastos comunes que puedan existir en la red, como podría ser el alquiler del mismo espacio.

De esta manera puede obtenerse financiación de las terceras personas que se acerquen a algún evento o actividad que realice la red, o bien puede generarse un excedente de moneda social que posteriormente puede utilizarse para gratificar a aquellas personas que se implican de una manera cada vez más completa en la red.

Dichos eventos pueden consistir en cualquier tipo de actividad lúdica, artística o formativa, desde talleres de Yoga, hasta presentaciones de libros, pasando por cine fórum, celebración de aniversarios, conciertos, dinámicas, juegos, charlas, debates... Dependiendo únicamente del tipo de espacio físico y de la voluntad y energía de las personas implicadas en la moneda social.


- - -


## 4.3.- Infraestructuras telemáticas. Las redes de intercambio.

Al hablar de la infraestructura física, se ha mencionado la función de gestión, entendida como reuniones en las que las personas de la red tratan de gestionar de manera colectiva el funcionamiento y la situación de la red de moneda social. Sin embargo, ya se dijo que esta función tenderá a ser facilitada y simplificada a través de las infraestructuras telemáticas. De esta manera, si el espacio físico sería similar al cuerpo físico de la red, estas estructuras telemáticas, serían algo parecido a su cerebro, o usando un término más religioso, podrían ser como el espíritu de la red.

Con esta comparación resulta necesario mencionar que lo esencial en las redes no son ni el cuerpo ni el alma, sino precisamente algo distinto de ambas cosas y es lo que realmente hace posibles y viables esas dos cosas. El tercer elemento, pero el más importante de todos, es precisamente la misma red, integrada por las personas que forman parte de ella, y que es realmente el organismo vivo, más allá de su cuerpo o de su alma. Las infraestructuras físicas y las telemáticas no son más que herramientas que posibilitan el crecimiento de la parte física y de la parte organizativa de una realidad previa, que es la red de moneda social.

Las infraestructuras telemáticas pueden utilizarse para muchísimas cosas, ya que las tecnologías tienden a simplificar y facilitar cada vez más aspectos de la sociedad, pero en este manual aludiremos solo a las estructuras telemáticas que consideramos esenciales para el buen funcionamiento de la red de moneda social, independientemente de que cada red pueda incorporar más infraestructuras telemáticas, o sea capaz de sobrevivir sin utilizar alguna de ellas. Las estructuras podrán clasificarse en "decisorias" y "contables".

- - -

### 4.3.1.- Estructuras decisorias: Información, Debate y Decisión.
Estas serán las estructuras destinadas a gestionar la toma de decisiones, por lo que su finalidad será la de facilitar en la práctica los procedimientos mencionados anteriormente como "democracia" y "participación". Su papel consistirá en reducir los inconvenientes que puedan existir para la celebración de asambleas con el objetivo de tomar decisiones, y mantener a todas las personas que integran la red debidamente informadas de la situación y funcionamiento del sistema económico. Como estructuras decisorias citaremos 3 herramientas telemáticas que las redes de moneda social podrán utilizar para facilitar estos procedimientos de información, debate y toma de decisiones.

 - Para la información, puede utilizarse tanto una página web, como un blog, o incluso un grupo formado dentro de una red social, o incluso una lista de correos. pero debe tenerse en cuenta que no todas esas herramientas son igualmente manejables ni eficaces para según qué cosas. Así, por ejemplo, una información enviada por una lista de correos de la red puede resultar más inmediata que una publicación con la misma información realizada en el blog de la red, por la sencilla razón de que el correo electrónico se mira con mayor frecuencia que un blog. Sin embargo, para almacenar información de manera que resulte localizable en el futuro, es mucho mejor publicarla en un blog de la red, que tener que buscar entre los mails. Una web, por ejemplo, podría hacer mucho más fácil la búsqueda de noticias o de información concreta de lo que puede permitir un blog.

 Pero independientemente de la herramienta que decida utilizar cada red, o incluso aunque decida utilizarlas todas, y elegir cuál usar para cada cosa o en cada momento, resulta evidente que internet puede ayudar a mantener informada a la gente que por cualquier motivo no pueda asistir presencialmente y con frecuencia a las reuniones que celebre la red. Mientras no se empiezan a usar estas herramientas, la información deberá hacerse presencialmente en las reuniones y asambleas, o bien mediante folletos y octavillas, lo que puede incrementar los gastos de impresión y reducir el alcance de las noticias, aunque también puede tener sus ventajas respecto al trato personal y directo, como respecto de la financiación si existe en funcionamiento la idea del "ocio alternativo" como fuente de ingresos o creación de riqueza.

- Para los debates pueden usarse herramientas que sean puramente escritas, como pueden ser los foros virtuales, donde quedan registradas las propuestas y opiniones de cada persona que interviene en un debate, y se permite que cada persona opine cuando le resulte posible acceder al foro, sin tener que someterse a un horario estricto para la asamblea. Hay otras herramientas no escritas, como son los chats de voz, donde se permite la celebración de reuniones y asambleas on line y en tiempo real, permitiendo a la gente citarse en alguna de dichas plataformas virtuales y celebrar un debate, o una reunión de trabajo, sin necesidad de desplazarse hasta un espacio físico concreto, bastando con desplazarse hasta un ordenador con acceso a internet.

- Para la toma de actas, existen herramientas telemáticas que funcionan como documentos colaborativos, y que permiten que, mientras se celebra un debate a través de un chat de voz, haya varias personas que editen un documento colaborativo entre todas ellas. De esta manera, pueden ir copiando o resumiendo las cuestiones que se traten, las propuestas que se hagan, o las decisiones que se tomen, con la ventaja de que dicho "acta" puede ser revisado y corregido en tiempo real por otras personas. De esta manera se toman actas con mucha mejor calidad en cuanto a los contenidos, y que al tomarse directamente en el pc, no necesitan ser digitalizadas con posterioridad para su publicación en las herramientas de información. También hay chats de voz que permiten la grabación de las conversaciones, pero la práctica indica que suelen usarse más las actas que las grabaciones de voz, por su facilidad a la hora de examinar los contenidos.

- Para la toma de decisiones puede utilizarse cualquiera de las herramientas anteriores, ya que una votación puede hacerse tanto en un foro, como en una red social, como en un chat de voz. Pero existen herramientas creadas expresamente para la toma de decisiones, donde resulta posible controlar de manera más eficaz los principios de "una persona, un voto", en los que puede garantizarse el anonimato en las votaciones, o pueden mezclarse funciones de debate, de escalabilidad de las propuestas según su aceptación, y de votación. Se menciona esta herramienta por separado, para dejar claro que aunque una herramienta determinada tenga dos o más de estas funcionalidades, debe tenerse en cuenta que deben usarse ambas, ya que de nada servirá una red que utilice los foros para debatir y, aunque el foro permita votar, no realice nunca ningún proceso de toma de decisiones.

Tener una herramienta no significa utilizarla. Y utilizar una herramienta no significa utilizarla correctamente, ni sacarle todo el partido posible.

- - -

### 4.3.2.- Estructuras contables: Participación y Transparencia.
Este tipo de herramientas tienen por objetivo suprimir o reducir al mínimo la necesidad de personas que deban ocuparse de llevar la contabilidad del grupo, permitiendo que la contabilidad sea llevada por todas las personas implicadas en la red. Para ello deben tener un canal o método utilizable por cualquier persona de la red que desee anotar en la contabilidad una operación económica en la que se haya visto implicada, ya sea al hacerla con otra persona de la red, o con una persona ajena a la red, cuando dicha operación pueda afectar a la red.

El primer elemento del que debe constar esta estructura contable es una base de datos donde consten los nombres de todas las personas que funcionan dentro de la red, para garantizar que toda operación contable que se registra es realizada por una persona de la red, ya sea dicha persona una persona individual, ya sea una persona colectiva o ya sea algún proyecto autogestionado dentro de la red.

El segundo elemento es el canal para que cada persona pueda informar sobre las operaciones que realiza y que afecten a la red, siendo lo más simple un formulario virtual a rellenar por dicha persona en el que deban hacerse constar los datos necesarios para llevar la contabilidad (datos de la persona con la que se hace la transacción, concepto de la misma, el precio, los impuestos que le hayan sido aplicados, etc).

El tercer elemento es la plataforma en la que se registra y publica toda la contabilidad de la red, y que deba permitir que cualquier persona usuaria acceda a la información contable de manera directa, sin tener que solicitar permiso a nadie, ni concertar una cita previa. De esta manera, cualquier irregularidad o anomalía en la contabilidad podrá ser detectada por cualquier persona, y todas las personas podrán descargar copias de seguridad de la contabilidad y funcionar como testigos de todas las transacciones.

Por último, deberá disponer de un mecanismo para avisar de las incidencias o anomalías que puedan detectarse en la contabilidad, como podrían ser un cargo o un ingreso injustificado en alguna cuenta, o algún error en el funcionamiento de esta estructura contable.

Esta herramienta deberá estar muy relacionada con la estructura de toma de decisiones, ya que será la red la que decida qué ámbito económico considera parte de la "intimidad" y protegida por el derecho a la privacidad, eximiendo del deber de reflejar determinados contenidos en determinadas transacciones.

Por último, existe un concepto contable que puede resultar de interés a estas contabilidades horizontales y participativas, como es el "saldo liberado". Dicho saldo consiste en una cierta cantidad de riqueza generada por alguna persona para la red, y que por norma general sería gestionada por la misma red. Sin embargo, los saldos liberados, son saldos positivos que tienen las personas pero de los que no pueden disponer para su uso personal, sino que solo pueden ser utilizados dentro de los proyectos o propuestas económicas que surjan dentro de la red y que no puedan ser financiadas por la asamblea. De esta manera, se dota de un mayor grado de participación en la toma de decisiones relativas a la financiación, aunque solo respecto de aquellas aportaciones que cada persona haya realizado personalmente.

Esto lleva a distinguir el "saldo normal" de una persona, del que sería su "saldo liberado", siendo el primero el que tiene para disponer de él libremente, y siendo el segundo un saldo que tiene pero del que únicamente puede disponer para financiar intereses generales de la red.

Estas estructuras contables deberán estar preparadas para llevar la contabilidad en moneda social, pero también deben estar preparadas para llevar la contabilidad de cara al sistema económico oficial, ya que una de sus funciones será precisamente la de traducir la economía alternativa de la red a la economía del sistema oficial para permitir a la red interactuar con dicho sistema cuando resulte necesario.

Mientras una moneda social no llega a la fase en que precisa de una forma jurídica tampoco precisará de este sistema contable de traducción, pero una vez que se crea una forma jurídica para interactuar con el sistema, es necesario que dicha forma jurídica sea capaz de utilizar el lenguaje económico del sistema para enfrentarse a él.

- - -

## 4.4.- Difusión y visibilidad.

Para la correcta expansión de la moneda pueden utilizarse varias estrategias, atendiendo a la necesidad de crecer de manera rápida, o de crecer de manera sólida, siendo recomendable crecer de ambas maneras, pero si hay que elegir, siempre será mejor optar por la solidez en el crecimiento, aunque sea a costa de tardar más tiempo en expandirse. Para ello hay que tener en cuenta algunos factores:

- - -

### 4.4.1.- Canales de difusión
La red se dará a conocer a personas ajenas a la red para ofrecerles la oportunidad de conocerla, entender cómo funciona e incorporarse si lo desean, para lo que pueden utilizarse herramientas como el boca-oreja, consistente en que las personas de la red hablen de ella a otras personas, o pueden utilizarse otras herramientas más comerciales, como pueden ser anuncios publicitarios, cartelería, espacios de radio o televisión, blogs, apariciones en prensa o la organización de eventos puntuales, siendo todos ellos canales que cada red podrá utilizar atendiendo a sus posibilidad, deseos y estrategias. Para el uso de estas herramientas de expansión puede resultar muy útil tener elaborados unos guiones con la información que se quiere facilitar a la gente sobre el funcionamiento, las utilidades o las ventajas que supone la utilización de una red de moneda social.

- - -

### 4.4.2.- La visibilidad
Este factor, aunque está muy relacionado con la difusión, se centra mucho más en la parte del boca-oreja y la experimentación real por parte de las personas que integran la red, de manera que las personas ajenas a la red puedan percibir la red de moneda social como una realidad que existe y que funciona. Es por ello que dentro de este factor desempeñan un papel crucial los eventos, ya que en los eventos es donde más sencillo resulta acercarse a la gente ajena a la red y permitirles ver cómo funciona la red, como se realizan las transacciones, y conocer tanto los bienes y servicios existentes en la red, como conocer a las personas que constituyen realmente la riqueza de la red.

Es también en estos eventos cuando suelen utilizarse la monedas sociales en formato papel para que las personas ajenas a la red puedan utilizar la moneda social durante el tiempo que dura el evento, lo que permite a la gente experimentar con la moneda social y hacer sus primeras transacciones en moneda social. Y además ayuda al proceso de descapitalización.

Pero a parte de los eventos, hay un factor muy importante en la visibilidad de la moneda social, que consiste en crear la necesidad de moneda social como la visibilización de una necesidad urgente frente al sistema económico oficial.

Para ello, resulta necesario corregir un error clásico en la expansión de las monedas sociales, como es el hecho de pensar que, para expandir la moneda, las personas que la integran deben convencer a la gente para que acepte moneda social, cuando la visibilización de la moneda consiste precisamente en lo contrario, es decir, las personas de la red lo que deben hacer es pedir moneda social a cambio de sus bienes y servicios, para que la gente entienda que necesita monedas sociales si quiere acceder a esos bienes o servicios.

De esa manera, a medida que la gente va necesitando moneda social, empezará a querer conseguir moneda social, y decidirá aceptar moneda social a cambio de sus bienes y servicios para poder pagar en moneda social a quien se lo pida. De esta corrección en la visibilidad semana la máxima de "una red es más sólida cuanto más intensas son las personas que la componen", entendiendo por intensas a aquellas personas que tratan de cobrar siempre en moneda social, incluso a quienes no pertenecen a ninguna red de moneda social, ya que esa petición real en la vida cotidiana es la mejor manera de hacer visible la existencia de la moneda social y de despertar curiosidad en aquellas personas a las que se les pide que paguen en moneda social sin saber lo que es.

- - -

### 4.4.3.- La capacidad de absorción
Otro factor a tener cuenta es la capacidad que tiene una moneda social de aceptar nuevas incorporaciones sin afectar gravemente al funcionamiento de la red, lo que será su "capacidad de absorción", ya que cada incorporación supone el incremento de las necesidad a satisfacer, el incremento de los recursos a gestionar, y el incremento de las posibles monedas en circulación.

Una avalancha de gente que se incorpore a la red en un corto espacio de tiempo puede dar lugar a determinados problemas, como podrían ser la falta de determinados bienes o servicios que antes eran producidos para toda la red, y que de repente empiezan a ser producidos de manera insuficiente debido a que se produce la misma cantidad por la misma gente para un número mayor de personas. Para evitar este problema resulta necesario tener en cuenta las necesidades reales de la red, su capacidad para satisfacerlas, y tomar decisiones respecto a qué tipo de productores o prestadores de servicios sería beneficioso incorporar a la red, así como a qué tipo de consumidores se está en condiciones de poder satisfacer las necesidades, y hacer ambas cosas sin alterar negativamente el funcionamiento de la red.

Otro problema podría ser la insuficiencia de espacio en las centrales de abastecimiento para acumular los productos producidos dentro de la red, ya que al incorporarse un número considerable de productores en poco tiempo, es posible que de repente crezca mucho la producción de algunos recursos o la oferta de determinados servicios, sin que crezca a un ritmo similar el consumo de ese tipo de recursos o servicios, y sin poder ampliar el tamaño de la central de abastecimiento, creándose una situación de escasez en los recursos necesarios y una abundancia de excedentes que no pueden ser consumidos dentro de la red.

La capacidad de absorción de la red consiste precisamente en la capacidad que tiene la red para regular el crecimiento de las necesidades y de los productos para que la red siga funcionando de una manera eficiente.

- - -

### 4.5.- El crecimiento de la red

Si la red desarrolla adecuadamente los factores de difusión, visibilidad y absorción, podrá crecer a medida que vayan incorporándose nuevos productores y consumidores a la red de moneda social, haciendo sostenible la producción interna con el consumo interno, manteniendo un equilibrio relativo entre ambas caras de esta misma moneda que es la economía.

Del mismo modo, cada vez serán más los bienes y servicios que se pueden ofertar y obtener dentro de la red, ya que a medida que crece el número de consumidores, crecerá la demanda de bienes y servicios, y a la vez que se incrementan los productores crecerá también la variedad y la cantidad de los bienes y servicios disponibles.

Pero del mismo modo que en un principio existían unos excedentes que se utilizaban para celebrar los mercados de moneda social, los eventos, los trueques y transacciones con otras redes, a medida que crezca la red también crecerá el número de excedentes, y cada vez serán más los bienes y servicios disponibles que no resultan necesarios dentro de la red.

Como a través de los eventos y del crecimiento de la red se habrán incorporado a la red las personas más cercanas a ella, cada vez resultará más difícil encontrar a personas ajenas que quieran incorporarse a la red, ya que llegará un momento en el que quienes no se hayan incorporado no tengan pensado hacerlo a pesar de conocer la moneda social, bien porque tienen una economía oficial que les permite satisfacer todas sus necesidades, o bien porque simplemente no confíen en la moneda social, o hayan decidido libremente no incorporarse a ninguna red de moneda social porque se consideran autosuficientes y no la vean necesaria.

Cuando la red ya no puede ofrecer a quienes están fuera algo que necesiten, habrá alcanzado su límite de expansión, y le resultará muy difícil incorporar nuevas personas, por lo que tendrá que asumir que ha llegado el momento de dejar de crecer, o deberá reestructurarse internamente para producir nuevos bienes y servicios, o mejorar la calidad o la cantidad de los existentes.

Este será el límite máximo de la capacidad productiva de la red en esta fase de crecimiento. Posteriormente veremos como la red podrá seguir creciendo, pero para ello, deberá primero rebasar la barrera del euro que le permita interactuar con aquellas personas que no quieren incorporarse a la red de moneda social.

Mientras ha sucedido todo este proceso de creación y expansión, las personas de la red han seguido soportando determinados gastos de consumo en moneda oficial, como pueden ser el agua, la luz, el teléfono o los impuestos, por lo que llegado el momento, habrá que pensar en la manera de poder satisfacer estas necesidades dentro de la red, incluso antes de poder generar estos recursos dentro de la misma red.

- - -

### 4.6.- La barrera del euro

A parte del agua, el gas, el teléfono, la gasolina o la electricidad, existen algunos gastos no tan evidentes, pero que también son necesarios y tal vez no hayan podido ser ofrecidos por la red, como podrían ser un cepillo de dientes, una toalla, tornillos o herramientasde cualquier tipo.

Cuando una necesidad no puede ser satisfecha por la red de moneda social, resulta necesario salir fuera a buscar los recursos necesarios, y si bien puede resultar posible encontrar el bien o servicio en otra red de moneda social, también es posible que la única manera de satisfacer esa necesidad sea adquiriendo el bien o servicio dentro de la economía oficial, y para ello resulta necesario disponer de euros.

Esas necesidades que precisan de euros para ser satisfechas, es lo que denominamos "la barrera del euro", y es como el gran muro que toda moneda social debe tratar de derribar para poder considerarse como un sistema económico completo. Y si bien ya se ha dejado claro que lo ideal es poder producirlo directamente, o si no es posible, lo ideal sería intercambiarlo por algún bien o servicio de la red, llega un momento en el crecimiento de la moneda donde la barrera del euro se levanta ante las personas de la red de manera que parece indestructible.

En la parte final de este manual veremos cómo afrontar la barrera del euro desde la red de moneda social, pero antes resulta más acorde a nuestro método expositivo, detenernos un momento en la posibilidad que hemos mencionado: Conseguir los bienes y servicios de otras redes de moneda social, para no tener que acudir inmediatamente al euro.

* * *

# 5.- RELACIONES DE LAS SOCIALES ENTRE ELLAS DIFERENTES MONEDAS

## 5.1.- El factor geográfico

A la hora de que una moneda social interactúe con otra, serán varios los factores que afecten a la viabilidad de dicha relación, siendo el más inmediato y físico el factor geográfico, ya que dos monedas sociales que no se encuentren cerca geográficamente tendrán muy difícil interactuar entre ellas, y ello por la sencilla razón de que hay unos costes de desplazamiento que deben ser asumidos como un coste adicional de la relación, y la tendencia de las redes a evitar los costes económicos y el efecto contaminante de los transportes, hará que cada red trate de interactuar preferentemente con redes que se encuentren cerca geográficamente.

Pero a esta limitación conviene hacerle una justificación, como es, además del ahorro económico de no tener que realizar un transporte largo, el respeto al medio ambiente, que en determinados casos puede no ser un valor esencial de la red, pero que debería serlo, al ser el planeta un recurso finito que todas las redes económicas deberían cuidar como patrimonio común, aunque lo hagan pensando en sí mismos y en la duración a largo plazo de su sistema económico.

Podrán darse casos en que resulte inevitable asumir una relación económica con una red alejada, porque tal vez sea la única red que dispone del recurso que se necesita, o tal vez sea porque es la única interesada en adquirir un excedente del que nuestra red no puede deshacerse. En dichos casos, habrá que ver la manera en que se asumen los gastos del transporte o el desplazamiento, y tratar de aprovechar al máximo los trayectos para minimizar el impacto ambiental que dicho trayecto pueda suponer al planeta, así como para, de paso, minimizar o compartir los gastos.

Para ello es bueno ver qué otros productos de dicha red pueden resultar necesarios, o ver si dicha red necesita algún producto de la nuestra, para que el medio transporte utilizado sea aprovechado al máximo en cada viaje de ida o de vuelta. O incluso aprovechar si alguna persona de la red va a desplazarse a esa zona para que lleve o traiga los productos, o bien para que aproveche el viaje que se vaya a realizar con los productos.

Además, esta cuestión pone de relieve la necesidad de plantear dentro de las redes de moneda social el asunto de la soberanía energética, ya que para realizar dichos transportes habrá que buscar algún tipo de combustible que pueda ser producido dentro de la propia red, o bien habrá que asumir un coste en euros para realizar estos desplazamientos, debiendo tener claro que la red aspira a no depender del euro más de lo que resulte imprescindible.

Es por todo ello que, a partir de determinada distancia geográfica, puede resultar más conveniente crear una nueva red de moneda social que seguir ampliando la red existente, ya que si una red crece demasiado geográficamente, puede verse algún día en la necesidad de realizar largos trayectos y desplazamientos incluso para realizar operaciones económicas internas, y eso convertiría en habitual este problema de los desplazamientos.

Otra cuestión a señalar dentro del factor geográfico, es la irrelevancia de este factor respecto a determinados servicios o productos que puedan ser realizados por vía telemática, como puede ser un servicio de asesoramiento, el desarrollo de alguna aplicación web, un servicio de alojamiento web, o el intercambio de productos que puedan existir en formatos digitales, como libros, manuales, música o documentales.

Este tipo de trabajo intelectual debe ser generado y compartido de manera global por el conjunto de todas las redes económicas, ya que para este tipo de relaciones económicas no existe el problema del transporte físico, siendo bastante con tener canales telemáticos de comunicación.

- - -

## 5.2.- Los intereses comunes

Otro factor que resulta relevante a la hora de que dos redes de moneda social interactúen, suele ser la afinidad o interés común por un mismo asunto, como suele suceder con el tema de los productos ecológicos, ya que es posible que una red interesada en consumir productos ecológicos no pueda producir los que necesita, y que las redes que produzcan esos productos no sean precisamente las redes más cercanas, viéndose en la necesidad de renunciar a dichos productos, o de asumir la necesidad de importarlos desde una red alejada.

Las redes que incluyan la ecología como interés común tenderán a relacionarse con mayor facilidad entre ellas, que con otra red que no se preocupe por la ecología de los productos, por lo que será conveniente tener identificadas a las redes económicas que se basan en valores y principios similares a los de nuestra red.

Del mismo modo que un valor puede ser la ecología, hay otras redes que establecen otros intereses o preferencias a la hora de consumir, como pueden ser "consumir productos 100% en moneda social", "consumir productos de producción local", o "consumir productos artesanos".

Hay incluso redes que se niegan a consumir productos determinados, lo que nos llevará a hablar de incompatibilidad o de enemistad, en oposición a la afinidad, aunque desde este manual se aconseja fortalecer los vínculos de afinidad, y mecanismos para el buen funcionamiento de la red, dejando al margen las relaciones de enemistad, ya que con el tiempo pueden aparecer intereses comunes con esas redes incompatibles que serán difícil de llevar a cabo si existen enemistades previas. Pero como siempre, esas son cosas que deberá decidir cada red de manera independiente.

Con aquellas redes con las que, por afinidad, se pretenda entablar relaciones económicas estables, se podrá estudiar la manera de colectivizar recursos, por ejemplo vehículos, para disponer de una infraestructura logística común que permita el transporte de mercancías o personas, o incluso colectivizar únicamente el uso de los vehículos particulares de las personas que integren ambas redes, sin necesidad de colectivizar la propiedad.

También existen determinados servicios que pueden resultar más beneficiosos si se consumen de manera colaborativa, como puede suceder con las líneas de teléfono, ya que al contratar un mayor número de líneas, suele existir un descuento razonable, por lo que podría resultar interesante la contratación colectiva de muchas líneas, tanto para una red, como para un grupo de redes que estén dispuestas a trabajar juntas en el servicio de telecomunicaciones y sean capaces de confluir a la hora de decidir con qué compañía contratan estos servicios.

Lo mismo sucede respecto a la instalación de antenas, aunque en este aspecto, sucede algo más descentralizado, ya que cada red se encargará de poner las antenas en su zona, pero resultaría interesante que todas las redes afines sean capaces de optar por un mismo sistema de telecomunicaciones, para que las antenas que ponga una red, puedan ser utilizadas por aquellas redes que colocan antenas compatibles, con la finalidad de tener una infraestructura propia para las telecomunicaciones generada por, y a disposición de, estas redes económicas. Posteriormente, esto permitirá la contratación colectiva de servicios y compartir los gastos de dicha contratación, al resultar posible compartir el servicio contratado a través de antenas propias.

Y quien sabe si algún día permitirán incluso ofrecer un servicio independiente de este tipo.

Respecto al arte, la cultura y el ocio, también existen relaciones entre la diferentes redes de moneda social que resultarán mucho más provechosas si se coordinan y se realizan de manera colectiva entre distintas redes, ya que los artistas que generan los contenidos culturales tendrán más oportunidades de difusión y de ser conocidos, a la vez que cada red conocerá la oferta cultural de las demás redes, lo que reforzará las relaciones entre las redes que incluyan una oferta cultural, al margen de aquellas redes que se centren únicamente en la adquisición de productos alimenticios.

También puede haber algún interés que, por no ser común, tenga el efecto contrario al mencionado, ya que por ejemplo, una moneda social complementaria que tenga su base de confianza exclusivamente en la moneda oficial, podrá ser fácilmente descartada por aquellas redes que han decidido basar su economía en la confianza, por considerar que aquellas monedas sociales orientadas únicamente a fidelizar una clientela no están realmente contribuyendo a la creación de una economía alternativa. Aunque sea posible interactuar con dicha red como quien interactúa con una empresa ajena a las monedas sociales.

Por el contrario, se reforzarán las relaciones entre aquellas monedas que comparten su visión de la economía alternativa, y las redes podrán preferir operar con aquellas que usan la red desde una perspectiva amplia de la economía, en lugar de comerciar con aquellas redes creadas por empresas con fines puramente comerciales o de marketing para incrementar sus ventas y su beneficio empresarial.

Lo mismo sucederá cuando una red tenga entre sus principios el respeto a los derechos de los animales y decidan tener una línea de funcionamiento acorde al veganismo, por ejemplo, ya que habrá redes que por ser similares resulten afines a ella, y otras que, por tener productos animales, produzcan algún rechazo dentro de la red.

En este sentido conviene tener claro que siempre será una ventaja saber distinguir entre productores concretos de una red y la propia red, ya que dos redes pueden resultar compatibles para determinadas personas de cada red, mientras que resulten incompatibles para otras personas de las mismas redes. En esos casos serán las redes las que decidan si dichas relaciones deben tomarse desde la esfera de la participación, permitiendo que cada usuario intercambie con quien quiera y se abstenga de hacerlo con quien no quiera, o si se hace desde la esfera de la democracia, haciendo que sea la red quien adopte una decisión colectiva que deberá ser respetada por todas las personas de la red.

Desde este manual se recomienda usar la esfera de la participación siempre que resulte posible, para evitar la posible aparición de élitesy decisiones impuestas, pero no debe olvidarse la responsabilidad heredada de la red, y la posibilidad de cada red para autorregularse en estas materias.

Las relaciones entre las diferentes monedas serán las que permitan conocerse unas a otras, para ver si tienen intereses comunes y si están dispuestas a afrontarlos de manera colectiva, o identificar aquellas monedas con las que, por tener intereses diferentes a los de nuestra red, preferimos no tener relaciones económicas, o reducirlas al mínimo imprescindible, tal y como hacemos con la moneda oficial.

- - -

## 5.3.- Compatibilidad de formatos

Pero independientemente de que dos redes de moneda social estén dispuestas a realizar transacciones económicas, bien porque están próximas, bien porque son afines, o bien porque comparten un interés común, resultará necesario para que puedan realizar intercambios entre ellas que utilicen formatos de contabilidad compatibles.

Dentro de los formatos, ya se mencionó la necesidad de tener una contabilidad horizontal centralizada en una aplicación informática que fuese utilizada por todas las personas de una red, por lo que, para que dos redes puedan asumir el papel de persona-red que intercambia con otra persona-red, ambas deberán tener algún sistema contable común que permita contabilizar las entregas realizadas por cada una de dichas redes a la otra.

El sistema CES (Community Exchange Sistem) ofrece esta posibilidad a todas las redes que usan dicho sistema para su contabilidad interna, dando lugar a lo que podríamos denominar una contabilidad federada de las monedas sociales que utilizan este sistema.

También cabe la posibilidad de desarrollar nuevas aplicaciones telemáticas que ofrezcan esta posibilidad, debiendo tenerse en cuenta que las redes de moneda social tipo LETS necesitan que los titulares del saldo negativo y el positivo que se generan con cada intercambio deben quedar reflejados dentro de la misma aplicación, para evitar los desajustes o desequilibrios generales.

Existen maneras alternativas de solucionar este problema, del mismo modo que se pueden solucionar los intercambios a la hora de utilizar la moneda oficial, ya que ésta no suele estar incorporada a estos sistemas contables, pero dicha explicación se dará en la parte relativa a la descapitalización y a las relaciones de la moneda social con el euro, siendo posible aplicar esas mismas soluciones a otras monedas sociales que tengan formatos incompatibles, pero teniendo en cuenta que es posible que otra moneda social no tenga la misma funcionalidad que nuestra red, y que ambas redes no tienen por qué ser afines si tienen sistemas contables diferentes, lo que puede hacer más complicada la implantación de dicha alternativa.

De cualquier manera, las monedas que intentan intercambiar sin tener unos formatos contables que resulten compatibles, siempre podrán acudir a soluciones más básicas, como serían el intercambio directo, o el uso de una moneda ajena ambas monedas, o incluso podrán acudir a una solución más avanzada, como podría ser una economía de gratuidad, o que una de ambas redes se incorporase a la otra como una persona más de la red anfitriona.

- - -


## 5.4.- El valor de conversión

Pero al realizar esos intercambios entre diferentes monedas sociales, aparece nuevamente el problema del valor de conversión, ya que es posible que una de las dos monedas no esté vinculada al euro porque haya evolucionado lo suficiente como para no necesitar moneda oficial, y la otra sí, por lo que una de ella sufrirá devaluaciones dependiendo del valor del euro, mientras que la otra funcionará de 139manera autónoma, lo que podría dar lugar a un nuevo mercado de divisas dentro de las economías alternativas, abriendo la puerta a la especulación con los valores de las monedas sociales.

El mismo problema surgiría si una de las redes ya está consolidada y tiene una gran cantidad de productos y servicios a disposición de las personas, y la otra puede que no haya conseguido consolidarse y no ofrezca nada que pueda interesar a la primera red, obligándola a interactuar con otras redes sin saber si necesita algo de ellas, ya que ello afectará de una manera directa a la utilidad de las monedas de una otra y otra red frente a las personas que las utilizan.

Esto podría desembocar en que las redes traten de atribuir más valor a las monedas de una red que a las monedas de otra red menos consolidada. Esto lo harían estableciendo un ratio de conversión distinto de 1 a 1 (una moneda de la primera red equivale a una moneda de la segunda red).

A esto debe añadirse el problema de las afinidades, ya que es posible que una moneda acepte intercambios 1 a 1 con algunas monedas que le resultan afines, y sin embargo no acepte intercambiar con otras monedas determinadas, o los acepta con un ratio diferente del 1 a 1, y que a su vez, alguna de esas monedas afines acepte los intercambios con una red con las que dicha red afín no desea intercambiar. Esto podría crear situaciones en las que interesaría acudir a unas monedas que intercambien 1 a 1 en la red con la que queremos intercambiar, mientras que con nuestra red intercambiarían con un ratio de conversión de 2 a 1, o se nos negaría la posibilidad de intercambiar.

Es por ello que lo recomendable es que todas las monedas sociales se conviertan 1 a 1 entre ellas, y que los mecanismos de control para las diferentes realidades de cada red se establezcan directamente a través de los precios, ya que los precios pueden ser negociados para cada intercambio de manera individual con cada persona, o con cada red.

Estos problemas son problemas generales que afectan directamente a toda la red, ya que una moneda social responderá de las monedas emitidas por cualquiera de las personas de su red, pero a la hora de establecer los límites máximo de saldo negativo puede suceder que dos monedas afines no tengan el mismo límite, lo que exige un esfuerzo adicional a la hora de intercambiar con personas de otras redes, como sería conocer los límites máximos de saldo negativo en dichas redes, a fin de que una persona que ha sido limitada en el uso de la moneda social por su propia red, no aproveche las redes afines para sobrepasar su límite máximo de saldo negativo.

En estos casos debe tenerse en cuenta que la otra red adquiere el crédito contra toda la red de esta persona que supera su limite máximo, por lo que sería aconsejable que los sistemas contables federados reflejasen ese tipo de información a la hora de registrar intercambios entre personas de distintas redes, ya que la gente conocerá las normas de su propia red, pero sería conveniente que conociese también las normas de las redes afines. En última instancia, sería ideal que esas redes afines llegaran a consensuar unas normas comunes aplicables a todas ellas.

En última instancia, siempre deberá tenerse en cuenta el elemento de la confianza en cada persona al realizar intercambios con ella, ya que como se explicó al hablar de la responsabilidad heredada, no tiene sentido que se confíe en nombre de la red en personas de las que personalmente no nos fiamos. Y para eso está la posibilidad de buscar el intercambio directo con las personas de dudosa confianza (ya que el intercambio directo permite comprobar la disponibilidad y predisposición de la gente), o la posibilidad de consultar sus historiales de intercambio, su saldo, sus ofertas y su actividad dentro de su propia red de moneda social.

- - -

## 5.5.- La federación de monedas sociales

Esas relaciones entre diferentes redes de moneda social podrá dar lugar a federaciones de redes en las que un conjunto de redes de monedas sociales deciden aglutinarse para gestionar de manera más eficiente esos intereses y necesidades comunes.

Desde que se utiliza una herramienta contable común, como sería el caso del CES, ya podemos hablar de contabilidades federadas, pero una Federación de Monedas Sociales exigiría algo más que una contabilidad compatible, ya que en el seno de esta federación habría que acordar unos parámetros similares a los que cada red acordó para todas las personas de su red (un límite máximo de saldo negativo, unos procedimientos para la toma de decisiones, una definición de las materias que son afrontadas desde la participación y las que son afrontadas desde la democracia, los gastos comunes que se quieren soportar, la gestión interna, la transparencia...).

Y además de darse esas normas para aplicarlas a las redes, también se verán en la necesidad de consensuar unas normas homogéneas para todas las redes que decidan federarse y que simplifiquen el número de normas que debe conocer una persona de una red para poder utilizar de manera eficiente la federación de monedas sociales sin poner enpeligro a ninguna de las redes.

Llegado este momento, las redes se comportarán como personas individuales en la fase de consolidación de la moneda, decidiendo si necesitan infraestructuras físicas comunes o les basta con las de cada red, si necesitan infraestructuras telemáticas, y si les interesa o no crear una personalidad jurídica colectiva para dicha federación.

Una vez creada la federación, las redes podrán conservar sus dinámicas locales a nivel interno, del mismo modo que las personas que crearon una red de moneda social conservaban sus economías personales al margen de la red, pero habrán creado un nuevo marco en el que poder realizar determinados intercambios y transacciones, y este nuevo marco federado será más extenso que el que creaban las redes locales de manera autónoma, porque la federación se creará precisamente cuando se pretendan romper las barreras territoriales y espaciales entre las diferentes redes de moneda social que hayan logrado consolidarse.

* * *

# 6.- COEXISTENCIA DE LAS MONEDAS SOCIALES CON EL DINERO OFICIAL

## 6.1.- Encuentro entre la moneda social y la moneda oficial

Aún existiendo redes afines, o incluso una federación de monedas sociales, resultará bastante complicado que una red consiga acceder a todos los recursos necesarios para satisfacer íntegramente todas sus necesidades de consumo al margen de la moneda oficial, por la sencilla razón de que hay muchísima gente que sigue produciendo y trabajado dentro del sistema oficial, y en parte también debido a que el sistema ha monopolizado determinados servicios que resultan imprescindibles para la mayoría, como sucede con la luz, el agua, el gas o la gasolina.

Dichos suministros deberán contratarse en moneda oficial en la mayoría de los casos, sobre todo cuando la red se forma en la ciudad, ya que en los núcleos urbanos se dispone de menos margen de maniobra económica, por estar el espacio mucho más repartido, y existir una filosofía imperante de especialización, industrialización y capitalismo bastante aceptada.

Eso hace que dichas redes no dispongan de todo el espacio necesario para generar su propia electricidad, ni su propio combustible, e incluso carecerán del espacio necesario para producir sus propios alimentos, al haber menos espacio para el cultivo y la agricultura.

Esta parte de la realidad, que funciona en euros por mucho que disguste a las monedas sociales, debe resultar accesible a las personas que integran la red, y debe tenerse claro que los monopolios estatales serán muy reticentes a admitir pagos en moneda social, y no estarán dispuestos a realizar intercambios fuera de su sistema oficial, por lo que habrá que buscar la manera de que la red consiga los euros necesarios para poder ofrecer a sus integrantes dichos servicios en moneda social, asumiendo que ya ha intentado producirlos por sí misma y no le ha resultado posible, de momento.

El objetivo de esta maniobra económica es permitir a las personas que usan las monedas sociales el acceso a los bienes y servicios que solo resulta posible adquirir en euros y resultan necesarios. Como en un principio resultará difícil funcionar en moneda social en el 100% de la vida económica, habrá de tenerse claro que dichos bienes y servicios deberán liberarse de forma gradual, a medida que vaya ampliándose la red de moneda social.

- - -

## 6.2.- La economía 100% en moneda social

Hay redes de moneda social o personas que forman parte de ellas que tienden a pensar que "si esos servicios solo pueden adquirirse en euros, las personas necesitarán euros para poder adquirirlos", y serán dichas redes o personas quienes traten de asumir o aceptar la existencia de moneda oficial en los intercambios que tienen lugar dentro de la red.

Quienes ven la situación así no suelen tardar en renunciar a los intercambios 100% en moneda social para volver progresivamente al concepto de compra-venta en moneda oficial, ya que al entender que si la red solo satisface un 30% de las necesidades, las personas que la integran solo podrán manejar un 30% de su economía en moneda social, tolerando de esa manera la realización de operaciones en euros dentro de la red hasta un máximo del 70%.

Eso da lugar a que la red deje de funcionar 100% en moneda social, para tener un funcionamiento mixto entre moneda social y euros, lo que devuelve a las personas la necesidad de conseguir euros incluso para consumir dentro de la red de moneda social. Y a medida que dentro de la red empiezan a solicitarse euros a cambio de los productos o servicios, la red de moneda social empieza a desmoronarse, hasta que finalmente se reduce a una red de fidelización de clientela que funciona en euros con descuentos (que se aplican por ser de la red de moneda social, y equilibrando los descuentos de unas personas con los descuentos de otras), y cuyos clientes funcionan como un grupo de consumo que lleva sus cuentas en moneda social, pero que con el tiempo va reduciendo el uso de la moneda social, al perderse la variedad productiva y detenerse el proceso de crecimiento.

Finalmente se convertirán una red de personas que consume colaborativamente en euros, pero que se tienen confianza entre ellos y obtienen euros a través de la organización de fiestas y eventos. Todo ello resulta un inconveniente para el crecimiento de la economía alternativa.

Es por esto que las personas que integran la red no deberían utilizar los euros cuando intercambian entre ellas, fomentando el funcionamiento de la red 100% en moneda social, y confiando en que la red sabrá darle a las monedas sociales el poder social que tienen los euros, pero para hacerlo, la misma red debe tener confianza en su propia moneda.

Cuando hablamos de la funcionalidad de las monedas sociales como euros, lo que tratamos de decir es que las monedas sociales deben funcionar para las personas de la red con el mismo potencial con el que funcionan los euros para las personas ajenas a la red, pero no deben ser intercambiadas por euros bajo ningún concepto (ninguna persona de la red debe recibir euros de la red a cambio de monedas sociales ni de sus bienes o servicios), sino que la moneda social debe servir para poder pagar esos gastos en euros a través de la red.

Así, la gente no debe pedir euros al entregar un producto o servicio a la red, sino que debe pedir únicamente moneda social, aceptando los intercambios 100% en moneda social, para que las personas que forman la red puedan acceder a los bienes y servicios sin necesidad de euros.

Lo que supone un gran paso evolutivo dentro de la red es cuando las personas que la integran, e incluso las personas ajenas a la red, empiezan a ver que para adquirir determinados bienes o servicios necesitan precisamente moneda social, y les resulta difícil acceder a dichos bienes o servicios usando únicamente euros, para lo que es 144necesario que la red esté compuesta por personas "intensas" (aquellas que solo entregan bienes y prestan servicios a cambio de monedas sociales).

Con esto tratamos de mostrar que dentro de la economía alternativa, la moneda social debe tener un valor superior al de la moneda oficial, y debe ser preferida aquélla frente a ésta, pero eso es algo difícil de conseguir mientras dentro de la propia red se sigan exigiendo euros para adquirir bienes o servicios, o las personas de la red vendan los productos más caros en moneda social de lo que los venden en euros, porque en esos casos es la misma red la que sigue sin confiar plenamente en su propia moneda, y la gente preferirá tener una contabilidad en euros, por resultar más sencillo, más útil y más barato.

- - -

## 6.3.- La adquisición de bienes y servicios con moneda social

La solución radica en que quienes dicen que solo pueden aceptar un 30% de moneda social en la venta de sus productos o servicios, no mezclen ambos sistemas económicos tratando de usar un "modelo uniforme" (cobrar el 30% en moneda social sobre el 100% de su actividad económica), sino que resulta necesario para el buen desarrollo de la red que apliquen un "modelo separado" (cobrar el 100% en moneda social, pero aplicado únicamente sobre un 30% de su actividad económica, que es el porcentaje que pensaban incorporar a la red).

En ambos casos, dicha persona seguirá poniendo en la red de moneda social únicamente el 30% de su producción, y mantendrá el 70% dentro del sistema oficial, pero al menos, en el modelo separado, la parte destinada a la red no estará sometida a la condición de haber conseguido euros para intercambiar con ella.

Quienes usan el modelo uniforme está perjudicando a la red de dos maneras, ya que de una parte están haciendo que la red sólo funcione entre aquellas personas que tienen el % de euros que exigen, excluyendo precisamente a quienes tratan de mantener su economía 100% en moneda social. Y de otra parte están impidiendo la gestión colectiva de los euros que podrían ingresar en la red, ya que éstos se quedan en manos de aquellas personas que cometen este error, haciendo que solo esta parte de la red pueda satisfacer sus necesidades en euros, y obligando a quienes pretendían usar 100% moneda social a que vuelvan a pedir euros a cambio de sus bienes o servicios.

Evidentemente, los Productores de Economía Mixta que usan el modelo uniforme, suelen ver incrementadas sus ventas porque la red empieza a ver la aceptación de moneda social como un descuento del precio en euros, lo que le ayuda a promocionar su negocio y a incrementar su volumen de ventas en euros, y lo hace precisamente a costa de la red de moneda social, convirtiendo la red en una campaña de fidelización de la clientela.

Resulta evidente que debe haber una manera en la que quienes tengan moneda social y hayan renunciado a ingresar euros para ingresar monedas sociales puedan acceder a los servicios que necesitan pagar en euros utilizando para ello las monedas sociales que han generado con su actividad. Pero dicha manera no debe pasar por convertir las monedas sociales en euros, ni por iniciar una fase de retorno a la economía en euros, sino que habrá que ver la manera en que la moneda social es capaz de funcionar como un grupo de consumo y utilizar esos euros de manera colectiva, obteniendo una mayor rentabilidad a través de la compra colectiva, generando una riqueza adicional, y permitiendo que quienes necesiten acceder a algún bien o servicio que solo esté disponible en euros puedan hacer que la red afronte ese gasto en su nombre, permitiendo a su vez que esa persona afronte dicho gasto frente a la red 100% en moneda social.

Las personas que integran la red de moneda social no tienen una necesidad directa de euros, sino que lo que realmente necesitan es que haya euros disponibles en la red para que la red pueda hacer frente a esos gastos necesarios, para que a esa persona se le pueda permitir afrontar esos gastos en moneda social. Así, las monedas sociales pueden tener la misma funcionalidad que los euros, solo que la red incorporará parte de sus valores y principios a esa parte de la economía, exigiendo transparencia, reciprocidad y un criterio relativamente sensato sobre qué gastos son necesarios, y cuáles no lo son, para lo que será necesario adoptar una decisión colectiva de la red desde la esfera de la democracia, ya que los gastos que se consideren necesarios deben ser necesarios para todas las personas que integran la red, y debe tenerse en cuenta la proporción de euros que suponen dichos gastos, y la proporción de euros que la red es capaz de generar para afrontarlos.

Esto supone la creación de una parte de la economía oficial que se incorpora a la red para gestionarla de manera colectiva, y supone un avance en la dirección contraria a una economía que utiliza un % en euros y un % en moneda social. En un caso, es la red la que asume una parte del sistema económico oficial dentro de su funcionamiento (el sistema oficial come terreno a la moneda social), mientras que en el otro, es el funcionamiento de la red el que se extiende a una parte del sistema económico oficial al gestionar colectivamente esas necesidades en euros (los valores de la moneda social comen terreno a la economía oficial).

- - -

## 6.4.- Los grupos de consumo

Una vez que son detectadas esas partes de la economía oficial que la red considera necesidades, es cuando debe empezar a gestionarse dicha necesidad como un grupo de consumo, y así, en lugar de pelear entre ellos por poder obtener el servicio del monopolio oficial que se ha considerado una necesidad, la red empieza a cooperar para que dicho monopolio preste al grupo el servicio que resulta necesario en las mejores condiciones de consumo y a un precio más económico en términos de moneda oficial. Y el ahorro empieza a convertirse en una forma de financiación.

Estos grupos de consumo funcionan para las monedas sociales como un caballo de Troya dentro del sistema oficial, ya que su esencia radica en que se consolida un grupo que va a consumir grandes cantidades de productos o de servicios, lo que permite comprar dentro del sistema oficial con unos descuentos considerables.

Para entender esta lógica basta con entender que la moneda social está ahorrando a ese productor el trabajo de distribución, ya que el grupo recibirá una parte considerable de la producción en una única compra, y posteriormente ya se ocupará el grupo de distribuir los productos entre sus integrantes.

Este grupo de consumo podrá utilizar las infraestructuras físicas o telemáticas de la red para almacenar y distribuir los productos y servicios entre las personas que los demandan, pero en dicha distribución estarán usando los valores de la economía alternativa, y llevarán sus cuentas de manera transparente y accesible a todas las personas que forman parte del grupo de consumo, y de la red.

Este grupo de consumo podrá estar formado tanto por gente de la red como por gente ajena a ella, siendo posible que la red retire su  articipación en el grupo de consumo para gestionarla dentro de la red como un grupo de consumo interno de la red, o bien gestionando el grupo de consumo completo, en cuyo caso deberá permitir a las personas ajenas a la red, pero que forman parte del grupo de consumo, abonar sus compras en moneda oficial.

La relación con dichas personas no se basará en la moneda social, pero será transparente, y permitirá que tanto las personas de la red, como esas personas que, sin formar parte de la red, desean utilizar la fuerza de los grupos de consumo, obtengan mejores precios y servicios, tanto en la red de moneda social como en el sistema oficial.

La peculiaridad será que unas podrán adquirirlos de la central de intercambio directamente en moneda social o intercambio, y otras, que serán las personas ajenas a la red, tendrán que pagarlos forzosamente en euros.

Estos euros revertirán directamente en la caja común para la adquisición de más bienes y productos, del mismo modo que los bienes y productos aportados directamente por la red quedarán también en la central de intercambio listos para ser consumidos.

En dicha central de intercambio, las personas de la red podrán retirar esos productos 100% en moneda social, o en intercambio por otros bienes o servicios que hayan sido demandados por la red, o por el grupo de consumo.

El grupo de consumo tendrá una cuenta pública dentro de la contabilidad en moneda social, de manera que podrá recibir los pedidos y realizar los pagos y cobros en moneda social por vía telemática, pero como ente colectivo que es dentro de la red, tendrá a su vez una cuenta ecolatipac, que funcionará tal y como se describe en el apartado relativo al ecolatipac dentro del último capítulo de este manual.

- - -

## 6.5.- Productores y prestadores de servicios de economía mixta

Del mismo modo que los consumidores de la red intercambian 100% en moneda social, los productores de la red también intercambian 100% en moneda social, debiendo notarse que dentro de la red todas las personas son productoras y consumidoras a la vez, ya que todas hacen intercambios indirectos entre ellas. Por eso no resulta difícil entender que las personas que integran la red utilicen 100% moneda social en todos los intercambios que realizan dentro de la propia red.

Pero antes mencionamos la existencia de proveedores del grupo de consumo que forman parte de la red, y otros a los que la red debe realizar las compras en euros, y son dichos productores los que la red debe tratar de incorporar a la red para que sus productos y servicios sean ofertados dentro de la red 100% en moneda social, para que puedan ser considerados como parte activa de la red de moneda social. Por eso, mientras acepten euros de la red seguirán siendo jugadores de economía mixta que están obligando a las personas de la red a mantener una economía en euros.

Para ello hay varias cosas que pueden hacer las redes de moneda social con dichos productores. Una de ellas es admitir que durante un tiempo prueben a utilizar un % concreto de moneda social, ya sea el 10%, el 30%, el 50%, o el que cada productor acuerde con la red, convirtiéndose en productores de economía mixta (en abreviatura PEM). En reciprocidad se les permitirá formar parte del grupo de consumo, pero utilizando únicamente el % de moneda social que cada persona acepta de la red.

Es decir, que si un productor acepta el 10% en moneda social, luego solo podrá consumir abonando un 10% en moneda social. Esta medida debe ser temporal, ya que si una persona se convierte en un gasto fijo de 90% en euros, la red deberá seguir buscando sustitutos para dicha persona, probando diferentes productores hasta encontrar a las personas productoras dispuestas a aceptar el mayor % de moneda social posible, quedándose finalmente con aquellas que aceptan el 100% en moneda social. En ese momento esa persona pasará a ser un productor de la red, por lo que producirá y consumirá realizando sus intercambios 100% en moneda social.

Pero este uso mixto con los proveedores del grupo de consumo debe ser regulado desde el mismo grupo de consumo y desde la red, ya que las personas que integran la red deberán tener en cuenta esta peculiaridad de dicha persona a la hora de intercambiar con ella, de manera que si alguien que acepta el 100% de moneda social, intercambia algo a una persona que solo acepta el 50% en moneda social, debe hacerlo a través del grupo de consumo.Cuando alguien de la red entrega algo a un PEM, debe anotarse el 100% en moneda social, obteniendo el 50% de esa persona y el 50% del grupo de consumo, todo en moneda social. Ese PEM entregará al grupo de consumo el 50% en euros, y a la persona que le entrega el bien el 50% en moneda social. De esa manera se puede conservar el uso del 100% para la persona de la red, y el grupo de consumo es quien recibe la financiación en euros para futuras compras de los PEM, equilibrando el desajuste entre los perfiles económicos de esas dos personas.

Cuando alguien de la red recibe algo de los PEM, será mejor hacerlo a través del grupo de consumo, ya que será éste quien deba entregar el 50% en euros a dicha persona, y la persona de la red entregará el 50% en moneda social a dicho PEM, y el 50% en moneda social al grupo de consumo, realizando así su pago 100% en moneda social. Por esto es importante que este tipo de proveedores de economía mixta sean considerados como gente ajena a la red, y para realizar esas ventas deben haber sido aceptados previamente por todos los integrantes del grupo de consumo.

Estos proveedores de economía mixta, a su vez, tendrán relaciones con personas totalmente ajenas a la red que pueden formar parte del grupo de consumo, por lo que si aportan sus productos al 50% en moneda social, y alguien extraño a la red decide consumir sus productos en el grupo de consumo, éste hará lo mismo que hace con las personas de la red y gestionará el intercambio entre el PEM y la persona ajena a la red. Así, la persona ajena a la red abonará el 100% en euros al grupo de consumo, y éste abonará el 50% en euros al PEM, y el 50% en moneda social, tal y como si el intercambio lo hubiera realizado una persona de la red, quedando el 50% en euros en poder del grupo de consumo para las compras futuras.

Existe una solución más directa para los PEM (citada anteriormente como "modelo de economías separadas"), y que consistiría en que si solo quieren aceptar el 50% en moneda social, se les pide que vendan solo la mitad de lo que querían vender, y que lo vendan 100% en moneda social. De esa manera pueden vender el 50% que quieren vender en euros en otra parte, alejados de la moneda social y del grupo de consumo. Esta medida tiene la ventaja de que favorece el funcionamiento 100% en moneda social, y además permite averiguar quienes de los que funcionan como PEM están dentro de la red para conseguir euros, y quiénes están tratando realmente de ampliar el uso de la moneda social dentro de sus posibilidades. A quienes entran en la red para conseguir euros habría que recordarles que para funcionar en euros hay todo un sistema económico capitalista extendido por el mundo, y decirles que no tienen porqué venir a buscar euros precisamente a un lugar donde tratamos de liberarnos del uso de los euros.

- - -

## 6.6.- El uso compartido frente al alquiler

Por último, dentro del funcionamiento de los grupos de consumo, existe una faceta que incumbe más al uso sobre los bienes que al consumo de los mismos, ya que no todos los bienes que necesitamos desparecen al consumirse, tal y como sucede con las bienes de uso.

Para estos bienes también resulta muy beneficioso el grupo de consumo, ya que en lugar de comprar un bien que utilizaremos pocas horas en nuestras vidas (como pueden ser un taladro, una motosierra, un generador de electricidad, brochas para pintar, u otro tipo de herramientas), dichos bienes o herramientas pueden ser adquiridos de manera colectiva y ponerse a disposición del grupo de consumo a cambio de monedas sociales, o si la persona usuaria no pertenece a la red de moneda social, podría establecerse una relación de alquiler sobre dicho bien, así las personas podrían utilizar el bien y aportar euros para recuperar lo invertido en la adquisición, o bien aportar el uso de otras herramientas, u otros bienes y servicios que puedan ser necesitados por la red.

Esta idea resulta aplicable desde el momento en que las personas de la red ofrecen prestar herramientas, vehículos o cualquier tipo de bienes, pero si resultase necesario incorporar algún bien de uso a la red, dicho bien también podría adquirirse desde la propia red y servir como elemento capaz de generar riqueza interna, al cubrir una necesidad más del colectivo.

Incluso más allá de los bienes, hay otras necesidades que pueden ser compartidas por todas las personas de una red, o incluso y sobre todo por personas de distintas redes,como puede suponer la necesidad de alojamiento y alimentación durante los viajes, siendo posible que una red ofrezca dichos servicios en moneda social como manera de colaborar con la red.

Esto permitirá que dicha persona pueda compartir el espacio libre en su casa, haciendo que la persona alojada pueda ahorrar en el precio de dichos servicios al no tener que contratarlos con el sistema de moneda oficial, y tener dichos servicios disponibles dentro de las redes de moneda social.

Y del mismo modo que con los bienes en régimen de alquiler, la moneda social, a través de su forma jurídica, puede ofrecer un servicio de acogida para personas ajenas a la red que, a la vez que proporciona moneda social a la persona que acoge, servirá para que la red ingrese más moneda oficial que permita seguir ampliando el número de bienes y servicios que, sin ser producidos por la red puedan ser satisfechas a través de ésta. pero esta dinámica será analizada en el capitulo siguiente bajo el concepto de "ecolatipac".

* * *

# 7.- EL ECOLATIPAC Y LA DESCAPITALIZACIÓN

## 7.1.- La aceptación de moneda oficial. El Ecolatipac.

Ya se ha aludido varias veces a la aceptación de moneda oficial por parte de la red de moneda social a través del ecolatipac, por lo que ahora trataremos de explicar qué es exactamente el ecolatipac y cómo funciona.

El Ecolatipac es un usuario colectivo y público creado dentro de la red de moneda social cuya funcionalidad consiste en permitir el ingreso de euros en la red de moneda social y afrontar los pagos en euros a los que deba hacer frente la red, permitiendo que la gestión de dichas monedas oficiales pueda ser gestionada de manera colectiva aportando a la red todos los beneficios de los grupos de consumo.

Al ser una cuenta de usuario dentro de la red, resulta evidente que una red podrá tener uno o varios ecolatipacs dependiendo de cómo pretenda gestionar las monedas oficiales que entren en la red. Podrá permitir que cada usuario tenga su propio ecolatipac para descentralizar esa gestión, podrá tener un único ecolatipac que gestiones la moneda oficial de toda la red para centralizar la gestión, o podrá tener varios ecolatipacs atendiendo a las materias o servicios que pretenda gestionar cada uno de ellos, utilizando la centralización y la descentralización según cada materia o grupo concreto.

La moneda social tendrá al menos un ecolatipac, lo llamen así o lo llamen de otra manera, porque toda red de moneda social es capaz de generar un excedente de riqueza que no puede ser consumido por la propia red, y dicho excedente suele exportarse a la economía oficial para ser intercambiado por lo que dicha economía sea capaz de aportar a la red, y la mayoría de las veces, la economía oficial no puede aportar nada más que euros. Si aportase otro tipo de bienes o servicios, estarían entrando a formar parte del sistema económico alternativo basado en el intercambio, por lo que, en esos casos, no sería necesario utilizar el ecolatipac, sino incorporar a dichas personas a la red directamente.

A la red no le supone un problema la aceptación de estos euros, o al menos, no se lo supone mientras tenga necesidades que no pueda satisfacer por sí misma y necesite acudir al sistema económico oficial para satisfacerlas. Pero debe tenerse en cuenta que el ecolatipac es el paso que supone una gestión colectiva de dichas monedas oficiales, permitiendo a las personas de la red aceptar 100% de moneda social en sus intercambios, en lugar de tener que preocuparse por conseguir euros para satisfacer las necesidades que solo pueden cubrirse en euros.

Para que el ecolatipac funcione, debe estar asociado a una caja donde se almacenan las monedas oficiales (los euros), y de esta manera, permitir a toda la red conocer el número de euros de que dispone, dato que conocerán al conocer el número de monedas sociales negativas que tiene el ecolatipac, ya que éste anotará monedas sociales en negativo por cada euro que entre en la caja, y anotará monedas sociales positivas cada vez que un euro salga de la caja. Se debe tener claro que el ecolatipac funciona como el espejo de la parte del sistema capitalista que utiliza la red de moneda social para la gestión de sus euros. De ahí que su nombre esté formado por la palabra "capital" escrita al revés, precedida de la palabra "eco".

El ecolatipac funcionará de dos maneras diferentes, ya que de un lado funcionará frente a las personas ajenas a la red para entregar o adquirir a personas ajenas a la red los bienes y servicios de la red que no hayan sido consumidos o producidos por ésta. Y de otro lado, funcionará de cara a la gente de la red para permitirles entregar o adquirir dichos bienes y servicios usando una economía 100% en moneda social.

Con esto debe entenderse qué es el ecolatipac y cómo funciona, ya que si alguien de la red fabrica camisetas que no son consumidas en la red, dichas camisetas podrán intercambiarse por euros a través del ecolatipac, para invertir dichos euros en aquellos bienes y servicios que la red hubiese querido recibir de la persona que entregó los euros, para tenerlos a disposición de la red. Esto se hará del siguiente modo:

- La persona que fabrica las camisetas conseguirá monedas sociales del ecolatipac al entregar sus camisetas, y el ecolatipac conservará los euros como un saldo positivo de la caja, pero lo reflejará contablemente como monedas sociales negativas, ya que dichas monedas sociales negativas equivalen a las monedas sociales positivas que recibe el fabricante de camisetas.

- Cuando dicha persona decida adquirir algún producto que no sea producido por la red y que entre dentro de lo que la red considera "necesidades", podrá pedir al ecolatipac que adquiera dicho bien o servicio con esos euros, y el grupo de consumo lo hará en condiciones óptimas de calidad y precio, para permitir a esa persona adquirir dicho bien o servicio 100% en moneda social.

- Dicha persona entregará monedas sociales positivas al ecolatipac a cambio de dicho bien o servicio, haciendo que el círculo del intercambio se cierre cuando la persona haya recibido su bien o servicio a cambio de las camisetas que entregó en un principio.

- El ecolatipac compensa sus saldos positivo y negativo en moneda social cuando dicho círculo se cierra. Digamos que el ecolatipac se encargará de convertir esos euros en los bienes y servicios que la red necesita del exterior. O sea, que el ecolatipac funciona como un usuario que representa a toda la sociedad que prefiere utilizar euros en lugar de moneda social para realizar los intercambios.

- Es necesario destacar aquí la importancia de definir las necesidades de la red que van a satisfacerse desde el ecolatipac, ya que el nivel de funcionamiento de esta herramienta dependerá de que el volumen de excedentes convertido a euros desde el grupo de consumo mantenga cierta proporcionalidad con el volumen de necesidades que se pretende satisfacer desde dicho grupo de consumo.

- Será la definición de estas necesidades la cuestión que las redes de moneda social deberán plantearse de manera colectiva, asumiendo que cualquier necesidad que sea aprobada para ser satisfecha desde el ecolatipac pueda ser adquirida para todas las personas de la red, aunque luego solo podrán satisfacerla aquellas personas que dispongan de moneda social para afrontar el pago de tales bienes o  servicios en moneda social. Dichas personas serán normalmente las mismas que aportan a la red las cosas que necesita y reciben monedas sociales a cambio.

- - -

## 7.2.- La generación de riqueza real frente a la moneda oficial

Una vez que la red entiende la existencia del ecolatipac como punto de conexión e intercambio entre la red de moneda social y la economía oficial, hay que recordar aquellas ventajas que trataba de insertar en la economía el capitalismo úitl, es decir, aquél que coordinaba las fuerzas de producción para reducir los costes y obtener un mayor número de productos para satisfacer un mayor número de necesidades.

En la primera parte de este manual vimos como el capitalismo monetario terminó utilizando el capitalismo útil para generar un mayor número de monedas dentro del sistema oficial, lo que suponía la ejecución de procesos productivos dentro de la economía oficial.

Por ejemplo, alguien compraba 5 huevos por 5 euros, y elaboraba 5 tortillas que posteriormente vendía a 2 euros cada una, obteniendo en total 10 euros, lo que suponía una creación de riqueza monetaria por valor de 5 euros adicionales sobre los euros invertidos inicialmente.

Dentro de la moneda social, lo que interesa no son esos 5 euros adicionales, sino la creación de la riqueza real, en este caso la elaboración de tortillas, ya que dentro de la red serán las 5 tortillas creadas lo que se considere riqueza real. Ya que esas tortillas son el bien que va a ser consumido por la red, dentro de la red, esas tortillas pasarán a ser cuantificadas en moneda social y no en euros.

Si tenemos en cuenta que dentro del ecolatipac, el número de euros debe tener un reflejo en moneda social, puede resultar tentadora la idea de utilizar los euros de la red para tratar de multiplicar dichos euros, pero eso llevaría a la red a estar trabajando desde el prisma de la riqueza monetaria, y estaría exigiendo a la red la continua adquisición de euros, o estaría dedicándose más a satisfacer las necesidades de la gente que usa euros, que las necesidades de la gente que usa moneda oficial. Por eso es importante que esa riqueza no sea generada dentro de la contabilidad en euros, sino que la red deberá tener como misión trasladar esa generación de riqueza al interior de la red de moneda social, para que dicha riqueza sea generada en la contabilidad de la moneda social, y no en la contabilidad oficial.

Esta pieza del funcionamiento de la red es de suma importancia, ya que en la economía alternativa debe tenerse claro que si se adquieren herramientas para reparar vehículos que se ponen a disposición de las personas de la red que saben reparar vehículos, no se hace para que dichas personas se dediquen a reparar vehículos a cambio de euros, sino que la prioridad será que todas las personas de la red tengan acceso a un taller que funcione 100% en moneda social, independientemente de que cuando nadie de la red necesite reparar su vehículo, dicho servicio pueda ofrecerse a personas ajenas a la red a través del ecolatipac y del grupo de consumo para realizar un intercambio con el sistema económico oficial.

Pero esto se hará con la única intención de no desaprovechar las habilidades de la persona que sabe reparar vehículos, y la de utilizar las herramientas disponibles en beneficio de la comunidad, por lo que el precio en euros que debería poner la red para dicho servicio fuera de la red debería ser más barato que el precio en el mercado oficial, aunque del mismo modo, dicho precio podrá ser más caro que el precio del mismo servicio para gente que usa 100% en moneda social.

Y se atenderá prioritariamente a la gente de la red, al ser la creación de dicho servicio dentro de la red la auténtica finalidad de ese "taller de vehículos en moneda social", o "ecotaller". la gente de la red debe consumir bienes y servicios de mayor calidad y a mejores precios.

De esta manera, el ecotaller empezará a ser una actividad más dentro de la moneda social, y las personas que desarrollan esa actividad podrán emitir moneda social hasta el límite máximo de saldo negativo, y podrán recibir monedas sociales de quienes intercambien sus servicios en el ecotaller, ya sean otras personas de la red, o sean el ecolatipac y el grupo de consumo. El alquiler de las herramientas podrá satisfacerlo en moneda social al grupo de consumo, si las necesita. Y el ecotaller podrá empezar a realizar trabajos dentro de la red que serán una réplica del capitalismo útil, en la medida en que dicho servicio será accesible para la gente de la red, pero también empezará a tener lugar una generación adicional de monedas sociales, ya que dicha persona empezará a convertir su trabajo personal de mecánico en monedas sociales para poder intercambiarlo por otros bienes y servicios de la red.

De esta manera, el ecotaller podrá obtener beneficios en moneda social, del mismo modo que quien compra 5 huevos por 5 monedas sociales y elabora 5 tortillas que vende a 2 monedas sociales cada una, obtiene 10 monedas sociales, lo que supone un beneficio de 5 monedas sociales que la red le entregará por su trabajo elaborando tortillas.

De esta manera se hace que la riqueza generada por la red de moneda social se cuantifique dentro de la moneda social, sin que las personas de la red necesiten en ningún caso generar este tipo de riqueza en euros, ya que será la red la que se encargará de generar la riqueza en euros a través del ecolatipac y el grupo de consumo cuando sea necesario, pero dentro de la red, las personas cuantifican sus intercambios y sus riquezas en moneda social. Otro día habrá alguien que elabore 5 tortillas para esa persona y los saldos se compensarán.

Para el funcionamiento de cara al exterior lo aconsejable es tener alguna forma jurídica que pueda desarrollar todas las actividades de la red de cara al sistema oficial, para que dichas actividades de cara al sistema puedan desarrollarse cumpliendo con toda la legalidad del sistema oficial, pero es necesario insistir en que la parte realmente interesante es la creación de un sistema económico alternativo basado en el intercambio, la cooperación y el apoyo mutuo, y ese sistema es el que funciona en monedas sociales al 100%, aunque en sus inicios resulte necesario apoyarse en el ecolatipac para satisfacer determinadas necesidades.

Con todo ello, podemos decir que del mismo modo que la gente creaba riqueza en euros, dentro de la red resulta posible la generación de riqueza en monedas sociales, pero la riqueza realmente creada serán los bienes y servicios disponibles en la red, ya que las monedas reflejan los usos concretos de dichas riquezas reales sin ser la riqueza real cuyo movimiento representan.

Pero hay que tener en cuenta un pequeño problema, y es que si la red invierte 10 euros del ecolatipac en una actividad fuera de la red y consigue generar 20 euros, existirían 10 euros generados por la red de manera evidente, que contabilizan como un beneficio de 10 euros, y esos 10 euros no tendrán moneda social que los equilibre dentro de la red, ya que no se han generado a través de un intercambio dentro de la red, y esto podría hacer descuadrar el saldo en euros del ecolatipac con su saldo en moneda social, además de que la red habría generado un beneficio de 10 euros al más puro estilo capitalista, quedando sometido a su régimen fiscal.

Para evitar esto, la red se abstiene de realizar operaciones de inversión en euros que solo vayan a reportar euros, ya que su objetivo no es generar beneficios en el sistema oficial, sino "satisfacer todas las necesidades de las personas que componen la red con los recursos de los que disponen".

Por eso es que el objetivo de las economías alternativas consiste en incrementar sus riquezas reales, en lugar de hacer crecer su riqueza monetaria, que, como ya se ha visto anteriormente, tenderá a aproximarse al cero por aplicación de las leyes de equilibrio y de gravedad.

Con este sistema de funcionamiento se rompe el axioma capitalista de que el número de monedas existente en un grupo equivale al total de las riquezas de un grupo, ya que en las monedas sociales las unidades de confianza se utilizan para cuantificar la confianza utilizada por la red, y no las riquezas de la misma. En este sentido conviene recordar que la utilización de esta confianza representa un desajuste entre las necesidades de una persona y su capacidad para satisfacerlas, ya que dicha necesidad ha sido satisfecha por otra persona de la red que confía en que la red le ayude en un futuro a satisfacer alguna necesidad suya.

- - -

## 7.3.- La descapitalización

Por último, y para terminar de explicar el funcionamiento completo del ecolatipac, es necesario aludir al mecanismo de la "descapitalización", cuyo función primordial consiste en "transformar los euros obtenidos por la red en riquezas reales dentro de la red", haciendo que se reduzca el tablero en el que la economía funciona en euros para ampliar el tablero en el que la economía funciona en moneda social. Y para ello debemos ver varias cosas.

La primera cosa que habrá que ver de ¿De dónde salen esos euros que pueden ser descapitalizados por la red de moneda social?. Y evidentemente salen del propio funcionamiento de la red, de diferentes maneras.

- - -

### 7.3.1.- Formas de ingresar euros:
1. __Aportaciones voluntarias:__ Una de ellas consiste precisamente en las aportaciones voluntarias que las personas de la red deseen hacer a la red para que sean descapitalizados, cosa que sucede cuando una persona de la red decide adquirir bienes o servicios de la red, pero en lugar de pagar con moneda social, decide libremente pagar con euros para que dichos euros sean descapitalizados a través de la red. Esto sucede cuando hay personas que, a pesar de entender este sistema económico, permanecen trabajando dentro del sistema percibiendo un salario únicamente en euros, por lo que carecen de tiempo para realizar actividades productivas en moneda social, y pagan en euros a la red conociendo el método de la moneda social.

2. __Proveedores de Economía Mixta:__ Otra manera de obtener euros para el proceso de descapitalización consiste en aquellos proveedores de la red que todavía no se han atrevido a incorporarse plenamente en la red y que prefieren usar la moneda social solo de una manera parcial. Debemos recordar que este tipo de productores y prestadores de servicios podían funcionar dentro de la red únicamente con el porcentaje que estaban dispuestos a aceptar en moneda social, ofreciendo el 30% de sus productos a la red a cambio de monedas sociales al 100%, en cuyo caso siguen la dinámica de la red en el % de su actividad que hayan pactado con la red, al 100% en moneda social.

3. Pero es posible que estos productores prefieran aceptar el 30% en moneda social sobre todos sus productos, en cuyo caso resulta posible aplicar un mecanismo de descapitalización mediante el reflejo de una participación en cada venta por parte de la red. Es decir, si dicha persona participa en la red al 30%, seguirá vendiendo sus productos a gente ajena a la red, y dichas personas pagarán 100% en euros, por lo que el vendedor o prestador de servicios puede reflejar contablemente que el 70% de esa venta la ha realizado ella, pero el 30% restante ha sido realizado por la red, por lo que ese 30% de euros irá destinado a la caja del ecolatipac y dicha persona recibirá ese 30% en monedas sociales, del mismo modo que si la compra la hubiese hecho una persona de la red.

 Para poder reflejar contablemente esta participación 30-70 en dicha venta, siempre será mucho mejor que la venta se haga a través de la forma jurídica de la moneda social, ya que dentro de la red resulta mucho más sencillo atribuir la gestión del 70% en euros a la persona que realiza la venta, de lo que podría resultar si se hace manteniendo una venta realizada por dos vendedores distintos a la vez.

 Es por ello que dichas ventas se realizan desde la forma jurídica de la red, pero el ecolatipac solo conservará en su poder el 30% de dicha venta en euros, y entregará a ese Productor de Economía Mixta el 70% en euros de dicha venta. Recuérdese que este mecanismo sólo puede mantenerse durante un tiempo limitado para cada productor, ya que el objetivo será que dicho productor incremente su porcentaje de moneda social hasta que el mismo sea del 100% en moneda social, en cuyo caso se acabará el problema de estos repartos y conversiones porcentuales para que todos los euros puedan ser gestionados a través del grupo de consumo de la red, permaneciendo accesibles a la red a través del ecolatipac, y evitando que los euros generados por el funcionamiento de la red queden en manos particulares.

 Venta de productos y servicios que se convierten en excedentes y que sean realizadas a personas ajenas a la red: También es posible que haya personas ajenas a la red que deseen adquirir los bienes y servicios de la red para beneficiarse de las ventajas de consumir a través de un grupo de consumo, sabiendo que estas personas ajenas a la red solo podrán pagar en euros. Dichos euros ingresarán directamente en el ecolatipac, y el ecolatipac asumirá tantas monedas sociales negativas como euros reciba por este concepto.

 Como estos apuntes negativos necesitan tener un reflejo en monedas positivas para mantener el balance a cero, dichas monedas positivas se anotarán a favor del productor de la red que haya aportado el bien o servicio consumido por la persona ajena que ha pagado en euros, de esa manera, el productor de la red recibe el precio de la venta en moneda social dejando los euros en el ecolatipac, sabiendo que así dichos euros serán utilizados de manera colaborativa y su inversión será mucho más rentable a través del grupo de consumo. Así podrá conservar su economía 100% en moneda social.

 Durante los eventos que se celebran con formato billete, el ecolatipac anotará únicamente su saldo negativo, entregando las monedas positivas en un "formato billete" a la persona que entrega los euros, para que pueda utilizarlas como monedas sociales "al portador" durante el tiempo que dure el evento, y al finalizar el evento, las personas de la red entregarán las "monedas sociales al portador" quehayan aceptado durante el evento para que dichas monedas sociales sean anotadas a su favor en el sistema contable informático de la red.

4. __IVA repercutido en cada venta:__ Como cabe la posibilidad de que haya personas ajenas a la red consumiendo productos de la red, dichas ventas realizadas sin utilizar moneda social deberán cobrar el correspondiente IVA, lo que fiscalmente se conoce como "IVA repercutido" (por ser el IVA ese Impuesto sobre el Valor Añadido que debiendo pagar cada ciudadano al comprar, es el vendedor quien debe repercutirlo a los consumidores, cobrándoles el % establecido por el Estado como IVA para esa operación, para posteriormente entregarlo al Estado al hacer su declaración del IVA).

 Posteriormente, en la declaración del IVA hay que compensar el IVA repercutido a los consumidores y el IVA soportado en la adquisición de bienes y servicios necesarios para la realización de la actividad. Este IVA habrá sido soportado por el grupo de consumo y el ecolatipac, que es la cuenta que gestiona los euros del grupo de consumo, y como el ecolatipac tiende a consumir todos los euros que ingresa, lo normal es que tenga tanto IVA soportado como IVA repercutido (es decir, que cada vez que un consumidor ha comprado algo el ecolatipac ha pagado un IVA que suele coincidir o estar muy próximo a la cantidad de IVA que ha repercutido a los consumidores que pagan en euros).

 Esta similitud entre IVA soportado e IVA repercutido, hace que la declaración de IVA tenga un valor muy próximo a cero, por lo que el IVA repercutido realmente no se ingresa en el Estado, sino que se utiliza durante el mismo ejercicio económico para pagar el IVA correspondiente a las adquisiciones que se realizan. Esto conlleva a la necesidad de solicitar y guardar las facturas correspondientes a todas las adquisiciones realizadas por el grupo de consumo, ya que serán dichas facturas las que acrediten que el IVA repercutido se ha invertido en soportar el IVA correspondiente la actividad empresarial de la red de moneda social.

 Es por esta faceta mixta empresarial y de consumo que supone la compensación de los IVA soportado y repercutido la razón de que las redes de moneda social tenderán a convertirse en formas jurídicas que permitan el ánimo de lucro y la realización de actividades empresariales variadas, aunque su funcionamiento no vaya a dar beneficios en euros por su propia dinámica de funcionamiento. usar un modelo asociativo sin ánimo de lucro no permite realizar estas actividades empresariales repercutiendo el IVA, y sin embargo, será muy complicado que los centros comerciales o los proveedores del sistema acepten a vender al ecolatipac sin cobrar el correspondiente IVA, por lo que para los grupos de consumo resulta más rentable fiscalmente compensar los IVA de las ventas y los ingresos, que vivir en un régimen de exención del IVA, porque habrá IVA que se vean obligados a soportar y que no podrán compensar de otra manera.

 Para entender la ventaja de esta manera de financiación, debe tenerse en cuenta que los euros pagados en las actividades de consumo son pagados por todas las personas, ya que todas las empresas suelen cobrar el IVA, y utilizando la forma jurídica y asumiendo las obligaciones fiscales inherentes a ella, dicho IVA soportado puede verse compensado con el IVA repercutido que, sin una forma jurídica que emita las facturas y repercuta realmente el IVA, no podría compensarse.

 Incluso podría barajarse la posibilidad de solicitar a hacienda la devolución del IVA soportado con la actividad, si la red llegase a tener un resultado negativo, pero dicho resultado supondría que la actividad ha tenido pérdidas, por lo que convendría revisar la causa de las mismas antes que nada, y en su caso, solucionar dichas pérdidas en lugar de considerarlas una forma de financiación, siendo lo aconsejable trasladar las leyes de Equilibrio y de Gravedad al funcionamiento de la red dentro del sistema económico oficial, para contagiar al sistema oficial de nuestra tendencia al balance cero. Por lo que será aconsejable no dar pérdidas.

5. __Ahorro generado en el grupo de consumo:__ Otra manera de ingresar euros la supone directamente la reducción en los precios a la hora de realizar un consumo colectivo, ya que, por poner un ejemplo, en un establecimiento donde una comida cueste 10 euros normalmente, la red de moneda social puede negociar una compra colectiva de 100 comidas, exigiendo al establecimiento un descuento de 3 euros en cada comida, haciendo que cada comida cueste 7 euros para las personas de la red, y suponiendo un coste total de 700 euros.

 Posteriormente el ecolatipac procede a pagar estas comidas, obteniendo un ahorro de 300 euros, y puede ofrecer a las personas de la red la posibilidad de comer en este establecimiento abonando el precio en moneda social, por lo que cada persona de la red que asista a dicha comida podrá abonar 10 monedas sociales, que supondrán para la red un ingreso de 1000 monedas sociales a cambio de 700 euros.

 Como cada euro que posee el ecolatipac equivale a una moneda social en negativo, esta operación le permite compensar 300 monedas adicionales, además de las 700 correspondientes a los 700 euros, lo que dejará un saldo de 300 euros disponibles en la red que ya no tienen necesidad de responder por ninguna moneda social, por lo que pueden ser reinvertidos en la propia red como aportación voluntaria del propio ecolatipac.

 En el mismo ejemplo anterior, podría optarse por repartir ese ahorro entre las personas usuarias y el ecolatipac, dejando que el ecolatipac solo genere una aportación de 100 euros, destinando el ahorro de los otros 200 a un descuento en el precio para las personas de la red que asistan a la comida, lo que tendrá un efecto beneficioso a efectos de difusión, ya que el día de la comida, mientras los clientes habituales pagan 10 euros por cada comida, la gente de la red podrá estar abonando únicamente 8 monedas sociales, lo que ayuda a que la gente vea reflejada en los precios la fuerza de la moneda social, además de hacer visible el uso de la monea social, y todo ello, obteniendo una aportación voluntaria de 100 euros para el ecolatipac.

 Esto, a su vez, puede animar al dueño del establecimiento a informarse un poco más sobre la moneda social, y plantearse si aceptar moneda social de una manera más habitual, para no perder esas 300 unidades monetarias que se apropia el grupo de consumo en cada evento.

 Si la red tiene bienes y servicios que le interesen, dicho establecimiento no tardará en incorporarse a la red, aunque sea como un Productor de Economía Mixta, liberando al grupo de consumo del trabajo de organizar este tipo de comidas en dicho establecimiento, e incrementando la oferta de la propia red de moneda social en aquella proporción que dicho establecimiento decida implicarse con la red.

 Lo mismo sucede cuando se aplica este sistema de compra colectiva a cualquier necesidad, ya sea concreta o habitual, que la red haya decidido satisfacer con el dinero gestionado desde el ecolatipac, porque siempre habrá un ahorro, y un productor en fase de incorporación.

 Y lo mismo sucede con el uso lúdico de la central de intercambio, ya que el consumo que normalmente se realiza en establecimientos ajenos a la red se debe realizar en euros, como sucede con los bares.

 Pero esta actividad puede desarrollarse en la central de intercambio, adquiriendo dichos productos a un precio inferior de manera colectiva y consumiéndolos en la central de intercambio en lugar de consumirlos en bares, lo que supone un ingreso para la red en forma de ahorro colectivo.

 Para poner un ejemplo, si el grupo de consumo se reúne en un bar y se gastan 1 euro en cada cerveza, consumiendo un total de 30 cervezas, dichas cervezas podrían haberse adquirido a 50 céntimos en un supermercado y tenerse guardadas en la central de intercambio.

 Cuando son consumidas, las personas de la red podrán consumirlas abonando el euro que pagaban en el bar, o pagando un precio inferior, según se quiera repartir el ahorro entra la red y las personas de la red.

 Suponiendo que la red decida dejar todo el ahorro en la red, por esas 30 cervezas consumidas, la red habrá ingresado 15 euros adicionales.

 Todo ello se menciona sin perjuicio de que dichas maniobras deban realizarse con productos no existentes en la red, ya que si la red produce cerveza de manera artesanal que están 100% en moneda social, será mejor consumir estos productos dentro de la red, y utilizar esta posibilidad de ahorro con aquellos bienes que no son producidos por la propia red.

- - -

### 7.3.2.- Forma de gastar los euros:

Aunque ya se han mencionado varias maneras de gastar los euros de la red, como son la compra colectiva para satisfacer las necesidades de la red, o las acciones de consumo colectivo orientadas a implicar más personas en la red, conviene hacer, antes de acabar, unas cuantas matizaciones sobre dichos actos de inversión por parte del ecolatipac, ya que serán esos gastos hechos con los euros colectivizados por la red lo que conocemos con el nombre de "descapitalizar", ya que el objetivo de esta acción consiste en sacar esos euros del sistema capitalista para integrarlos dentro de un sistema de economía alternativa.

1. __Las necesidades comunes:__

 Los euros colectivizados en el ecolatipac deberán destinarse a satisfacer las necesidades comunes, lo que de una parte deberá incluir aquellos gastos comunes que haya decidido tener la red para el desarrollo de su actividad, como será el alquiler del local (si se ha optado por alquilar un inmueble como central de intercambio), o los recibos de la luz y el agua correspondientes a dicho inmueble.

 En los casos en que se decida utilizar un formato de libreta o en billetes, habrá que asumir los costes económicos que dichos formatos puedan suponer, aunque siempre habrá posibilidad de que cada usuario imprima su libreta y posteriormente la red estampe un sello en la misma a modo de verificación de la libreta. También es posible que existan diseñadores gráficos o imprentas dentro de la red que puedan ofrecer los billetes y las libretas en moneda social, dejando los euros disponibles para otro tipo de necesidades que no puedan ser satisfechas desde la propia red.

 También serán gastos necesarios el pago de los impresos oficiales y las fotocopias de documentación que resulten necesarios para la constitución de la forma jurídica, o para la realización de contratos y negocios que sean necesarios para la consolidación de la red. Y lo mismo sucederá si se decide arrendar un dominio en internet para alojar la página web de la moneda.

 Estas necesidades, aunque tengan un carácter común, deberán ser sometidos a votación y acuerdo de la red, ya que si una red decide no disponer de dichos formatos, o de dichas infraestructuras legales, físicas o telemáticas, no tendrá necesidad de soportar estos gastos.

2. __ Los acuerdos de la red:__

 Estos gastos serán los gastos acordados por la red para que sean considerados como "necesidades", y serán gastos que cualquier persona de la red deberá poder 100% en moneda social a través del ecolatipac, asumiendo el ecolatipac el pago en euros, y recibiendo de la persona de la red la cantidad de monedas sociales correspondientes.

 Estas necesidades podrán varias de unas redes a otras, siendo lo habitual que las redes empiecen descapitalizando en necesidades de alimentación, al ser la alimentación una necesidad generalizada en la que resulta relativamente sencillo incorporar productores, debido a que los productores locales de alimento normalmente vende a intermediarios que obtienen un elevado margen de beneficio.

 Pero del mismo modo que se puede acordar la necesidad de alimentación, las redes tendrán que hilar cada vez más fino, y ser capaces de elegir qué tipo de alimentos desean comprar colectivamente, y podrán decidir si consumen alimentos ecológicos, si los prefieren locales, o si prefieren comprarlos baratos, sabiendo que lo ideal es que sean ecológicos, locales y baratos.

 Además de la alimentación, existen otro tipo de necesidades que podrían ser descapitalizadas si una red decide colectivizar ese gasto, como sucedería si todas las personas de una red se animan a contratar un conjunto de lineas de teléfono obteniendo un descuento considerable por la contratación conjunta, lo que supondría un ahorro para la red que sería repartido entre la red y los consumidores, y que permitiría pagar este servicio en moneda social.

 Lo mismo podría hacerse con la luz, o con el abastecimiento de cualquier producto o servicio que las personas de la red paguen normalmente en euros, pero estos acuerdos deben tener en cuenta una previsión del gasto que supondrá la incorporación de dicha necesidad a la red, para que el mismo resulte asumible con los ingresos existentes en el ecolatipac, y añadiendo los ingresos que podrá suponer dicho ahorro a la red: Y esto es porque puede pasar que aunque no resulte posible que el ecolatipac produzca dinero para afrontar el total de todas las facturas de teléfono de todas las personas de la red, seguramente sea posible asumir tales pagos si los euros que esas personas pagaban por dicho servicio fuera de la red quedan a disposición del ecolatipac, y lo más probable es que sobre dinero que pueda destinarse a satisfacer alguna otra necesidad.

3. __Las gratificaciones:__

 Por último, la red tendrá que gratificar a las personas que ayudan a que la red funcione, bien dedicando su tiempo a la gestión y preparación de los eventos, a quienes realizan jornadas más o menos estables en las centrales de intercambio, o cuando soliciten la ayuda o presencia de alguna persona a la que haya que financiar el viaje y el alojamiento, o recompensarle por realizar algún tipo de aportación valiosa a la red.

 Dichas gratificaciones serán la contrapartida de haber eliminado las relaciones laborales dentro de la red, sin que exista ningún contrato que obligue a hacer algo a cambio de tener derecho a exigir algo, sino que la red tenderá a funcionar con el trabajo voluntario de las personas que forman parte de ellas, o personas que facilitan el funcionamiento de las redes, como quienes crean infraestructuras telemáticas de software libre, o ceden espacios o trabajos realizados.

 En esos casos, es bueno que la red acostumbre a asignar una partida anual para gratificaciones, pudiendo realizar las mismas tanto en moneda social, como a través del ecolatipac. Dicha gratificación será ofrecida a las personas a quienes se acuerde gratificar, y dichas personas podrán aceptar o rechazar, total o parcialmente, dicha gratificación, o podrán pedir que una parte de la misma sea entregada a otra persona, o sea conservada dentro del ecolatipac para una futura descapitalización.

* * *

# EPILOGO
Con esto daremos por terminado este manual básico de economía alternativa y monedas sociales, considerando que ya contiene suficiente información como para que resulte posible iniciar y consolidar las redes de moneda social.

Nos hubiera gustado incorporar un capitulo sobre la constitución y gestión fiscal de la forma jurídica, pero debido a que sería un tema en el que saldrían nuevos temas a desarrollar, pospondremos dicho tema para un manual a parte sobre "construcción y manejo de marionetas", entendiendo que dichas formas jurídicas son las marionetas creadas por la red para interactuar con el sistema económico oficial de manera colectiva dentro del gran teatro de la economía actual.

Deseando que el presente manual les haya sido de utilidad, aprovechamos para darle las gracias por haber leído este manual, y les deseamos una feliz descapitalización de sus economías.

__Construir alternativas económicas ahora también depende de usted__.